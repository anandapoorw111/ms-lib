<?php 

$lang['import_import']     			= "Import CSV File";
$lang['import_list']     			= "Import file by CSV format";
$lang['import_note']     			= "Important Notes";
$lang['download']     				= "Download";
$lang['select_file']     			= "Select CSV File";
$lang['upload_btn']     			= "Import CSV";
$lang['import_note_1']     			= "Download CSV file format";
$lang['import_note_2']     			= "The table title of the CSV file cannot be changed";
$lang['import_note_3']     			= "Use the prefix of the book code correctly";
$lang['import_note_4']     			= "Book Category ID Must be Correct";
$lang['import_note_5']     			= "Book Writer ID Must be Correct";
$lang['import_note_6']     			= "Book Publications ID Must be Correct";

?>