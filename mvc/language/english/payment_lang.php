<?php 

$lang['payment_payment']     	= "Payment";
$lang['payment_add_payment']    = "Add Payment";
$lang['payment_list']     		= "List";
$lang['payment_member_name']    = "Member Name";
$lang['payment_for']     		= "Payment For";
$lang['payment_amount']     	= "Payable Amount";
$lang['payment_date']     		= "Date";
$lang['payment_status']     	= "Status";
$lang['payment_action'] 	  	= "Action";
$lang['payment_receipt'] 	  	= "Receipt";
$lang['payment_money_receipt'] 	= "Money Receipt";
$lang['payment_email'] 			= "Email";
$lang['payment_phone'] 			= "Contact No.";
$lang['payment_mr'] 			= "MR";
$lang['payment_from'] 	  		= "Cash Received From";
$lang['payment_of'] 	  		= "Of";
$lang['payment_for'] 	  		= "For";
$lang['payment_inword'] 	  	= "In Word";
$lang['payment_received_by'] 	= "Received By";


?>