<?php 
$lang['reports_reports']     	= "Reports";
$lang['reports_member']     	= "Member Report";
$lang['reports_book']     		= "Book Report";
$lang['reports_payment']     	= "Payment Report";
$lang['reports_categories']     = "Categories Report";
$lang['reports_details']     	= "Details Here";
$lang['reports_date']     		= "Date";
$lang['report_print']     		= "Print";
$lang['report_generated']     	= "Report Generate";
$lang['report_generate']     	= "Generate";
$lang['report_status']     		= "Status";

$lang['member_name']     		= "Name";
$lang['member_type']     		= "Type";
$lang['member_occupation']     	= "Occupation";
$lang['member_parents']     	= "Parents";
$lang['member_phone']     		= "Contact No.";
$lang['member_address']     	= "Address";
$lang['report_photo']     		= "Photo";
$lang['report_code']     		= "Code";
$lang['member_nid']     		= "NID";
$lang['report_remark'] 			= "Remark";

$lang['reports_please_select']     = "Please Select";
$lang['reports_membership_type']   = "Membership Type";
$lang['reports_member_Active']     = "Active";
$lang['reports_member_Inactive']   = "Inactive";


$lang['reports_categories_type']   = "Categories";

$lang['book_name']     			= "Name";
$lang['book_writer']     		= "Writer";
$lang['book_edition']     		= "Edition";
$lang['book_quantity'] 			= "Quantity";
$lang['book_availability'] 		= "Availability";
$lang['book_rack_no']    		= "Rack no.";
$lang['categories_name']     = "Name";
$lang['categories_book']     = "Books";



$lang['report_payment']     = "Payment Report";
$lang['reports_from_date']     = "From Date";
$lang['reports_to_date']     = "To Date";
$lang['reports_for']     = "For";
$lang['reports_lost_book']     = "Lost Book Fee";
$lang['reports_expiry_date']     = "Fee of exceeding return date";
$lang['reports_membership_fee']     = "Membership Fee";
$lang['payment_member_name']    = "Member Name";
$lang['payment_for']     		= "Payment For";
$lang['payment_amount']     	= "Payable Amount";
$lang['payment_date']     		= "Date";






?>