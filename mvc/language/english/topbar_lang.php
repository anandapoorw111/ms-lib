<?php 
$lang['add']      = "Add";
$lang['edit']     = "Edit";
$lang['view']     = "View";
$lang['delete']   = "Delete";

$lang['menu_dashboard']     = "Dashboard";
$lang['menu_user']          = "Users";
$lang['menu_newuser']          = "User";
$lang['menu_usertype']      = "User Role";

$lang['menu_permissions']   = "Permissions";
$lang['menu_permissionlog'] = "Permissionlog";
$lang['menu_permissionmodule'] = "Permission Module";
$lang['menu_menu']             = "Menu";

$lang['menu_settings']  = "Settings";
$lang['menu_sitesettings']  = "Company Profile";
$lang['menu_membership']  = "Membership";
$lang['menu_member']  = "Member";
$lang['menu_memberlist']  = "Member List";
$lang['menu_categories']  = "Categories";
$lang['menu_book']  = "Book";
$lang['menu_book_archive']  = "Archive";
$lang['menu_wastage']  = "Wastage";
$lang['menu_publication']  = "Publication";
$lang['menu_circulation']  = "Circulation";
$lang['menu_payment']  = "Payment";
$lang['menu_report']  = "Report";
$lang['menu_writer']  = "Writer";
$lang['menu_emailsetting']  = "Email Config";
$lang['menu_bookrequest']  = "Book Request";
$lang['menu_genarateid']  = "Genarate ID Card";
$lang['menu_import']  = "Import CSV";
?>