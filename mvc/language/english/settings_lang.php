<?php 

$lang['settings_settings']     = "Settings";
$lang['settings_general_setting']     = "General Settings";
$lang['settings_list']     = "List";

$lang['settings_company_name']      = "Company Name";
$lang['settings_tag_line']      = "Tag Line";
$lang['settings_type']      = "Business Type";
$lang['settings_owner']      = "Owner Name";
$lang['settings_mobile']      = "Mobile No.";
$lang['settings_phone']      = "Phone No.";
$lang['settings_fax']      = "Fax No.";
$lang['settings_email']      = "Email";
$lang['settings_tax']      = "Tax number";
$lang['settings_address']      = "Address";
$lang['settings_time_zone']      = "Time Zone";
$lang['settings_logo']      = "Company Logo";
$lang['settings_currency_code']  = "Currency Code";
$lang['settings_currency_symbol']  = "Currency Symbol";
$lang['settings_prefixes']  = "Prefixes";
$lang['category_prefixe']  = "Category";
$lang['member_prefixe']  = "Member";
$lang['book_prefixe']  = "Book";
$lang['membership_prefixe']  = "Membership";

$lang['settings_language']  = "Language";


$lang['settings_update']     = "Update";
$lang['settings_action'] 	  = "Action";




?>