<?php 

$lang['publication_publication']     = "Publication";
$lang['publication_add_publication']     = "Add Publication";
$lang['publication_list']     = "List";

$lang['publication_name']       = "Name";
$lang['publication_note']  	  = "Note";
$lang['publication_status']     = "Status";
$lang['publication_action'] 	  = "Action";


$lang['publication_insert'] = "Insert";
$lang['publication_update'] = "Update";

?>