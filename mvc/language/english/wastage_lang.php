<?php 

$lang['wastage_wastage']     = "Wastage";
$lang['wastage_add_wastage']     = "Add Wastage";
$lang['wastage_list']     = "List";

$lang['wastage_photo']       = "Photo";
$lang['wastage_book_code']       = "Book Code";
$lang['wastage_book_select']       = "Select Book Name";
$lang['wastage_book_name']  	  = "Book Name";
$lang['wastage_writer_name']     = "Writer Name";
$lang['wastage_wastager_by']     = "Wastage By";
$lang['wastage_date']     = "Date";
$lang['wastage_payable_amount']     = "Payable Amount";
$lang['wastage_note']     = "Note";
$lang['wastage_action'] 	  = "Action";


$lang['wastage_insert'] = "Insert";
$lang['wastage_update'] = "Update";

?>