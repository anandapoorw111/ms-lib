<?php 

$lang['genarateid_genarateid']     	 = "Genarate ID Card";
$lang['genarateid_please_select']    = "Please Select";
$lang['genarateid_label']     	  	 = "Select Member";
$lang['genarateid_generate']     	 = "Generate";
$lang['genarateid_tooltip']     	 = "You Can Select Multiple Members";
$lang['genarateid_details']       	 = "Details";
$lang['genarateid_print']       	 = "Download";
$lang['genarateid_name']       	 	 = "Name";
$lang['genarateid_joined']       	 = "Joined";
$lang['genarateid_membership']       = "Membership";
$lang['genarateid_gender']       	 = "Gender";


?>