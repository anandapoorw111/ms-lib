<?php 

$lang['categories_categories']     = "Kategorier";
$lang['categories_add_categories']     = "Lägg till Kategori";
$lang['categories_list']     = "Lista";

$lang['categories_code']       = "Koda";
$lang['categories_name']       = "namn";
$lang['categories_note']  	  = "Notera";
$lang['categories_status']     = "Status";
$lang['categories_action'] 	  = "Handling";

$lang['categories_insert'] = "Föra in";
$lang['categories_update'] = "Uppdatering";

?>