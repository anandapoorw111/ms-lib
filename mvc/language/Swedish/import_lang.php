<?php 

$lang['import_import']     			= "Importera CSV -fil";
$lang['import_list']     			= "Importera filen efter CSV -format";
$lang['import_note']     			= "Viktiga anteckningar";
$lang['download']     				= "Ladda ner";
$lang['select_file']     			= "Välj CSV -fil";
$lang['upload_btn']     			= "Importera CSV";
$lang['import_note_1']     			= "Ladda ner CSV -filformat";
$lang['import_note_2']     			= "Tabelltiteln för CSV -filen kan inte ändras";
$lang['import_note_3']     			= "Använd prefixet för bokkoden korrekt";
$lang['import_note_4']     			= "Bokkategori -ID måste vara korrekt";
$lang['import_note_5']     			= "Bokförfattar -ID måste vara korrekt";
$lang['import_note_6']     			= "Bokpublikations -ID måste vara korrekt";

?>