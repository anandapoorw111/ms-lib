<?php 

$lang['genarateid_genarateid']     	 = "Genarate ID-kort";
$lang['genarateid_please_select']    = "Vänligen välj";
$lang['genarateid_label']     	  	 = "Välj medlem";
$lang['genarateid_generate']     	 = "Generera";
$lang['genarateid_tooltip']     	 = "Välj flera medlemmar";
$lang['genarateid_details']       	 = "Detaljer";
$lang['genarateid_print']       	 = "Ladda ner";
$lang['genarateid_name']       	 	 = "namn";
$lang['genarateid_joined']       	 = "Gick med";
$lang['genarateid_membership']       = "Medlemskap";
$lang['genarateid_gender']       	 = "Kön";

?>