<?php 

$lang['circulation_circulation']     = "Omlopp";
$lang['circulation_add_circulation']     = "Lägg till Kategori";
$lang['circulation_issue_and_return']     = "Utfärdat och returnerat";
$lang['circulation_list']     = "Lista";

$lang['circulation_member_code'] = "Medlemskod";
$lang['circulation_member_id'] = "Medlems-ID";
$lang['circulation_member_name'] = "Medlemsnamn";
$lang['circulation_book_name'] = "Boknamn";
$lang['circulation_book_code'] = "Bokkod";
$lang['circulation_book_code_tooltip'] = "Sätt din bokkod";
$lang['circulation_writer_name'] = "Författarens namn";
$lang['circulation_issue_date'] = "Utgivningsdatum";
$lang['circulation_expiry_date'] = "Senaste datum att återvända";
$lang['circulation_return_date'] = "Återlämningsdatum";
$lang['circulation_return_status'] = "Returstatus";
$lang['circulation_penalty'] = "Straff";
$lang['circulation_penalty_fee'] = "Straffavgift";
$lang['circulation_no_of_days'] = "Antal dagar";
$lang['circulation_penalty_message'] = "Datum för bokomvandling är över. Vänligen betala straffet.";
$lang['circulation_penalty_note'] = "Notera";
$lang['member_penalty_amount'] = "Totala summan";
$lang['circulation_action'] 	  = "Handling";

$lang['circulation_insert'] = "Föra in";
$lang['circulation_update'] = "Uppdatering";
$lang['circulation_search'] = "Sök";
$lang['circulation_details'] = "Detaljer";
$lang['circulation_issue'] = "Utfärdad Bok";
$lang['circulation_issued'] = "Utfärdad";
$lang['circulation_not_available'] = "Inte tillgänglig";
$lang['circulation_member_search'] = "Sök medlem";

$lang['circulation_member_gender'] = "Kön";
$lang['circulation_member_occupation'] = "Ockupation";
$lang['circulation_member_phone'] = "Telefon";
$lang['circulation_book_return'] = "Lämna tillbaka";
$lang['circulation_book_renewal'] = "Förnyelse";
$lang['circulation_book_lost'] = "Förlorat";
$lang['circulation_book_lost_message'] = "Eftersom du förlorade boken måste du betala priset för boken.";
$lang['circulation_book_price'] = "Bokpris";
$lang['circulation_book_payable_amount'] = "Betalbart belopp";

$lang['book_photo']     		= "Foto";
$lang['book_code']     			= "Koda";
$lang['book_name']     			= "namn";
$lang['book_writer']     		= "Författare";
$lang['book_edition']     		= "Utgåva";
$lang['book_price']     		= "Pris";
$lang['book_availability'] 		= "Tillgänglighet";
$lang['book_rack_no']    		= "Rack nr.";
$lang['book_action'] 			= "Handling";
?>