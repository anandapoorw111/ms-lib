<?php 

$lang['user_user']     = "Användare";
$lang['user_add_user']     = "Lägg till användare";
$lang['user_add']     = "Lägg till";
$lang['user_list']     = "Lista";
$lang['user_name']     = "namn";
$lang['user_dob']      = "Födelsedatum";
$lang['user_gender']   = "Kön";
$lang['user_please_select']   = "Vänligen välj";
$lang['user_male']   = "Manlig";
$lang['user_female']   = "Kvinna";
$lang['user_religion'] = "Religion";
$lang['user_email']    = "E-post";
$lang['user_phone']    = "Telefon";
$lang['user_address']  = "Adress";
$lang['user_jod']      = "Datum för anslutning";
$lang['user_photo']    = "Foto";
$lang['user_active']   = "Aktiva";
$lang['user_usertypeID'] = "Användartyp-ID";
$lang['user_username'] = "Användarnamn";
$lang['user_password'] = "Lösenord";
$lang['user_role'] 	= "Roll";
$lang['user_status'] = "Status";
$lang['user_active'] = "Aktivera";
$lang['user_deactive'] = "Avaktivera";
$lang['user_action'] = "Handling";
$lang['user_update'] = "Uppdatering";
$lang['user_profile'] = "Profil";
$lang['user_change_password'] = "Ändra lösenord";
$lang['user_new_password'] = "nytt lösenord";
$lang['user_confirm_password'] = "Bekräfta lösenord";

















?>