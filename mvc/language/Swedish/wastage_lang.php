<?php 

$lang['wastage_wastage']     = "Slöseri";
$lang['wastage_add_wastage']     = "Lägg till slöseri";
$lang['wastage_list']     = "Lista";

$lang['wastage_photo']       = "Foto";
$lang['wastage_book_code']       = "Bokkod";
$lang['wastage_book_select']       = "Välj boknamn";
$lang['wastage_book_name']  	  = "Boknamn";
$lang['wastage_writer_name']     = "Författarens namn";
$lang['wastage_wastager_by']     = "Slöseri av";
$lang['wastage_date']     = "Datum";
$lang['wastage_payable_amount']     = "Betalbart belopp";
$lang['wastage_note']     = "Notera";
$lang['wastage_action'] 	  = "Handling";

$lang['wastage_insert'] = "Föra in";
$lang['wastage_update'] = "Uppdatering";

?>