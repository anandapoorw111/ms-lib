<?php 

$lang['bookrequest_bookrequest']     	= "Bokförfrågan";
$lang['bookrequest_add_bookrequest']    = "Begärd bok";
$lang['bookrequest_list']     			= "Lista";
$lang['bookrequest_name']       		= "Boknamn";
$lang['bookrequest_writer_name']       	= "Författarens namn";
$lang['bookrequest_categories']       	= "Kategorier";
$lang['bookrequest_edition']       		= "Utgåva";
$lang['bookrequest_note']  	  			= "Notera";
$lang['bookrequest_member']  	  		= "Medlemsnamn";
$lang['bookrequest_action'] 	  		= "Handling";
$lang['bookrequest_insert'] 			= "Föra in";
$lang['bookrequest_update'] 			= "Uppdatering";

?>