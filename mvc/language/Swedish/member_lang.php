<?php 
$lang['member_member']     			= "Medlem";
$lang['member_add_member']     		= "Lägg till medlem";
$lang['member_profile']     		= "Medlemsprofil";
$lang['member_update']     			= "Uppdatering";
$lang['member_add']     			= "Lägg till";
$lang['member_please_select']     	= "Vänligen välj";
$lang['member_info']     			= "Medlemsinformation";
$lang['member_list']     			= "Lista";
$lang['member_name']     			= "namn";
$lang['member_father_name']     	= "Faderns namn";
$lang['member_mother_name']     	= "Moderns namn";
$lang['member_date_of_birth']     	= "Födelsedatum";
$lang['member_nid_no']     			= "nationellt identitetskortnummer";
$lang['member_nid_no_tooltip']     	= "personnummer";
$lang['member_membership']     		= "Medlemskap";
$lang['member_membership_tooltip']	= "Välj vilken typ av medlemskap du vill ha";
$lang['member_occupation']     		= "Ockupation";
$lang['member_parents']     		= "Föräldrar";
$lang['member_gender']     			= "Kön";
$lang['member_male']     			= "Manlig";
$lang['member_female']     			= "Kvinna";
$lang['member_third_gender']     	= "Andra";
$lang['member_blood_group']     	= "Blodgrupp";
$lang['member_religion']     		= "Religion";
$lang['member_phone']     			= "Kontakta nr.";
$lang['member_email']     			= "E-post";
$lang['member_since']     			= "Registrerat datum";
$lang['member_address']     		= "Adress";
$lang['member_photo']     			= "Foto";
$lang['member_change_password']     = "Ändra lösenord";
$lang['member_new_password']     	= "nytt lösenord";
$lang['member_confirm_password']    = "Bekräfta lösenord";
$lang['member_login_info']     		= "Inloggningsinfo";
$lang['member_username']     		= "Användarnamn";
$lang['member_password']     		= "Lösenord";
$lang['member_confirm_password']    = "Bekräfta lösenord";
$lang['member_code']     			= "Koda";
$lang['member_status']     			= "Status";
$lang['member_action'] 				= "Handling";
$lang['member_details_list'] 		= "Detaljer";
$lang['member_circulation']			= "Cirkulationsdetaljer";
$lang['member_payment_details']		= "Betalningsinformation";

$lang['circulation_book_code']		= "Bokkod";
$lang['circulation_book_name']		= "Boknamn";
$lang['circulation_writer_name']	= "Författarens namn";
$lang['circulation_issue_date']		= "Utfärdat datum";
$lang['circulation_expiry_date']	= "Senaste datum att återvända";
$lang['circulation_return_date']	= "Returdatum";
$lang['circulation_penalty']		= "Straff";
$lang['circulation_return_status']	= "Status";

$lang['payment_for']				= "Betalning för";
$lang['payment_amount']				= "Belopp";
$lang['payment_date']				= "Datum";

?>