<?php 
$lang['reports_reports']     	= "Rapporter";
$lang['reports_member']     	= "Medlemsrapport";
$lang['reports_book']     		= "Bokrapport";
$lang['reports_payment']     	= "Betalningsrapport";
$lang['reports_categories']     = "Kategorirapport";
$lang['reports_details']     	= "Detaljer här";
$lang['reports_date']     		= "Datum";
$lang['report_print']     		= "Skriva ut";
$lang['report_generated']     	= "Rapporter generera";
$lang['report_generate']     	= "Generera";
$lang['report_status']     		= "Status";

$lang['member_name']     		= "namn";
$lang['member_type']     		= "Typ";
$lang['member_occupation']     	= "Ockupation";
$lang['member_parents']     	= "Föräldrar";
$lang['member_phone']     		= "Kontakta nr.";
$lang['member_address']     	= "Adress";
$lang['report_photo']     		= "Foto";
$lang['report_code']     		= "Koda";
$lang['member_nid']     		= "personnummer";
$lang['report_remark'] 			= "Anmärkning";

$lang['reports_please_select']     = "Vänligen välj";
$lang['reports_membership_type']   = "typ av medlemskap";
$lang['reports_member_Active']     = "Aktiva";
$lang['reports_member_Inactive']   = "Inaktiv";


$lang['reports_categories_type']   = "Kategorier";

$lang['book_name']     			= "namn";
$lang['book_writer']     		= "Författare";
$lang['book_edition']     		= "Utgåva";
$lang['book_quantity'] 			= "Kvantitet";
$lang['book_availability'] 		= "Tillgänglighet";
$lang['book_rack_no']    		= "Rack nr.";
$lang['categories_name']     = "namn";
$lang['categories_book']     = "Böcker";



$lang['report_payment']     = "Betalningsrapport";
$lang['reports_from_date']     = "Från datum";
$lang['reports_to_date']     = "Hittills";
$lang['reports_for']     = "För";
$lang['reports_lost_book']     = "Förlorad bokavgift";
$lang['reports_expiry_date']     = "Ersättning för överskridande av returdagen";
$lang['reports_membership_fee']     = "Medlemskaps avgift";
$lang['payment_member_name']    = "Medlemsnamn";
$lang['payment_for']     		= "Betalning för";
$lang['payment_amount']     	= "Betalbart belopp";
$lang['payment_date']     		= "Datum";

?>