<?php 

$lang['membership_membership']     = "Medlemskap";
$lang['membership_add_membership']     = "Lägg till medlemskap";
$lang['membership_list']     = "Lista";

$lang['membership_code']      = "Koda";
$lang['membership_name']      = "namn";
$lang['membership_limit_book']  = "Bok Utfärdad gräns";
$lang['membership_limit_day']  = "Daggräns";
$lang['membership_fee']  = "Medlemskaps avgift";
$lang['membership_penalty_fee']  = "Straffavgift";
$lang['membership_renew_limit']  = "Gräns ​​för förnyelse";
$lang['membership_free']  = "Fri";
$lang['membership_status']     = "Status";
$lang['membership_action'] 	  = "Handling";

$lang['membership_add_to_cart'] = "Lägg till i kundvagn";
$lang['membership_insert'] = "Föra in";
$lang['membership_error'] = "Något är fel";
$lang['membership_update'] = "Uppdatering";

$lang['membership_note'] = "Notera:";
$lang['membership_penalty_note'] = "1. Straffavgift tillkommer enligt dagen.";
$lang['membership_renew_note'] = "2. Förnyelsegränsen gäller för boken.";

?>