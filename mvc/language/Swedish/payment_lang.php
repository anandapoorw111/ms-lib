<?php 

$lang['payment_payment']     	= "Betalning";
$lang['payment_add_payment']    = "Lägg till betalning";
$lang['payment_list']     		= "Lista";
$lang['payment_member_name']    = "Medlemsnamn";
$lang['payment_for']     		= "Betalning för";
$lang['payment_amount']     	= "Betalbart belopp";
$lang['payment_date']     		= "Datum";
$lang['payment_status']     	= "Status";
$lang['payment_action'] 	  	= "Handling";
$lang['payment_receipt'] 	  	= "kvitto";
$lang['payment_money_receipt'] 	= "Pengar kvitto";
$lang['payment_email'] 			= "E-post";
$lang['payment_phone'] 			= "Kontakta nr.";
$lang['payment_mr'] 			= "HERR";
$lang['payment_from'] 	  		= "Kontanter erhållna från";
$lang['payment_of'] 	  		= "Av";
$lang['payment_for'] 	  		= "För";
$lang['payment_inword'] 	  	= "I ord";
$lang['payment_received_by'] 	= "Mottagen av";

?>