<?php 

$lang['payment_payment']     	= "भुगतान";
$lang['payment_add_payment']    = "भुगतान जोड़ें";
$lang['payment_list']     		= "सूची";
$lang['payment_member_name']    = "सदस्य का नाम";
$lang['payment_for']     		= "के लिए भुगतान";
$lang['payment_amount']     	= "भुगतान योग्य राशि";
$lang['payment_date']     		= "तारीख";
$lang['payment_status']     	= "स्थिति";
$lang['payment_action'] 	  	= "कार्य";
$lang['payment_receipt'] 	  	= "रसीद";
$lang['payment_money_receipt'] 	= "धन रसीद";
$lang['payment_email'] 			= "ईमेल";
$lang['payment_phone'] 			= "संपर्क नंबर।";
$lang['payment_mr'] 			= "श्री ग";
$lang['payment_from'] 	  		= "से प्राप्त नकद";
$lang['payment_of'] 	  		= "का";
$lang['payment_for'] 	  		= "के लिये";
$lang['payment_inword'] 	  	= "शब्द में";
$lang['payment_received_by'] 	= "द्वारा प्राप्त";

?>