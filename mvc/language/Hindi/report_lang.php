<?php 
$lang['reports_reports']     	= "रिपोर्टों";
$lang['reports_member']     	= "सदस्य रिपोर्ट";
$lang['reports_book']     		= "पुस्तक समीक्षा";
$lang['reports_payment']     	= "भुगतान रिपोर्ट";
$lang['reports_categories']     = "श्रेणियाँ रिपोर्ट";
$lang['reports_details']     	= "विवरण यहाँ";
$lang['reports_date']     		= "तारीख";
$lang['report_print']     		= "छाप";
$lang['report_generated']     	= "रिपोर्ट जनरेट";
$lang['report_generate']     	= "उत्पन्न";
$lang['report_status']     		= "स्थिति";

$lang['member_name']     		= "नाम";
$lang['member_type']     		= "प्रकार";
$lang['member_occupation']     	= "व्यवसाय";
$lang['member_parents']     	= "माता-पिता";
$lang['member_phone']     		= "संपर्क नंबर।";
$lang['member_address']     	= "पता";
$lang['report_photo']     		= "तस्वीर";
$lang['report_code']     		= "कोड";
$lang['member_nid']     		= "राष्ट्रीय पहचान पत्र";
$lang['report_remark'] 			= "रिमार्क";

$lang['reports_please_select']     = "कृपया चयन कीजिए";
$lang['reports_membership_type']   = "सदस्यता प्रकार";
$lang['reports_member_Active']     = "सक्रिय";
$lang['reports_member_Inactive']   = "निष्क्रिय";

$lang['reports_categories_type']   = "श्रेणियाँ";

$lang['book_name']     			= "नाम";
$lang['book_writer']     		= "लेखक";
$lang['book_edition']     		= "संस्करण";
$lang['book_quantity'] 			= "मात्रा";
$lang['book_availability'] 		= "उपलब्धता";
$lang['book_rack_no']    		= "रैक संख्या";
$lang['categories_name']     = "नाम";
$lang['categories_book']     = "पुस्तकें";

$lang['report_payment']     = "भुगतान रिपोर्ट";
$lang['reports_from_date']     = "तारीख से";
$lang['reports_to_date']     = "तारीख तक";
$lang['reports_for']     = "के लिये";
$lang['reports_lost_book']     = "खोई हुई किताब का शुल्क";
$lang['reports_expiry_date']     = "वापसी की तारीख विफल जुर्माना शुल्क";
$lang['reports_membership_fee']     = "मेम्बरशिप फीस";
$lang['payment_member_name']    = "सदस्य का नाम";
$lang['payment_for']     		= "के लिए भुगतान";
$lang['payment_amount']     	= "भुगतान योग्य राशि";
$lang['payment_date']     		= "तारीख";

?>