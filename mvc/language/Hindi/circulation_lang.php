<?php 

$lang['circulation_circulation']     = "प्रसार";
$lang['circulation_add_circulation']     = "श्रेणी जोड़ना";
$lang['circulation_issue_and_return']     = "जारी और लौटाया गया";
$lang['circulation_list']     = "सूची";

$lang['circulation_member_code'] = "सदस्य कोड";
$lang['circulation_member_id'] = "सदस्य पहचान पत्र";
$lang['circulation_member_name'] = "सदस्य का नाम";
$lang['circulation_book_name'] = "पुस्तक का नाम";
$lang['circulation_book_code'] = "पुस्तक कोड";
$lang['circulation_book_code_tooltip'] = "अपना बुक कोड डालें";
$lang['circulation_writer_name'] = "लेखक का नाम";
$lang['circulation_issue_date'] = "लेखक का नाम";
$lang['circulation_expiry_date'] = "अंतिम तिथी";
$lang['circulation_return_date'] = "वापसी दिनांक";
$lang['circulation_return_status'] = "वापसी की स्थिति";
$lang['circulation_penalty'] = "दंड";
$lang['circulation_penalty_fee'] = "दंड शुल्क";
$lang['circulation_no_of_days'] = "दिनों की संख्या";
$lang['circulation_penalty_message'] = "इस पुस्तक की वापसी तिथि पार हो गई है। कृपया दंड का भुगतान करें।";
$lang['circulation_penalty_note'] = "टिप्पणी";
$lang['member_penalty_amount'] = "कुल राशि";
$lang['circulation_action'] 	  = "कार्य";

$lang['circulation_insert'] = "डालने";
$lang['circulation_update'] = "अपडेट करें";
$lang['circulation_search'] = "खोज";
$lang['circulation_details'] = "विवरण";
$lang['circulation_issue'] = "अंक पुस्तक";
$lang['circulation_issued'] = "अंक";
$lang['circulation_not_available'] = "उपलब्ध नहीं है";
$lang['circulation_member_search'] = "सदस्य खोजें";

$lang['circulation_member_gender'] = "लिंग";
$lang['circulation_member_occupation'] = "व्यवसाय";
$lang['circulation_member_phone'] = "फ़ोन";
$lang['circulation_book_return'] = "वापसी";
$lang['circulation_book_renewal'] = "नवीनीकरण";
$lang['circulation_book_lost'] = "खो गया";
$lang['circulation_book_lost_message'] = "चूंकि आपने किताब खो दी है, आपको किताब की कीमत चुकानी होगी।";
$lang['circulation_book_price'] = "पुस्तक मूल्य";
$lang['circulation_book_payable_amount'] = "भुगतान योग्य राशि";

$lang['book_photo'] = "तस्वीर";
$lang['book_code'] = "कोड";
$lang['book_name'] = "नाम";
$lang['book_writer'] = "लेखक";
$lang['book_edition'] = "संस्करण";
$lang['book_price'] = "किताब की कीमत";
$lang['book_availability'] = "उपलब्धता";
$lang['book_rack_no'] = "रैक नं।";
$lang['book_action'] = "कार्य";

?>