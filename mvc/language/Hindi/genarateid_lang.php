<?php 

$lang['genarateid_genarateid']     	 = "आईडी कार्ड जनरेट करें";
$lang['genarateid_please_select']    = "कृपया चयन कीजिए";
$lang['genarateid_label']     	  	 = "सदस्य चुनें";
$lang['genarateid_generate']     	 = "उत्पन्न";
$lang['genarateid_tooltip']     	 = "एकाधिक सदस्यों का चयन करें";
$lang['genarateid_details']       	 = "विवरण";
$lang['genarateid_print']       	 = "डाउनलोड";
$lang['genarateid_name']       	 	 = "नाम";
$lang['genarateid_joined']       	 = "युक्त";
$lang['genarateid_membership']       = "सदस्यता";
$lang['genarateid_gender']       	 = "लिंग";

?>