<?php 

$lang['login_username']    = "उपयोगकर्ता नाम";
$lang['login_password']    = "पासवर्ड";
$lang['login_sign_in']     = "साइन इन करें";
$lang['retry']     = "पुनः प्रयास करें लॉगिन";
$lang['login_remember_me'] = "लॉगिन जानकारी सहेजें";
$lang['forgot_password'] = "मैं अपना पासवर्ड भूल गया";
$lang['forgot_password_header'] = "Forgot Password";
$lang['forgot_password_sms'] = "अपना पासवर्ड यहाँ बदलें";
$lang['forgot_email_address'] = "ईमेल पता";
$lang['reset_password'] = "पासवर्ड रीसेट";
$lang['reset_password_sms'] = "अपना नया पासवर्ड सेट करें";
$lang['new_password'] = "नया पासवर्ड";
$lang['confirm_new_password'] = "नए पासवर्ड की पुष्टि करें";
$lang['validation'] = "सत्यापन प्रक्रिया";
$lang['validation_code'] = "पुष्टि संख्या";
$lang['validation_sms'] = "कृपया सत्यापन कोड को ई-मेल से कॉपी करें और इसे यहां पेस्ट करें";
$lang['verify_button'] = "सत्यापित करें";
$lang['check_validation_code'] = "सत्यापन कोड अमान्य है";

?>