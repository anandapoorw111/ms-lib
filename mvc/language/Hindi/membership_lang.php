<?php 

$lang['membership_membership']     = "सदस्यता";
$lang['membership_add_membership']     = "सदस्यता जोड़ें";
$lang['membership_list']     = "सूची";

$lang['membership_code']      = "कोड";
$lang['membership_name']      = "नाम";
$lang['membership_limit_book']  = "पुस्तक सीमा";
$lang['membership_limit_day']  = "दिन की सीमा";
$lang['membership_fee']  = "मेम्बरशिप फीस";
$lang['membership_penalty_fee']  = "दंड शुल्क";
$lang['membership_renew_limit']  = "नवीनीकरण सीमा";
$lang['membership_free']  = "स्वतंत्र";
$lang['membership_status']     = "स्थिति";
$lang['membership_action'] 	  = "कार्य";

$lang['membership_add_to_cart'] = "कार्ट में जोड़ें";
$lang['membership_insert'] = "डालने";
$lang['membership_error'] = "कुछ गड़बड़ है";
$lang['membership_update'] = "अपडेट करें";

$lang['membership_note'] = "टिप्पणी:";
$lang['membership_penalty_note'] = "1. पेनल्टी फीस दिन के हिसाब से जोड़ी जाएगी।";
$lang['membership_renew_note'] = "2. नवीनीकरण की सीमा पुस्तक पर लागू होती है।";

?>