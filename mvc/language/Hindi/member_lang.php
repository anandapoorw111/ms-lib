<?php 
$lang['member_member']     			= "सदस्य";
$lang['member_add_member']     		= "सदस्य जोड़ें";
$lang['member_profile']     		= "सदस्य प्रोफ़ाइल";
$lang['member_update']     			= "अपडेट करें";
$lang['member_add']     			= "जोड़ना";
$lang['member_please_select']     	= "कृपया चयन कीजिए";
$lang['member_info']     			= "सदस्य जानकारी";
$lang['member_list']     			= "सूची";
$lang['member_name']     			= "नाम";
$lang['member_mother_name']     	= "मां का नाम";
$lang['member_father_name']     	= "पिता का नाम";
$lang['member_date_of_birth']     	= "जन्म की तारीख";
$lang['member_nid_no']     			= "राष्ट्रीय पहचान पत्र संख्या";
$lang['member_nid_no_tooltip']     	= "राष्ट्रीय पहचान संख्या";
$lang['member_membership']     		= "सदस्यता";
$lang['member_membership_tooltip']	= "सदस्यता के प्रकार का चयन";
$lang['member_occupation']     		= "व्यवसाय";
$lang['member_parents']     		= "माता-पिता";
$lang['member_gender']     			= "लिंग";
$lang['member_male']     			= "पुरुष";
$lang['member_female']     			= "महिला";
$lang['member_third_gender']     	= "अन्य";
$lang['member_blood_group']     	= "रक्त समूह";
$lang['member_religion']     		= "धर्म";
$lang['member_phone']     			= "संपर्क नंबर।";
$lang['member_email']     			= "ईमेल";
$lang['member_since']     			= "पंजीकृत तिथि";
$lang['member_address']     		= "पता";
$lang['member_photo']     			= "तस्वीर";
$lang['member_change_password']     = "पासवर्ड बदलें";
$lang['member_new_password']     	= "नया पासवर्ड";
$lang['member_confirm_password']    = "पासवर्ड की पुष्टि कीजिये";
$lang['member_login_info']     		= "लॉगिन जानकारी";
$lang['member_username']     		= "उपयोगकर्ता नाम";
$lang['member_password']     		= "कुंजिका";
$lang['member_confirm_password']    = "पासवर्ड की पुष्टि कीजिये";
$lang['member_code']     			= "कोड";
$lang['member_status']     			= "स्थिति";
$lang['member_action'] 				= "कार्य";
$lang['member_details_list'] 		= "विवरण";
$lang['member_circulation']			= "परिसंचरण विवरण";
$lang['member_payment_details']		= "भुगतान विवरण";

$lang['circulation_book_code']		= "पुस्तक कोड";
$lang['circulation_book_name']		= "पुस्तक का नाम";
$lang['circulation_writer_name']	= "लेखक का नाम";
$lang['circulation_issue_date']		= "जारी दिनांक";
$lang['circulation_expiry_date']	= "लौटने की अंतिम तिथि";
$lang['circulation_return_date']	= "वापसी की तारीख";
$lang['circulation_penalty']		= "दंड";
$lang['circulation_return_status']	= "स्थिति";

$lang['payment_for']				= "के लिए भुगतान";
$lang['payment_amount']				= "रकम";
$lang['payment_date']				= "तारीख";

?>