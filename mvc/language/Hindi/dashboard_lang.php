<?php 

$lang['dashboard_dashboard']     = "डैशबोर्ड";
$lang['dashboard_more']  = "और जानकारी";
$lang['dashboard_member']     = "सदस्यों";
$lang['dashboard_book'] 	  = "पुस्तकें";
$lang['dashboard_categories'] 	  = "श्रेणियाँ";
$lang['dashboard_membership'] = "सदस्यता";
$lang['dashboard_issued'] = "जारी किए गए";
$lang['dashboard_wastage'] = "क्षय";
$lang['dashboard_publication'] = "प्रकाशन";
$lang['dashboard_writer'] = "लेखक";
$lang['dashboard_bar_chat'] = "जारी और लौटाया गया बार चार्ट";
$lang['dashboard_issued_book'] = "अंतिम 5 जारी पुस्तक";
$lang['dashboard_returned_book'] = "अंतिम 5 लौटाई गई पुस्तक";
$lang['circulation_member_name'] = "सदस्य का नाम";
$lang['circulation_book_name'] = "पुस्तक का नाम";
$lang['circulation_writer_name'] = "लेखक का नाम";
$lang['circulation_issue_date'] = "जारी करने की तिथि";
$lang['circulation_expiry_date'] = "लौटने की अंतिम तिथि";
$lang['circulation_return_date'] = "वापसी दिनांक";
$lang['categories_code'] = "कोड";
$lang['categories_name'] = "नाम";

?>