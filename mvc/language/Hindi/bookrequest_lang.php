<?php 

$lang['bookrequest_bookrequest']     	= "पुस्तक अनुरोध";
$lang['bookrequest_add_bookrequest']    = "अनुरोधित पुस्तक";
$lang['bookrequest_list']     			= "सूची";
$lang['bookrequest_name']       		= "पुस्तक का नाम";
$lang['bookrequest_writer_name']       	= "लेखक का नाम";
$lang['bookrequest_categories']       	= "श्रेणियाँ";
$lang['bookrequest_edition']       		= "संस्करण";
$lang['bookrequest_note']  	  			= "टिप्पणी";
$lang['bookrequest_member']  	  		= "सदस्य का नाम";
$lang['bookrequest_action'] 	  		= "कार्य";
$lang['bookrequest_insert'] 			= "डालने";
$lang['bookrequest_update'] 			= "अपडेट करें";

?>