<?php 
$lang['add']      = "অ্যাড";
$lang['edit']     = "এডিট";
$lang['view']     = "ভিউ";
$lang['delete']   = "ডিলিট";

$lang['menu_dashboard']     = "ড্যাশবোর্ড";
$lang['menu_user']          = "ইউজাররা ";
$lang['menu_newuser']          = "ইউজার";
$lang['menu_usertype']      = "ইউজার রোল";

$lang['menu_permissions']   = "পারমিশন";
$lang['menu_permissionlog'] = "পারমিশন লগ";
$lang['menu_permissionmodule'] = "পারমিশন মডুল";
$lang['menu_menu']             = "মেনু";

$lang['menu_settings']  = "সেটিংস";
$lang['menu_sitesettings']  = "কোম্পানি প্রোফাইল";
$lang['menu_membership']  = "সদস্যপদ";
$lang['menu_member']  = "সদস্য";
$lang['menu_memberlist']  = "সদস্য তালিকা";
$lang['menu_categories']  = "ক্যাটাগরি";
$lang['menu_book']  = "বই";
$lang['menu_book_archive']  = "আর্কাইভ";
$lang['menu_wastage']  = "অপচয়";
$lang['menu_publication']  = "প্রকাশনা";
$lang['menu_circulation']  = "সংবহন";
$lang['menu_payment']  = "পেমেন্ট";
$lang['menu_report']  = "রিপোর্ট";
$lang['menu_writer']  = "লেখক";
$lang['menu_emailsetting']  = "ইমেল কনফিগারেশন";
$lang['menu_bookrequest']  = "বইয়ের অনুরোধ";
$lang['menu_genarateid']  = "আইডি কার্ড জেনারেট করুন";
$lang['menu_import']  = "CSV ইম্পোরট করুন";
?>