
<?php 

$lang['user_user']     = "ইউজার";
$lang['user_add_user']     = "ইউজার সংযোজন";
$lang['user_add']     = "সংযোজন";
$lang['user_list']     = "তালিকা";
$lang['user_name']     = "নাম";
$lang['user_dob']      = "জন্ম তারিখ";
$lang['user_gender']   = "লিঙ্গ";
$lang['user_please_select']   = "নির্বাচন করুন";
$lang['user_male']   = "পুরুষ";
$lang['user_female']   = "নারী";
$lang['user_religion'] = "ধর্ম";
$lang['user_email']    = "ইমেল";
$lang['user_phone']    = "ফোন";
$lang['user_address']  = "ঠিকানা";
$lang['user_jod']      = "যোগদানের তারিখ";
$lang['user_photo']    = "ছবি";
$lang['user_active']   = "সক্রিয়";
$lang['user_usertypeID'] = "ইউজারটাইপ আইডি";
$lang['user_username'] = "ইউজার নেম";
$lang['user_password'] = "পাসওয়ার্ড";
$lang['user_role'] 	= "রোল";
$lang['user_status'] = "অবস্থা";
$lang['user_active'] = "সক্রিয় করুন";
$lang['user_deactive'] = "নিষ্ক্রিয় করুন";
$lang['user_action'] = "ক্রিয়া";
$lang['user_update'] = "আপডেট";
$lang['user_profile'] = "প্রোফাইল";
$lang['user_change_password'] = "পাসওয়ার্ড পরিবর্তন করুন";
$lang['user_new_password'] = "নতুন পাসওয়ার্ড";
$lang['user_confirm_password'] = "পাসওয়ার্ড নিশ্চিত করুন";

















?>