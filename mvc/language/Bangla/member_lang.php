<?php 
$lang['member_member']     			= "সদস্য";
$lang['member_add_member']     		= "সদস্য সংযোজন";
$lang['member_profile']     		= "সদস্য প্রোফাইল";
$lang['member_update']     			= "আপডেট";
$lang['member_add']     			= "সংযোজন";
$lang['member_please_select']     	= "নির্বাচন করুন";
$lang['member_info']     			= "সদস্য তথ্য";
$lang['member_list']     			= "তালিকা";
$lang['member_name']     			= "নাম";
$lang['member_father_name']     	= "পিতার নাম";
$lang['member_mother_name']     	= "মাতার নাম";
$lang['member_date_of_birth']     	= "জন্ম তারিখ";
$lang['member_nid_no']     			= "এনআইডি নম্বর";
$lang['member_nid_no_tooltip']     	= "National Identification Number";
$lang['member_membership']     		= "সদস্যপদ";
$lang['member_membership_tooltip']	= "এখান থেকে সদস্যপদের ধরণ নির্বাচন করুন";
$lang['member_occupation']     		= "পেশা";
$lang['member_parents']     		= "পিতা-মাতা";
$lang['member_gender']     			= "লিঙ্গ";
$lang['member_male']     			= "পুরুষ";
$lang['member_female']     			= "নারী";
$lang['member_third_gender']     	= "অন্যান্য";
$lang['member_blood_group']     	= "রক্তের গ্রুপ";
$lang['member_religion']     		= "ধর্ম";
$lang['member_phone']     			= "যোগাযোগের নম্বর.";
$lang['member_email']     			= "ইমেল";
$lang['member_since']     			= "রেজিস্ট্রেশান তারিখ";
$lang['member_address']     		= "ঠিকানা";
$lang['member_photo']     			= "ছবি";
$lang['member_change_password']     = "পাসওয়ার্ড পরিবর্তন";
$lang['member_new_password']     	= "নতুন পাসওয়ার্ড";
$lang['member_confirm_password']    = "পাসওয়ার্ড নিশ্চিত করুন";
$lang['member_login_info']     		= "লগইন তথ্য";
$lang['member_username']     		= "ইউজারনেম";
$lang['member_password']     		= "পাসওয়ার্ড";
$lang['member_confirm_password']    = "পাসওয়ার্ড নিশ্চিত করুন";
$lang['member_code']     			= "কোড";
$lang['member_status']     			= "অবস্থা";
$lang['member_action'] 				= "ক্রিয়া";
$lang['member_details_list'] 		= "বিস্তারিত বিবরন";
$lang['member_circulation']			= "সংবহন বিবরন";
$lang['member_payment_details']		= "পেমেন্ট বিবরন";

$lang['circulation_book_code']		= "বইয়ের কোড";
$lang['circulation_book_name']		= "বইয়ের নাম";
$lang['circulation_writer_name']	= "লেখকের নাম";
$lang['circulation_issue_date']		= "ইস্যুর তারিখ";
$lang['circulation_expiry_date']	= "মেয়াদ শেষ তারিখ";
$lang['circulation_return_date']	= "মেয়াদ শেষ তারিখ";
$lang['circulation_penalty']		= "জরিমানা";
$lang['circulation_return_status']	= "অবস্থা";

$lang['payment_for']				= "পেমেন্টর কারন";
$lang['payment_amount']				= "পেমেন্টর পরিমান";
$lang['payment_date']				= "তারিখ";



?>