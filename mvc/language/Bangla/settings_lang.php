<?php 

$lang['settings_settings']     = "সেটিংস";
$lang['settings_general_setting']     = "সাধারন সেটিংস";
$lang['settings_list']     = "তালিকা";

$lang['settings_company_name']      = "প্রতিষ্ঠানের নাম";
$lang['settings_tag_line']      = "ট্যাগ লাইন";
$lang['settings_type']      = "ব্যবসার ধরণ";
$lang['settings_owner']      = "মালিকের নাম";
$lang['settings_mobile']      = "মুঠোফোন";
$lang['settings_phone']      = "ফোন";
$lang['settings_fax']      = "ফ্যাক্স";
$lang['settings_email']      = "ইমেল";
$lang['settings_tax']      = "ট্যাক্স নম্বর";
$lang['settings_address']      = "ঠিকানা";
$lang['settings_time_zone']      = "টাইম জোন";
$lang['settings_logo']      = "কোম্পানী লোগো";
$lang['settings_currency_code']  = "কারেন্সি";
$lang['settings_currency_symbol']  = "কারেন্সি প্রতীক";
$lang['settings_prefixes']  = "প্রেফিক্সেস";
$lang['category_prefixe']  = "ক্যাটাগরি";
$lang['member_prefixe']  = "সদস্য";
$lang['book_prefixe']  = "বই";
$lang['membership_prefixe']  = "সদস্যপদ";
$lang['settings_language']  = "ভাষা";

$lang['settings_update']     = "আপডেট";
$lang['settings_action'] 	  = "ক্রিয়া";




?>