<?php 

$lang['publication_publication']     = "প্রকাশনা";
$lang['publication_add_publication']     = "প্রকাশনা সংযোজন";
$lang['publication_list']     = "তালিকা";

$lang['publication_name']       = "নাম";
$lang['publication_note']  	  = "নোট";
$lang['publication_status']     = "অবস্থা";
$lang['publication_action'] 	  = "ক্রিয়া";


$lang['publication_insert'] = "ইনসার্ট";
$lang['publication_update'] = "আপডেট";

?>