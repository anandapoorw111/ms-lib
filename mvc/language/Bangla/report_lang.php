<?php 
$lang['reports_reports']     	= "রিপোর্ট";
$lang['reports_member']     	= "সদস্য রিপোর্ট";
$lang['reports_book']     		= "বইয়ের রিপোর্ট";
$lang['reports_payment']     	= "পেমেন্ট রিপোর্ট";
$lang['reports_categories']     = "ক্যাটাগরি রিপোর্ট";
$lang['reports_details']     	= "বিস্তারিত বিবরন";
$lang['reports_date']     		= "তারিখ";
$lang['report_print']     		= "প্রিন্ট করুন";
$lang['report_generated']     	= "জেনারেট রিপোর্ট";
$lang['report_generate']     	= "জেনারেট";
$lang['report_status']     		= "অবস্থা";

$lang['member_name']     		= "নাম";
$lang['member_type']     		= "সদস্যের প্রকার";
$lang['member_occupation']     	= "পেশা";
$lang['member_parents']     	= "পিতা-মাতা";
$lang['member_phone']     		= "যোগাযোগের নম্বর";
$lang['member_address']     	= "ঠিকানা";
$lang['report_photo']     		= "ছবি";
$lang['report_code']     		= "কোড";
$lang['member_nid']     		= "এনআইডি নম্বর";
$lang['report_remark'] 			= "মন্তব্য";

$lang['reports_please_select']     = "নির্বাচন করুন";
$lang['reports_membership_type']   = "সদস্যপদের প্রকার";
$lang['reports_member_Active']     = "সক্রিয়";
$lang['reports_member_Inactive']   = "নিষ্ক্রিয়";


$lang['reports_categories_type']   = "ক্যাটাগরি সমূহ";

$lang['book_name']     			= "নাম";
$lang['book_writer']     		= "লেখক";
$lang['book_edition']     		= "সংস্করণ";
$lang['book_quantity'] 			= "পরিমাণ";
$lang['book_availability'] 		= "প্রাপ্যতা";
$lang['book_rack_no']    		= "তাক নাম্বার";
$lang['categories_name']     = "নাম";
$lang['categories_book']     = "বই";



$lang['report_payment']     = "পেমেন্ট রিপোর্ট";
$lang['reports_from_date']     = "তারিখ হইতে";
$lang['reports_to_date']     = "তারিখ পর্যন্ত";
$lang['reports_for']     = "এইজন্য";
$lang['reports_lost_book']     = "হারানো বই এর জরিমানা";
$lang['reports_expiry_date']     = "মেয়াদান্তের জরিমানা";
$lang['reports_membership_fee']     = "সদস্য ফি";
$lang['payment_member_name']    = "সদস্যের নাম";
$lang['payment_for']     		= "পেমেন্টর কারন";
$lang['payment_amount']     	= "পেমেন্টর পরিমান";
$lang['payment_date']     		= "তারিখ";






?>