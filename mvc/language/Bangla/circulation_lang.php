<?php 

$lang['circulation_circulation']     = "সংবহন";
$lang['circulation_add_circulation']     = "শ্রেণিবদ্ধ সংযোজন";
$lang['circulation_issue_and_return']     = "ইস্যু ও ফেরৎ";
$lang['circulation_list']     = "তালিকা";

$lang['circulation_member_code'] = "সদস্য কোড";
$lang['circulation_member_id'] = "সদস্য আইডি";
$lang['circulation_member_name'] = "সদস্যের নাম";
$lang['circulation_book_name'] = "বইয়ের নাম";
$lang['circulation_book_code'] = "বইয়ের কোড";
$lang['circulation_book_code_tooltip'] = "আপনার বইয়ের কোড দিন";
$lang['circulation_writer_name'] = "লেখকের নাম";
$lang['circulation_issue_date'] = "ইস্যুর তারিখ";
$lang['circulation_expiry_date'] = "মেয়াদ শেষ তারিখ";
$lang['circulation_return_date'] = "ফেরৎ তারিখ";
$lang['circulation_return_status'] = "রিটার্নের স্থিতি";
$lang['circulation_penalty'] = "জরিমানা";
$lang['circulation_penalty_fee'] = "জরিমানার পরিমাণ";
$lang['circulation_no_of_days'] = "দিন সংখ্যা";
$lang['circulation_penalty_message'] = "এই বই ফেরতের তারিখটি পেরিয়ে গেছে। জরিমানা পরিশোধ করুন।";
$lang['circulation_penalty_note'] = "নোট";
$lang['member_penalty_amount'] = "সর্বমোট টাকা";
$lang['circulation_action'] 	  = "ক্রিয়া";


$lang['circulation_insert'] = "ইনসার্ট";
$lang['circulation_update'] = "আপডেট";
$lang['circulation_search'] = "খুঁজে দেখুন";
$lang['circulation_details'] = "বিস্তারিত";
$lang['circulation_issue'] = "বই ইস্যু";
$lang['circulation_issued'] = "ইস্যু";
$lang['circulation_not_available'] = "পাওয়া যায় নাই";
$lang['circulation_member_search'] = "সদস্য অনুসন্ধান";

$lang['circulation_member_gender'] = "লিঙ্গ";
$lang['circulation_member_occupation'] = "পেশা";
$lang['circulation_member_phone'] = "ফোন";
$lang['circulation_book_return'] = "ফেরৎ";
$lang['circulation_book_renewal'] = "নবায়ন";
$lang['circulation_book_lost'] = "হারিয়ে গেছে";
$lang['circulation_book_lost_message'] = "যেহেতু আপনি বইটি হারিয়েছেন, আপনাকে বইয়ের মূল্য দিতে হবে।";
$lang['circulation_book_price'] = "বইয়ের মূল্য";
$lang['circulation_book_payable_amount'] = "প্রদেয় পরিমান";


$lang['book_photo'] = "ছবি";
$lang['book_code'] = "কোড";
$lang['book_name'] = "নাম";
$lang['book_writer'] = "লেখক";
$lang['book_edition'] = "সংস্করণ";
$lang['book_price'] = "মূল্য";
$lang['book_availability'] = "প্রাপ্যতা";
$lang['book_rack_no'] = "তাক নাম্বার";
$lang['book_action'] = "ক্রিয়া";

?>