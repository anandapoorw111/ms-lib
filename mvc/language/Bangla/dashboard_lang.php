<?php 

$lang['dashboard_dashboard']     = "ড্যাশবোর্ড";
$lang['dashboard_more']  = "অধিক তথ্য";
$lang['dashboard_member']     = "সদস্য তথ্য";
$lang['dashboard_book'] 	  = "বইয়ের তথ্য";
$lang['dashboard_categories'] 	  = "শ্রেণিবদ্ধ";
$lang['dashboard_membership'] = "সদস্যপদ";
$lang['dashboard_issued'] = "ইস্যু কৃত";
$lang['dashboard_wastage'] = "অপচয়";
$lang['dashboard_publication'] = "প্রকাশনা";
$lang['dashboard_writer'] = "লেখক";
$lang['dashboard_bar_chat'] = "প্রদান ও ফেরৎ বার চার্ট";
$lang['dashboard_issued_book'] = "সর্বশেষ ৫টি ইস্যু কৃত বই";
$lang['dashboard_returned_book'] = "সর্বশেষ ৫টি রিটার্নড বই";
$lang['circulation_member_name'] = "সদস্যের নাম";
$lang['circulation_book_name'] = "বইয়ের নাম";
$lang['circulation_writer_name'] = "লেখকের নাম";
$lang['circulation_issue_date'] = "ইস্যুর তারিখ";
$lang['circulation_expiry_date'] = "মেয়াদ শেষ তারিখ";
$lang['circulation_return_date'] = "ফেরৎ তারিখ";
$lang['categories_code'] = "কোড";
$lang['categories_name'] = "নাম";

?>