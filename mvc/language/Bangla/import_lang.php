<?php 

$lang['import_import']     			= "CSV ফাইল ইম্পোরট করুন";
$lang['import_list']     			= "CSV ফরম্যাটে ফাইল ইম্পোরট করুন";
$lang['import_note']     			= "গুরুত্বপূর্ণ নোট";
$lang['download']     				= "ডাউনলোড করুন";
$lang['select_file']     			= "CSV ফাইল নির্বাচন করুন";
$lang['upload_btn']     			= "ইম্পোরট CSV";
$lang['import_note_1']     			= "CSV ফাইল ফরম্যাট ডাউনলোড করুন";
$lang['import_note_2']     			= "CSV ফাইলের টেবিলের শিরোনাম পরিবর্তন করা যাবে না";
$lang['import_note_3']     			= "বই কোডের প্রিফিক্স সঠিকভাবে ব্যবহার করুন";
$lang['import_note_4']     			= "বইয়ের ক্যাটাগরি আইডি সঠিক হতে হবে";
$lang['import_note_5']     			= "বই লেখক আইডি সঠিক হতে হবে";
$lang['import_note_6']     			= "বই প্রকাশনা আইডি সঠিক হতে হবে";

?>