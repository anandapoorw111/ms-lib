<?php 

$lang['bookrequest_bookrequest']     	= "বইয়ের অনুরোধ";
$lang['bookrequest_add_bookrequest']    = "অনুরোধিত বই";
$lang['bookrequest_list']     			= "তালিকা";
$lang['bookrequest_name']       		= "বইয়ের নাম";
$lang['bookrequest_writer_name']       	= "লেখকের নাম";
$lang['bookrequest_categories']       	= "বিভাগসমূহ";
$lang['bookrequest_edition']       		= "সংস্করণ";
$lang['bookrequest_note']  	  			= "নোট";
$lang['bookrequest_member']  	  		= "সদস্যের নাম";
$lang['bookrequest_action'] 	  		= "ক্রিয়া";
$lang['bookrequest_insert'] 			= "ইনসার্ট";
$lang['bookrequest_update'] 			= "আপডেট";

?>