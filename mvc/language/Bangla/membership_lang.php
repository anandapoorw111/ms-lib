<?php 

$lang['membership_membership']     = "সদস্যপদ";
$lang['membership_add_membership']     = "সদস্যপদ সংযোজন";
$lang['membership_list']     = "তালিকা";

$lang['membership_code']      = "কোড";
$lang['membership_name']      = "নাম";
$lang['membership_limit_book']  = "বুক লিমিট";
$lang['membership_limit_day']  = "ডে লিমিট";
$lang['membership_fee']  = "সদস্য ফি";
$lang['membership_penalty_fee']  = "পেনাল্টি ফি";
$lang['membership_renew_limit']  = "নবায়ন সীমা";
$lang['membership_free']  = "ফ্রি";
$lang['membership_status']     = "অবস্থা";
$lang['membership_action'] 	  = "ক্রিয়া";

$lang['membership_add_to_cart'] = "কার্ট যোগ করুন";
$lang['membership_insert'] = "ইনসার্ট";
$lang['membership_error'] = "Something is wrong";
$lang['membership_update'] = "আপডেট";

$lang['membership_note'] = "নোট:";
$lang['membership_penalty_note'] = "১. পেনাল্টি ফি দিন অনুযায়ী যুক্ত করা হবে।";
$lang['membership_renew_note'] = "২. নবায়ন সীমা বইয়ের জন্য প্রযোজ্য।";

?>