<?php 

$lang['genarateid_genarateid']     	 = "Tarjeta de identificación de Genarate";
$lang['genarateid_please_select']    = "Por favor seleccione";
$lang['genarateid_label']     	  	 = "Seleccionar miembro";
$lang['genarateid_generate']     	 = "Generar";
$lang['genarateid_tooltip']     	 = "Seleccionar varios miembros";
$lang['genarateid_details']       	 = "Detalles";
$lang['genarateid_print']       	 = "Descargar";
$lang['genarateid_name']       	 	 = "Nombre";
$lang['genarateid_joined']       	 = "unido";
$lang['genarateid_membership']       = "Afiliación";
$lang['genarateid_gender']       	 = "Género";

?>