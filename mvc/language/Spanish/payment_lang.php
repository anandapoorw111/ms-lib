<?php 

$lang['payment_payment']     	= "Pago";
$lang['payment_add_payment']    = "Agregar pago";
$lang['payment_list']     		= "Lista";
$lang['payment_member_name']    = "Nombre de miembro";
$lang['payment_for']     		= "Pago por";
$lang['payment_amount']     	= "Cantidad a pagar";
$lang['payment_date']     		= "Fecha";
$lang['payment_status']     	= "Estado";
$lang['payment_action'] 	  	= "Acción";
$lang['payment_receipt'] 	  	= "Recibo";
$lang['payment_money_receipt'] 	= "Recibo de dinero";
$lang['payment_email'] 			= "Correo electrónico";
$lang['payment_phone'] 			= "Número de contacto";
$lang['payment_mr'] 			= "Señor/Señora";
$lang['payment_from'] 	  		= "Efectivo recibido de";
$lang['payment_of'] 	  		= "De";
$lang['payment_for'] 	  		= "Para";
$lang['payment_inword'] 	  	= "En palabra";
$lang['payment_received_by'] 	= "Recibido por";

?>