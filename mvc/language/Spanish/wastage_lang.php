<?php 

$lang['wastage_wastage']     = "Pérdida";
$lang['wastage_add_wastage']     = "Agregar desperdicio";
$lang['wastage_list']     = "Lista";

$lang['wastage_photo']       = "Foto";
$lang['wastage_book_code']       = "Código del libro";
$lang['wastage_book_select']       = "Seleccione el nombre del libro";
$lang['wastage_book_name']  	  = "Nombre del libro";
$lang['wastage_writer_name']     = "Nombre del escritor";
$lang['wastage_wastager_by']     = "Desperdicio por";
$lang['wastage_date']     = "Fecha";
$lang['wastage_payable_amount']     = "Cantidad a pagar";
$lang['wastage_note']     = "Nota";
$lang['wastage_action'] 	  = "Acción";

$lang['wastage_insert'] = "Insertar";
$lang['wastage_update'] = "Actualizar";

?>