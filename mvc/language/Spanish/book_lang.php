<?php 

$lang['book_book']     			= "Libro";
$lang['book_add_book']     		= "Agregar libro";
$lang['book_update']     		= "Actualizar";
$lang['book_add']     			= "Agregar";
$lang['book_please_select']     = "Por favor seleccione";
$lang['book_info']     			= "Información del libro";
$lang['book_list']     			= "Lista";
$lang['book_name']     			= "Nombre";
$lang['book_categories']     	= "Categorías";
$lang['book_publication']     	= "Publicación";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "el escritor";
$lang['book_edition']     		= "Edición";
$lang['book_edition_year']     	= "Año de edición";
$lang['book_photo']     		= "Foto";
$lang['book_price']     		= "Precio";
$lang['book_quantity'] 			= "Cantidad";
$lang['book_availability'] 		= "Disponibilidad";
$lang['book_rack_no']    		= "Rack no.";
$lang['book_purchase_price']    = "Precio de compra";

$lang['book_details']     		= "Detalles";
$lang['book_entry']     		= "Entrada del libro";

$lang['book_code']     			= "Código";
$lang['book_status']     		= "Estado";
$lang['book_action'] 			= "Estado";
$lang['book_code_tooltip']		= "Tiene la oportunidad de usar su propio código para su biblioteca";
$lang['book_issued_quantity_tooltip']		= "Número de libros que se entregarán a los miembros";
$lang['book_issued_quantity']		= "Número de libros publicados";
$lang['isbn_tooltip']			= "International Standard Book Number";

?>