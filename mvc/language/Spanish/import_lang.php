<?php 

$lang['import_import']     			= "Importar archivo CSV";
$lang['import_list']     			= "Importar archivo por formato CSV";
$lang['import_note']     			= "Notas importantes";
$lang['download']     				= "Descargar";
$lang['select_file']     			= "Seleccionar archivo CSV";
$lang['upload_btn']     			= "Importar CSV";
$lang['import_note_1']     			= "Descargar formato de archivo CSV";
$lang['import_note_2']     			= "El título de la tabla del archivo CSV no se puede cambiar";
$lang['import_note_3']     			= "Usa el prefijo del código del libro correctamente";
$lang['import_note_4']     			= "ID de categoría de libro debe ser correcto";
$lang['import_note_5']     			= "La identificación del escritor del libro debe ser correcta";
$lang['import_note_6']     			= "La identificación de las publicaciones del libro debe ser correcta";

?>