<?php 

$lang['publication_publication']     = "Publicación";
$lang['publication_add_publication']     = "Agregar publicación";
$lang['publication_list']     = "Lista";

$lang['publication_name']       = "Nombre";
$lang['publication_note']  	  = "Nota";
$lang['publication_status']     = "Estado";
$lang['publication_action'] 	  = "Acción";

$lang['publication_insert'] = "Insertar";
$lang['publication_update'] = "Actualizar";

?>