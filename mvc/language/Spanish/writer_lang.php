<?php 

$lang['writer_writer']     = "el autor";
$lang['writer_add_writer']     = "Agregar escritor";
$lang['writer_list']     = "Lista";

$lang['writer_name']       = "Nombre";
$lang['writer_note']  	  = "Nota";
$lang['writer_status']     = "Estado";
$lang['writer_action'] 	  = "Acción";

$lang['writer_insert'] = "Insertar";
$lang['writer_update'] = "Actualizar";

?>