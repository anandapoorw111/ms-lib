<?php 

$lang['settings_settings']     = "Ajustes";
$lang['settings_general_setting']     = "Configuración general";
$lang['settings_list']     = "Configuración general";

$lang['settings_company_name']      = "nombre de empresa";
$lang['settings_tag_line']      = "Línea de etiqueta";
$lang['settings_type']      = "Tipo de negocio";
$lang['settings_owner']      = "Nombre del dueño";
$lang['settings_mobile']      = "Número de teléfono móvil";
$lang['settings_phone']      = "Número de teléfono";
$lang['settings_fax']      = "Número de fax";
$lang['settings_email']      = "Correo electrónico";
$lang['settings_tax']      = "Número de impuesto";
$lang['settings_address']      = "Habla a";
$lang['settings_time_zone']      = "Zona horaria";
$lang['settings_logo']      = "Logo de la compañía";
$lang['settings_currency_code']  = "Código de moneda";
$lang['settings_currency_symbol']  = "Símbolo de moneda";
$lang['settings_prefixes']  = "Prefijos";
$lang['category_prefixe']  = "Categoría";
$lang['member_prefixe']  = "Miembro";
$lang['book_prefixe']  = "Libro";
$lang['membership_prefixe']  = "Afiliación";
$lang['settings_language']  = "idioma";
$lang['settings_update']     = "Actualizar";
$lang['settings_action'] 	  = "Acción";

?>