<?php 
$lang['member_member']     			= "Miembro";
$lang['member_add_member']     		= "Añadir miembro";
$lang['member_profile']     		= "Perfil de miembro";
$lang['member_update']     			= "Actualizar";
$lang['member_add']     			= "Agregar";
$lang['member_please_select']     	= "Por favor seleccione";
$lang['member_info']     			= "Información del miembro";
$lang['member_list']     			= "Lista";
$lang['member_name']     			= "Nombre";
$lang['member_father_name']     	= "Nombre del Padre";
$lang['member_mother_name']     	= "Nombre de la madre";
$lang['member_date_of_birth']     	= "Fecha de nacimiento";
$lang['member_nid_no']     			= "Número de cédula de identidad nacional";
$lang['member_nid_no_tooltip']     	= "Número de Identificación Nacional";
$lang['member_membership']     		= "Afiliación";
$lang['member_membership_tooltip']	= "Seleccionar el tipo de membresía";
$lang['member_occupation']     		= "Ocupación";
$lang['member_parents']     		= "padre madre";
$lang['member_gender']     			= "Género";
$lang['member_male']     			= "Masculino";
$lang['member_female']     			= "Masculina";
$lang['member_third_gender']     	= "Otro género";
$lang['member_blood_group']     	= "Grupo sanguíneo";
$lang['member_religion']     		= "Religión";
$lang['member_phone']     			= "Número de contacto";
$lang['member_email']     			= "Correo electrónico";
$lang['member_since']     			= "La fecha registrada";
$lang['member_address']     		= "Habla a";
$lang['member_photo']     			= "Foto";
$lang['member_change_password']     = "Cambiar la contraseña";
$lang['member_new_password']     	= "Nueva contraseña";
$lang['member_confirm_password']    = "confirmar Contraseña";
$lang['member_login_info']     		= "Información de inicio de sesión";
$lang['member_username']     		= "Nombre de usuario";
$lang['member_password']     		= "Contraseña";
$lang['member_confirm_password']    = "confirmar Contraseña";
$lang['member_code']     			= "Código";
$lang['member_status']     			= "Estado";
$lang['member_action'] 				= "Acción";
$lang['member_details_list'] 		= "Detalles";
$lang['member_circulation']			= "Detalles de circulación";
$lang['member_payment_details']		= "Detalles del pago";

$lang['circulation_book_code']		= "Código del libro";
$lang['circulation_book_name']		= "Nombre del libro";
$lang['circulation_writer_name']	= "Nombre del escritor";
$lang['circulation_issue_date']		= "Fecha de emisión";
$lang['circulation_expiry_date']	= "Última fecha de regreso";
$lang['circulation_return_date']	= "Fecha de devolución";
$lang['circulation_penalty']		= "Multa";
$lang['circulation_return_status']	= "Estado";

$lang['payment_for']				= "Pago por";
$lang['payment_amount']				= "Monto";
$lang['payment_date']				= "Fecha";

?>