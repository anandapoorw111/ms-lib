<?php 

$lang['categories_categories']     = "Categorías";
$lang['categories_add_categories']     = "añadir categoría";
$lang['categories_list']     = "Lista";

$lang['categories_code']       = "Código";
$lang['categories_name']       = "Nombre";
$lang['categories_note']  	  = "Nota";
$lang['categories_status']     = "Estado";
$lang['categories_action'] 	  = "Acción";

$lang['categories_insert'] = "Insertar";
$lang['categories_update'] = "Actualizar";

?>