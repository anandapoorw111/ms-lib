<?php 

$lang['bookrequest_bookrequest']     	= "کتاب کی درخواست";
$lang['bookrequest_add_bookrequest']    = "درخواست شدہ کتاب";
$lang['bookrequest_list']     			= "فہرست";
$lang['bookrequest_name']       		= "کتاب کا نام";
$lang['bookrequest_writer_name']       	= "مصنف کا نام";
$lang['bookrequest_categories']       	= "اقسام";
$lang['bookrequest_edition']       		= "ایڈیشن";
$lang['bookrequest_note']  	  			= "نوٹ";
$lang['bookrequest_member']  	  		= "رکن کا نام";
$lang['bookrequest_action'] 	  		= "عمل";
$lang['bookrequest_insert'] 			= "داخل کریں";
$lang['bookrequest_update'] 			= "اپ ڈیٹ";

?>