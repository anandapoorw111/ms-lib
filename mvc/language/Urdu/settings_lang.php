<?php 

$lang['settings_settings']     = "ترتیبات";
$lang['settings_general_setting']     = "عام ترتیبات";
$lang['settings_list']     = "فہرست";

$lang['settings_company_name']      = "کمپنی کا نام";
$lang['settings_tag_line']      = "ٹیگ لائن";
$lang['settings_type']      = "کاروبار کی قسم";
$lang['settings_owner']      = "مالک کا نام";
$lang['settings_mobile']      = "موبائل نمبر.";
$lang['settings_phone']      = "فون نمبر.";
$lang['settings_fax']      = "فیکس نمبر";
$lang['settings_email']      = "ای میل";
$lang['settings_tax']      = "ٹیکس نمبر";
$lang['settings_address']      = "پتہ";
$lang['settings_time_zone']      = "ٹائم زون";
$lang['settings_logo']      = "کمپنی کا لوگو";
$lang['settings_currency_code']  = "رقم کا کوڈ";
$lang['settings_currency_symbol']  = "کرنسی کی علامت";
$lang['settings_prefixes']  = "سابقے";
$lang['category_prefixe']  = "قسم";
$lang['member_prefixe']  = "رکن";
$lang['book_prefixe']  = "کتاب";
$lang['membership_prefixe']  = "ممبرشپ";

$lang['settings_language']  = "زبان";

$lang['settings_update']     = "اپ ڈیٹ";
$lang['settings_action'] 	  = "عمل";

?>