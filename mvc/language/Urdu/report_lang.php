<?php 
$lang['reports_reports']     	= "رپورٹیں";
$lang['reports_member']     	= "ممبر رپورٹ";
$lang['reports_book']     		= "کتاب کی رپورٹ";
$lang['reports_payment']     	= "ادائیگی کی رپورٹ";
$lang['reports_categories']     = "زمرہ جات کی رپورٹ";
$lang['reports_details']     	= "تفصیلات یہاں";
$lang['reports_date']     		= "تاریخ";
$lang['report_print']     		= "پرنٹ کریں";
$lang['report_generated']     	= "رپورٹ بنائیں";
$lang['report_generate']     	= "پیدا کرنا";
$lang['report_status']     		= "حالت";

$lang['member_name']     		= "نام";
$lang['member_type']     		= "ٹائپ کریں";
$lang['member_occupation']     	= "قبضہ";
$lang['member_parents']     	= "والدین";
$lang['member_phone']     		= "رابطے کا نمبر.";
$lang['member_address']     	= "پتہ";
$lang['report_photo']     		= "تصویر";
$lang['report_code']     		= "کوڈ";
$lang['member_nid']     		= "قومی شناخت";
$lang['report_remark'] 			= "تبصرہ";

$lang['reports_please_select']     = "براہ مہربانی منتخب کریں";
$lang['reports_membership_type']   = "ممبرشپ کی قسم";
$lang['reports_member_Active']     = "فعال";
$lang['reports_member_Inactive']   = "غیر فعال";

$lang['reports_categories_type']   = "اقسام";

$lang['book_name']     			= "نام";
$lang['book_writer']     		= "لکھاری";
$lang['book_edition']     		= "ایڈیشن";
$lang['book_quantity'] 			= "مقدار";
$lang['book_availability'] 		= "دستیابی";
$lang['book_rack_no']    		= "ریک نمبر";
$lang['categories_name']     = "نام";
$lang['categories_book']     = "کتابیں";

$lang['report_payment']     = "ادائیگی کی رپورٹ";
$lang['reports_from_date']     = "اس تاریخ سے";
$lang['reports_to_date']     = "آج تک";
$lang['reports_for']     = "کے لئے";
$lang['reports_lost_book']     = "کھوئے ہوئے کتاب کی فیس";
$lang['reports_expiry_date']     = "واپسی کی آخری تاریخ سے تجاوز کرنے پر جرمانہ فیس";
$lang['reports_membership_fee']     = "رکنیت کی فیس";
$lang['payment_member_name']    = "رکن کا نام";
$lang['payment_for']     		= "ادائیگی";
$lang['payment_amount']     	= "قابل ادائیگی";
$lang['payment_date']     		= "تاریخ";

?>