<?php 

$lang['wastage_wastage']     = "بربادی";
$lang['wastage_add_wastage']     = "تباہی شامل کریں";
$lang['wastage_list']     = "فہرست";

$lang['wastage_photo']       = "تصویر";
$lang['wastage_book_code']       = "کتاب کا کوڈ";
$lang['wastage_book_select']       = "کتاب کا نام منتخب کریں";
$lang['wastage_book_name']  	  = "کتاب کا نام";
$lang['wastage_writer_name']     = "مصنف کا نام";
$lang['wastage_wastager_by']     = "تباہی سے";
$lang['wastage_date']     = "تاریخ";
$lang['wastage_payable_amount']     = "قابل ادائیگی";
$lang['wastage_note']     = "نوٹ";
$lang['wastage_action'] 	  = "عمل";

$lang['wastage_insert'] = "داخل کریں";
$lang['wastage_update'] = "اپ ڈیٹ";

?>