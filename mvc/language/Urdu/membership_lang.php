<?php 

$lang['membership_membership']     = "ممبرشپ";
$lang['membership_add_membership']     = "ممبرشپ شامل کریں";
$lang['membership_list']     = "فہرست";

$lang['membership_code']      = "کوڈ";
$lang['membership_name']      = "نام";
$lang['membership_limit_book']  = "کتاب کی حد";
$lang['membership_limit_day']  = "دن کی حد";
$lang['membership_fee']  = "رکنیت کی فیس";
$lang['membership_penalty_fee']  = "جرمانہ فیس";
$lang['membership_renew_limit']  = "تجدید کی حد";
$lang['membership_free']  = "مفت";
$lang['membership_status']     = "حالت";
$lang['membership_action'] 	  = "عمل";

$lang['membership_add_to_cart'] = "ٹوکری میں شامل کریں";
$lang['membership_insert'] = "داخل کریں";
$lang['membership_error'] = "کچھ غلط ہے";
$lang['membership_update'] = "اپ ڈیٹ";

$lang['membership_note'] = "نوٹ:";
$lang['membership_penalty_note'] = "1. پینلٹی فیس دن کے مطابق شامل کی جائے گی۔";
$lang['membership_renew_note'] = "2. تجدید کی حد کتاب پر لاگو ہوتی ہے۔ ";

?>