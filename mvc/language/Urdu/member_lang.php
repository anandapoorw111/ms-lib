<?php 
$lang['member_member']     			= "رکن";
$lang['member_add_member']     		= "ممبر شامل کریں";
$lang['member_profile']     		= "رکن پروفائل";
$lang['member_update']     			= "اپ ڈیٹ";
$lang['member_add']     			= "شامل کریں";
$lang['member_please_select']     	= "براہ مہربانی منتخب کریں";
$lang['member_info']     			= "ممبر کی معلومات";
$lang['member_list']     			= "فہرست";
$lang['member_name']     			= "نام";
$lang['member_father_name']     	= "والد کا نام";
$lang['member_mother_name']     	= "ماں کا نام";
$lang['member_date_of_birth']     	= "پیدائش کی تاریخ";
$lang['member_nid_no']     			= "قومی شناختی کارڈ نمبر";
$lang['member_nid_no_tooltip']     	= "قومی شناختی نمبر";
$lang['member_membership']     		= "ممبرشپ";
$lang['member_membership_tooltip']	= "ممبرشپ کی قسم منتخب کریں";
$lang['member_occupation']     		= "قبضہ";
$lang['member_parents']     		= "والدین";
$lang['member_gender']     			= "صنف";
$lang['member_male']     			= "مرد";
$lang['member_female']     			= "عورت";
$lang['member_third_gender']     	= "دوسرے";
$lang['member_blood_group']     	= "بلڈ گروپ";
$lang['member_religion']     		= "مذہب";
$lang['member_phone']     			= "رابطے کا نمبر.";
$lang['member_email']     			= "ای میل";
$lang['member_since']     			= "رجسٹرڈ تاریخ";
$lang['member_address']     		= "پتہ";
$lang['member_photo']     			= "تصویر";
$lang['member_change_password']     = "پاس ورڈ تبدیل کریں";
$lang['member_new_password']     	= "نیا پاس ورڈ";
$lang['member_confirm_password']    = "پاس ورڈ کی تصدیق کریں";
$lang['member_login_info']     		= "لاگ ان معلومات";
$lang['member_username']     		= "صارف نام";
$lang['member_password']     		= "پاس ورڈ";
$lang['member_confirm_password']    = "پاس ورڈ کی تصدیق کریں";
$lang['member_code']     			= "کوڈ";
$lang['member_status']     			= "حالت";
$lang['member_action'] 				= "عمل";
$lang['member_details_list'] 		= "تفصیلات";
$lang['member_circulation']			= "گردش کی تفصیلات";
$lang['member_payment_details']		= "ادائیگی کی تفصیلات";

$lang['circulation_book_code'] = "کتاب کا کوڈ";
$lang['circulation_book_name'] = "کتاب کا نام";
$lang['circulation_writer_name'] = "مصنف کا نام";
$lang['circulation_issue_date'] = "تاریخ اجراء";
$lang['circulation_expiry_date'] = "واپسی کی آخری تاریخ";
$lang['circulation_return_date'] = "واپسی کی تاریخ";
$lang['circulation_penalty']		= "جرمانہ";
$lang['circulation_return_status'] = "واپسی کی حیثیت";

$lang['payment_for']				= "ادائیگی";
$lang['payment_amount']				= "رقم";
$lang['payment_date']				= "تاریخ";

?>