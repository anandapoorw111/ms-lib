<?php 

$lang['login_username']    = "صارف نام";
$lang['login_password']    = "پاس ورڈ";
$lang['login_sign_in']     = "سائن ان";
$lang['retry']     = "لاگ ان کی دوبارہ کوشش کریں۔";
$lang['login_remember_me'] = "لاگ ان معلومات محفوظ کریں۔";
$lang['forgot_password'] = "میں اپنا پاسورڈ بھول گیا";
$lang['forgot_password_header'] = "پاسورڈ بھول گے";
$lang['forgot_password_sms'] = "اپنا پاس ورڈ یہاں ری سیٹ کریں۔";
$lang['forgot_email_address'] = "ای میل اڈریس";
$lang['reset_password'] = "پاس ورڈ ری سیٹ";
$lang['reset_password_sms'] = "اپنا نیا پاس ورڈ سیٹ کریں۔";
$lang['new_password'] = "نیا پاس ورڈ";
$lang['confirm_new_password'] = "نئے پاس ورڈ کی توثیق کریں";
$lang['validation'] = "تصدیق کا عمل۔";
$lang['validation_code'] = "تصدیقی کوڈ";
$lang['validation_sms'] = "براہ کرم ای میل سے توثیقی کوڈ کاپی کریں اور اسے یہاں پیسٹ کریں۔";
$lang['verify_button'] = "تصدیق کریں";
$lang['check_validation_code'] = "توثیقی کوڈ غلط ہے۔";

?>