<?php 

$lang['genarateid_genarateid']     	 = "صنف شناختی کارڈ";
$lang['genarateid_please_select']    = "براہ مہربانی منتخب کریں";
$lang['genarateid_label']     	  	 = "ممبر منتخب کریں";
$lang['genarateid_generate']     	 = "پیدا کرنا";
$lang['genarateid_tooltip']     	 = "متعدد ممبران کو منتخب کریں";
$lang['genarateid_details']       	 = "تفصیلات";
$lang['genarateid_print']       	 = "ڈاؤن لوڈ کریں";
$lang['genarateid_name']       	 	 = "نام";
$lang['genarateid_joined']       	 = "شامل ہوئے";
$lang['genarateid_membership']       = "ممبرشپ";
$lang['genarateid_gender']       	 = "صنف";

?>