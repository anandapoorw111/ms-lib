<?php 

$lang['login_username']    = "اسم المستخدم";
$lang['login_password']    = "كلمه السر";
$lang['login_sign_in']     = "تسجيل الدخول";
$lang['retry']     = "أعد محاولة تسجيل الدخول";
$lang['login_remember_me'] = "حفظ معلومات تسجيل الدخول";
$lang['forgot_password'] = "لقد نسيت كلمة المرور";
$lang['forgot_password_header'] = "هل نسيت كلمة السر";
$lang['forgot_password_sms'] = "أعد تعيين كلمة المرور الخاصة بك هنا";
$lang['forgot_email_address'] = "عنوان البريد الإلكتروني";
$lang['reset_password'] = "إعادة تعيين كلمة المرور";
$lang['reset_password_sms'] = "قم بتعيين كلمة المرور الجديدة الخاصة بك";
$lang['new_password'] = "كلمة مرور جديدة";
$lang['confirm_new_password'] = "تأكيد كلمة المرور الجديدة";
$lang['validation'] = "عملية التحقق";
$lang['validation_code'] = "شيفرة التأكيد";
$lang['validation_sms'] = "يرجى نسخ رمز التحقق من البريد الإلكتروني ولصقه هنا";
$lang['verify_button'] = "التحقق";
$lang['check_validation_code'] = "رمز التحقق غير صالح";

?>