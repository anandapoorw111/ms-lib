<?php 

$lang['circulation_circulation']     = "الدوران";
$lang['circulation_add_circulation']     = "إضافة فئة";
$lang['circulation_issue_and_return']     = "صدر وعاد";
$lang['circulation_list']     = "قائمة";

$lang['circulation_member_code'] = "كود العضو";
$lang['circulation_member_id'] = "معرف العضو";
$lang['circulation_member_name'] = "اسم عضو";
$lang['circulation_book_name'] = "اسم الكتاب";
$lang['circulation_book_code'] = "كود الكتاب";
$lang['circulation_book_code_tooltip'] = "ضع كود كتابك";
$lang['circulation_writer_name'] = "اسم الكاتب";
$lang['circulation_issue_date'] = "تاريخ الإصدار";
$lang['circulation_expiry_date'] = "تاريخ انتهاء العودة";
$lang['circulation_return_date'] = "تاريخ العودة";
$lang['circulation_return_status'] = "حالة العودة";
$lang['circulation_penalty'] = "ضربة جزاء";
$lang['circulation_penalty_fee'] = "رسوم جزاء";
$lang['circulation_no_of_days'] = "لا أيام";
$lang['circulation_penalty_message'] = "تاريخ عودة الكتاب قد انتهى. الرجاء دفع الغرامة.";
$lang['circulation_penalty_note'] = "ملحوظة";
$lang['member_penalty_amount'] = "إجمالي المبلغ المستحق";
$lang['circulation_action'] 	  = "عمل";


$lang['circulation_insert'] = "إدراج";
$lang['circulation_update'] = "تحديث";
$lang['circulation_search'] = "يبحث";
$lang['circulation_details'] = "تفاصيل";
$lang['circulation_issue'] = "كتاب الإصدار";
$lang['circulation_issued'] = "قضية";
$lang['circulation_not_available'] = "غير متاح";
$lang['circulation_member_search'] = "عضو البحث";

$lang['circulation_member_gender'] = "جنس";
$lang['circulation_member_occupation'] = "الاحتلال";
$lang['circulation_member_phone'] = "هاتف";
$lang['circulation_book_return'] = "إرجاع";
$lang['circulation_book_renewal'] = "التجديد";
$lang['circulation_book_lost'] = "ضائع";
$lang['circulation_book_lost_message'] = "منذ أن فقدت الكتاب ، عليك أن تدفع ثمن الكتاب.";
$lang['circulation_book_price'] = "سعر الكتاب";
$lang['circulation_book_payable_amount'] = "المبلغ المستحق";


$lang['book_photo'] = "صورة";
$lang['book_code'] = "شفرة";
$lang['book_name'] = "اسم";
$lang['book_writer'] = "كاتب";
$lang['book_edition'] = "الإصدار";
$lang['book_price'] = "سعر";
$lang['book_availability'] = "التوفر";
$lang['book_rack_no'] = "رف لا.";
$lang['book_action'] = "عمل";

?>