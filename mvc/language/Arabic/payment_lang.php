<?php 

$lang['payment_payment']     	= "دفع";
$lang['payment_add_payment']    = "إضافة الدفع";
$lang['payment_list']     		= "قائمة";
$lang['payment_member_name']    = "اسم عضو";
$lang['payment_for']     		= "الدفع مقابل";
$lang['payment_amount']     	= "المبلغ المستحق";
$lang['payment_date']     		= "تاريخ";
$lang['payment_status']     	= "حالة";
$lang['payment_action'] 	  	= "عمل";
$lang['payment_receipt'] 	  	= "إيصال";
$lang['payment_money_receipt'] 	= "استلام الأموال";
$lang['payment_email'] 			= "بريد إلكتروني";
$lang['payment_phone'] 			= "رقم الاتصال";
$lang['payment_mr'] 			= "السيد";
$lang['payment_from'] 	  		= "استلام النقد من";
$lang['payment_of'] 	  		= "دفع";
$lang['payment_for'] 	  		= "الدفع مقابل";
$lang['payment_inword'] 	  	= "بكلمات";
$lang['payment_received_by'] 	= "استلمت من قبل";
?>