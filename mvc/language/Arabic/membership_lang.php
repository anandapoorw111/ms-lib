<?php 

$lang['membership_membership']     = "عضوية";
$lang['membership_add_membership']     = "أضف عضوية";
$lang['membership_list']     = "قائمة";

$lang['membership_code']      = "شفرة";
$lang['membership_name']      = "اسم";
$lang['membership_limit_book']  = "حد إصدار الكتاب";
$lang['membership_limit_day']  = "حد اليوم";
$lang['membership_fee']  = "رسوم العضوية";
$lang['membership_penalty_fee']  = "رسوم جزاء";
$lang['membership_renew_limit']  = "حد تجديد إصدار الكتاب";
$lang['membership_free']  = "حر";
$lang['membership_status']     = "حالة";
$lang['membership_action'] 	  = "عمل";

$lang['membership_add_to_cart'] = "أضف إلى السلة";
$lang['membership_insert'] = "إدراج";
$lang['membership_error'] = "هناك شئ غير صحيح";
$lang['membership_update'] = "تحديث";

$lang['membership_note'] = "ملحوظة:";
$lang['membership_penalty_note'] = "1.سيتم إضافة رسوم الغرامة حسب أيام تجاوز الحد.";
$lang['membership_renew_note'] = "2. يسري حد التجديد على الكتاب.";

?>