<?php 
$lang['add']      = "يضيف";
$lang['edit']     = "يحرر";
$lang['view']     = "منظر";
$lang['delete']   = "حذف";

$lang['menu_dashboard']     = "لوحة القيادة";
$lang['menu_user']          = "المستخدمون";
$lang['menu_newuser']          = "المستعمل";
$lang['menu_usertype']      = "دور المستخدم";

$lang['menu_permissions']   = "أذونات";
$lang['menu_permissionlog'] = "سجل الأذونات";
$lang['menu_permissionmodule'] = "وحدة الإذن";
$lang['menu_menu']             = "لائحة الطعام";

$lang['menu_settings']  = "إعدادات";
$lang['menu_sitesettings']  = "ملف الشركة";
$lang['menu_membership']  = "عضوية";
$lang['menu_member']  = "عضو";
$lang['menu_memberlist']  = "قائمة الأعضاء";
$lang['menu_categories']  = "فئات";
$lang['menu_book']  = "الكتاب";
$lang['menu_book_archive']  = "أرشيف";
$lang['menu_wastage']  = "الهدر";
$lang['menu_publication']  = "النشر";
$lang['menu_circulation']  = "الدوران";
$lang['menu_payment']  = "دفع";
$lang['menu_report']  = "تقرير";
$lang['menu_writer']  = "كاتب";
$lang['menu_emailsetting']  = "تكوين البريد الإلكتروني";
$lang['menu_bookrequest']  = "طلب كتاب";
$lang['menu_genarateid']  = "إنشاء بطاقة الهوية";
$lang['menu_import']  = "استيراد ملف CSV";
?>