<?php 

$lang['publication_publication']     = "النشر";
$lang['publication_add_publication']     = "أضف المنشور";
$lang['publication_list']     = "قائمة";

$lang['publication_name']       = "اسم";
$lang['publication_note']  	  = "ملحوظة";
$lang['publication_status']     = "حالة";
$lang['publication_action'] 	  = "عمل";


$lang['publication_insert'] = "إدراج";
$lang['publication_update'] = "تحديث";

?>