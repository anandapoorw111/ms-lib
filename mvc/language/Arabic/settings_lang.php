<?php 

$lang['settings_settings']     = "إعدادات";
$lang['settings_general_setting']     = "الاعدادات العامة";
$lang['settings_list']     = "قائمة";

$lang['settings_company_name']      = "اسم الشركة";
$lang['settings_tag_line']      = "خط العلامة";
$lang['settings_type']      = "نوع العمل";
$lang['settings_owner']      = "اسم المالك";
$lang['settings_mobile']      = "رقم الهاتف المحمول";
$lang['settings_phone']      = "رقم الهاتف";
$lang['settings_fax']      = "رقم الفاكس";
$lang['settings_email']      = "بريد إلكتروني";
$lang['settings_tax']      = "الرقم الضريبي";
$lang['settings_address']      = "عنوان";
$lang['settings_time_zone']      = "وحدة زمنية";
$lang['settings_logo']      = "شعار الشركة";
$lang['settings_currency_code']  = "رمز العملة";
$lang['settings_currency_symbol']  = "رمز العملة";
$lang['settings_prefixes']  = "البادئات";
$lang['category_prefixe']  = "فئة";
$lang['member_prefixe']  = "عضو";
$lang['book_prefixe']  = "الكتاب";
$lang['membership_prefixe']  = "عضوية";

$lang['settings_language']  = "لغة";

$lang['settings_update']     = "تحديث";
$lang['settings_action'] 	  = "عمل";

?>