<?php 
$lang['reports_reports']     	= "التقارير";
$lang['reports_member']     	= "تقرير العضو";
$lang['reports_book']     		= "تقرير كتاب";
$lang['reports_payment']     	= "تقرير الدفع";
$lang['reports_categories']     = "تقرير الفئات";
$lang['reports_details']     	= "التفاصيل هنا";
$lang['reports_date']     		= "تاريخ";
$lang['report_print']     		= "مطبعة";
$lang['report_generated']     	= "إنشاء تقرير";
$lang['report_generate']     	= "يولد";
$lang['report_status']     		= "حالة";

$lang['member_name']     		= "اسم";
$lang['member_type']     		= "يكتب";
$lang['member_occupation']     	= "الاحتلال";
$lang['member_parents']     	= "آباء";
$lang['member_phone']     		= "رقم الاتصال";
$lang['member_address']     	= "عنوان";
$lang['report_photo']     		= "صورة";
$lang['report_code']     		= "شفرة";
$lang['member_nid']     		= "بطاقة الهوية الوطنية";
$lang['report_remark'] 			= "ملاحظة";

$lang['reports_please_select']     = "الرجاء التحديد";
$lang['reports_membership_type']   = "نوع العضوية";
$lang['reports_member_Active']     = "نشيط";
$lang['reports_member_Inactive']   = "غير نشط";


$lang['reports_categories_type']   = "فئات";

$lang['book_name']     			= "اسم";
$lang['book_writer']     		= "كاتب";
$lang['book_edition']     		= "الإصدار";
$lang['book_quantity'] 			= "كمية";
$lang['book_availability'] 		= "التوفر";
$lang['book_rack_no']    		= "رقم الرف";
$lang['categories_name']     = "اسم";
$lang['categories_book']     = "كتب";



$lang['report_payment']     = "تقرير الدفع";
$lang['reports_from_date']     = "من التاريخ";
$lang['reports_to_date']     = "حتى تاريخه";
$lang['reports_for']     = "ل";
$lang['reports_lost_book']     = "رسوم الكتاب المفقود";
$lang['reports_expiry_date']     = "تجاوز الحد رسوم الوقت";
$lang['reports_membership_fee']     = "رسوم العضوية";
$lang['payment_member_name']    = "اسم عضو";
$lang['payment_for']     		= "الدفع مقابل";
$lang['payment_amount']     	= "المبلغ المستحق";
$lang['payment_date']     		= "تاريخ";

?>