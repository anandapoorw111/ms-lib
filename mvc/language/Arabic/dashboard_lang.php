<?php 

$lang['dashboard_dashboard']     = "لوحة القيادة";
$lang['dashboard_more']  = "مزيد من المعلومات";
$lang['dashboard_member']     = "أعضاء";
$lang['dashboard_book'] 	  = "كتب";
$lang['dashboard_categories'] 	  = "فئات";
$lang['dashboard_membership'] = "عضوية";
$lang['dashboard_issued'] = "صادر";
$lang['dashboard_wastage'] = "الهدر";
$lang['dashboard_publication'] = "النشر";
$lang['dashboard_writer'] = "كاتب";
$lang['dashboard_bar_chat'] = "مخطط شريطي تم إصداره وإرجاعه";
$lang['dashboard_issued_book'] = "آخر 5 كتاب تم إصداره";
$lang['dashboard_returned_book'] = "آخر 5 كتاب مرتجع";
$lang['circulation_member_name'] = "اسم عضو";
$lang['circulation_book_name'] = "اسم الكتاب";
$lang['circulation_writer_name'] = "اسم الكاتب";
$lang['circulation_issue_date'] = "تاريخ الإصدار";
$lang['circulation_expiry_date'] = "تاريخ التراجع الأخير";
$lang['circulation_return_date'] = "تاريخ العودة";
$lang['categories_code'] = "شفرة";
$lang['categories_name'] = "اسم";

?>