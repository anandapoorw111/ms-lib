<?php 

$lang['usertype']     = "دور المستخدم";
$lang['usertype_list']     = "قائمة";
$lang['usertype_add_usertype']     = "إضافة دور المستخدم";
$lang['usertype_action'] = "عمل";
$lang['usertype_update'] = "تحديث";
$lang['usertype_add']     = "يضيف";
$lang['usertype_name']     = "اسم";

$lang['user_dob']      = "تاريخ الولادة";
$lang['user_gender']   = "جنس";
$lang['user_please_select']   = "الرجاء التحديد";
$lang['user_male']   = "ذكر";
$lang['user_female']   = "أنثى";
$lang['user_religion'] = "دين";
$lang['user_email']    = "بريد إلكتروني";
$lang['user_phone']    = "هاتف";
$lang['user_address']  = "عنوان";
$lang['user_jod']      = "تاريخ الالتحاق";
$lang['user_photo']    = "صورة";
$lang['user_active']   = "نشيط";
$lang['user_usertypeID'] = "معرف نوع المستخدم";
$lang['user_username'] = "اسم المستخدم";
$lang['user_password'] = "كلمه السر";
$lang['user_role'] 	= "دور";
$lang['user_status'] = "حالة";
$lang['user_active'] = "تفعيل";
$lang['user_deactive'] = "تعطيل";
$lang['user_profile'] = "الملف الشخصي";
$lang['user_change_password'] = "تغيير كلمة المرور";
$lang['user_new_password'] = "كلمة السر الجديدة";
$lang['user_confirm_password'] = "تأكيد كلمة المرور";

?>