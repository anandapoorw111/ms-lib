<?php 

$lang['import_import']     			= "استيراد ملف CSV";
$lang['import_list']     			= "استيراد ملف بتنسيق CSV";
$lang['import_note']     			= "ملاحظات هامة";
$lang['download']     				= "تحميل";
$lang['select_file']     			= "حدد ملف CSV";
$lang['upload_btn']     			= "استيراد ملف CSV";
$lang['import_note_1']     			= "تنزيل تنسيق ملف CSV";
$lang['import_note_2']     			= "لا يمكن تغيير عنوان الجدول الخاص بملف CSV";
$lang['import_note_3']     			= "استخدم بادئة كود الكتاب بشكل صحيح";
$lang['import_note_4']     			= "يجب أن يكون معرف فئة الكتاب صحيحًا";
$lang['import_note_5']     			= "يجب أن يكون معرف كاتب الكتاب صحيحًا";
$lang['import_note_6']     			= "يجب أن يكون معرف منشورات الكتاب صحيحًا";

?>