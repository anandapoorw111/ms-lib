<?php 

$lang['login_username']    = "Nom d'utilisateur";
$lang['login_password']    = "Mot de passe";
$lang['login_sign_in']     = "S'identifier";
$lang['retry']     = "Réessayer la connexion";
$lang['login_remember_me'] = "enregistrer les informations de connexion";
$lang['forgot_password'] = "j'ai oublié mon mot de passe";
$lang['forgot_password_header'] = "Mot de passe oublié";
$lang['forgot_password_sms'] = "Réinitialiser votre mot de passe ici";
$lang['forgot_email_address'] = "Adresse e-mail";
$lang['reset_password'] = "réinitialiser le mot de passe";
$lang['reset_password_sms'] = "Définissez votre nouveau mot de passe";
$lang['new_password'] = "nouveau mot de passe";
$lang['confirm_new_password'] = "Confirmer le nouveau mot de passe";
$lang['validation'] = "Processus de vérification";
$lang['validation_code'] = "le code de vérification";
$lang['validation_sms'] = "Veuillez copier le code de validation de l'e-mail et le coller ici";
$lang['verify_button'] = "Vérifier";
$lang['check_validation_code'] = "Le code de validation est invalide";

?>