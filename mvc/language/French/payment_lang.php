<?php 

$lang['payment_payment']     	= "Paiement";
$lang['payment_add_payment']    = "Ajouter un paiement";
$lang['payment_list']     		= "Lister";
$lang['payment_member_name']    = "Nom de membre";
$lang['payment_for']     		= "Paiement pour";
$lang['payment_amount']     	= "Montant payable";
$lang['payment_date']     		= "Date";
$lang['payment_status']     	= "Statut";
$lang['payment_action'] 	  	= "action";
$lang['payment_receipt'] 	  	= "Reçu";
$lang['payment_money_receipt'] 	= "Reçu d'argent";
$lang['payment_email'] 			= "E-mail";
$lang['payment_phone'] 			= "Numéro de contact";
$lang['payment_mr'] 			= "M";
$lang['payment_from'] 	  		= "Espèces reçues de";
$lang['payment_of'] 	  		= "De";
$lang['payment_for'] 	  		= "Pour";
$lang['payment_inword'] 	  	= "Dans Word";
$lang['payment_received_by'] 	= "Reçu par";
?>