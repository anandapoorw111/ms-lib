<?php 
$lang['add']      = "Ajouter";
$lang['edit']     = "Éditer";
$lang['view']     = "Vue";
$lang['delete']   = "Effacer";

$lang['menu_dashboard']     = "Tableau de bord";
$lang['menu_user']          = "Utilisateurs";
$lang['menu_newuser']          = "Utilisateur";
$lang['menu_usertype']      = "Rôle d'utilisateur";

$lang['menu_permissions']   = "Autorisations";
$lang['menu_permissionlog'] = "Journal des autorisations";
$lang['menu_permissionmodule'] = "Module d'autorisation";
$lang['menu_menu']             = "Menu";

$lang['menu_settings']  = "Paramètres";
$lang['menu_sitesettings']  = "Profil de la société";
$lang['menu_membership']  = "Adhésion";
$lang['menu_member']  = "Membre";
$lang['menu_memberlist']  = "Liste des membres";
$lang['menu_categories']  = "Catégories";
$lang['menu_book']  = "Livre";
$lang['menu_book_archive']  = "Archiver";
$lang['menu_wastage']  = "Gaspillage";
$lang['menu_publication']  = "Publication";
$lang['menu_circulation']  = "Circulation";
$lang['menu_payment']  = "Paiement";
$lang['menu_report']  = "Signaler";
$lang['menu_writer']  = "l' auteur";
$lang['menu_emailsetting']  = "Configuration de la messagerie";
$lang['menu_bookrequest']  = "Demande de livre";
$lang['menu_genarateid']  = "Générer une carte d'identité";
$lang['menu_import']  = "Importer un fichier CSV";
?>