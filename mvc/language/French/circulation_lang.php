<?php 

$lang['circulation_circulation']     = "Circulation";
$lang['circulation_add_circulation']     = "ajouter une catégorie";
$lang['circulation_issue_and_return']     = "ajouter une catégorie";
$lang['circulation_list']     = "Lister";

$lang['circulation_member_code'] = "Code membre";
$lang['circulation_member_id'] = "ID membres";
$lang['circulation_member_name'] = "Nom de membre";
$lang['circulation_book_name'] = "Nom du livre";
$lang['circulation_book_code'] = "Code du livre";
$lang['circulation_book_code_tooltip'] = "Mettez votre code de livre";
$lang['circulation_writer_name'] = "Nom de l'écrivain";
$lang['circulation_issue_date'] = "Date d'émission";
$lang['circulation_expiry_date'] = "Date d'expiration";
$lang['circulation_return_date'] = "Date de retour";
$lang['circulation_return_status'] = "Statut de retour";
$lang['circulation_penalty'] = "Peine";
$lang['circulation_penalty_fee'] = "Frais de pénalité";
$lang['circulation_no_of_days'] = "nombre de jours";
$lang['circulation_penalty_message'] = "La date de retour de ce livre est dépassée. Veuillez payer la pénalité.";
$lang['circulation_penalty_note'] = "Noter";
$lang['member_penalty_amount'] = "Montant total";
$lang['circulation_action'] 	  = "action";


$lang['circulation_insert'] = "Insérer";
$lang['circulation_update'] = "Mettre à jour";
$lang['circulation_search'] = "Rechercher";
$lang['circulation_details'] = "Des détails";
$lang['circulation_issue'] = "Livre d'émission";
$lang['circulation_issued'] = "Publier";
$lang['circulation_not_available'] = "Indisponible";
$lang['circulation_member_search'] = "Rechercher un membre";

$lang['circulation_member_gender'] = "Genre";
$lang['circulation_member_occupation'] = "Occupation";
$lang['circulation_member_phone'] = "Téléphoner";
$lang['circulation_book_return'] = "Revenir";
$lang['circulation_book_renewal'] = "Renouvellement";
$lang['circulation_book_lost'] = "disparu";
$lang['circulation_book_lost_message'] = "Puisque vous avez perdu le livre, vous devez payer le prix du livre.";
$lang['circulation_book_price'] = "Prix ​​du livre";
$lang['circulation_book_payable_amount'] = "Montant payable";


$lang['book_photo'] = "photo";
$lang['book_code'] = "Code";
$lang['book_name'] = "Nom";
$lang['book_writer'] = "l' auteur";
$lang['book_edition'] = "Édition";
$lang['book_price'] = "Prix";
$lang['book_availability'] = "Disponibilité";
$lang['book_rack_no'] = "N° de crémaillère";
$lang['book_action'] = "action";

?>