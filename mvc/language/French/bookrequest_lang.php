<?php 

$lang['bookrequest_bookrequest']     	= "demande de livre";
$lang['bookrequest_add_bookrequest']    = "Livre demandé";
$lang['bookrequest_list']     			= "Lister";
$lang['bookrequest_name']       		= "Nom du livre";
$lang['bookrequest_writer_name']       	= "Nom de l'écrivain";
$lang['bookrequest_categories']       	= "Catégories";
$lang['bookrequest_edition']       		= "Édition";
$lang['bookrequest_note']  	  			= "Noter";
$lang['bookrequest_member']  	  		= "Nom de membre";
$lang['bookrequest_action'] 	  		= "Action";
$lang['bookrequest_insert'] 			= "Insérer";
$lang['bookrequest_update'] 			= "Mettre à jour";

?>