<?php 

$lang['wastage_wastage']     = "Gaspillage";
$lang['wastage_add_wastage']     = "Ajouter du gaspillage";
$lang['wastage_list']     = "Lister";

$lang['wastage_photo']       = "photo";
$lang['wastage_book_code']       = "Code du livre";
$lang['wastage_book_select']       = "Sélectionnez le nom du livre";
$lang['wastage_book_name']  	  = "Nom du livre";
$lang['wastage_writer_name']     = "Nom de l'écrivain";
$lang['wastage_wastager_by']     = "Gaspillage par";
$lang['wastage_date']     = "Date";
$lang['wastage_payable_amount']     = "Montant payable";
$lang['wastage_note']     = "Noter";
$lang['wastage_action'] 	  = "action";


$lang['wastage_insert'] = "Insérer";
$lang['wastage_update'] = "Mettre à jour";

?>