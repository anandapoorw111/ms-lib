<?php 

$lang['publication_publication']     = "Publication";
$lang['publication_add_publication']     = "Ajouter une publication";
$lang['publication_list']     = "Lister";

$lang['publication_name']       = "Nom";
$lang['publication_note']  	  = "Noter";
$lang['publication_status']     = "Statut";
$lang['publication_action'] 	  = "action";

$lang['publication_insert'] = "Insérer";
$lang['publication_update'] = "Mettre à jour";

?>