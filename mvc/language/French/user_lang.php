<?php 

$lang['user_user']     = "Utilisateur";
$lang['user_add_user']     = "Ajouter un utilisateur";
$lang['user_add']     = "Ajouter";
$lang['user_list']     = "Lister";
$lang['user_name']     = "Nom";
$lang['user_dob']      = "Date de naissance";
$lang['user_gender']   = "Genre";
$lang['user_please_select']   = "Veuillez sélectionner";
$lang['user_male']   = "Mâle";
$lang['user_female']   = "Femelle";
$lang['user_religion'] = "Religion";
$lang['user_email']    = "E-mail";
$lang['user_phone']    = "Téléphoner";
$lang['user_address']  = "Adresse";
$lang['user_jod']      = "Date d'adhésion";
$lang['user_photo']    = "photo";
$lang['user_active']   = "Actif";
$lang['user_usertypeID'] = "Identifiant du type d'utilisateur";
$lang['user_username'] = "Nom d'utilisateur";
$lang['user_password'] = "Mot de passe";
$lang['user_role'] 	= "Rôle";
$lang['user_status'] = "Statut";
$lang['user_active'] = "Activer";
$lang['user_deactive'] = "Désactiver";
$lang['user_action'] = "action";
$lang['user_update'] = "Mettre à jour";
$lang['user_profile'] = "Profil";
$lang['user_change_password'] = "Changer le mot de passe";
$lang['user_new_password'] = "nouveau mot de passe";
$lang['user_confirm_password'] = "Confirmez le mot de passe";

?>