<?php 

$lang['membership_membership']     = "Adhésion";
$lang['membership_add_membership']     = "Ajouter une adhésion";
$lang['membership_list']     = "Lister";

$lang['membership_code']      = "Code";
$lang['membership_name']      = "Nom";
$lang['membership_limit_book']  = "Limite d'émission de livres";
$lang['membership_limit_day']  = "Limite de jours";
$lang['membership_fee']  = "Cotisation";
$lang['membership_penalty_fee']  = "Frais de pénalité";
$lang['membership_renew_limit']  = "Limite de renouvellement";
$lang['membership_free']  = "Libérer";
$lang['membership_status']     = "Statut";
$lang['membership_action'] 	  = "action";

$lang['membership_add_to_cart'] = "Ajouter au panier";
$lang['membership_insert'] = "Insérer";
$lang['membership_error'] = "Quelque chose ne va pas";
$lang['membership_update'] = "Mettre à jour";

$lang['membership_note'] = "Noter:";
$lang['membership_penalty_note'] = "1. Des frais de pénalité seront ajoutés selon les jours.";
$lang['membership_renew_note'] = "2. La limite de renouvellement s'applique au livre.";

?>