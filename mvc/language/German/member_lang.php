<?php 
$lang['member_member']     			= "Mitglied";
$lang['member_add_member']     		= "Mitglied hinzufügen";
$lang['member_profile']     		= "Mitgliedsprofil";
$lang['member_update']     			= "Aktualisieren";
$lang['member_add']     			= "Hinzufügen";
$lang['member_please_select']     	= "Bitte auswählen";
$lang['member_info']     			= "Mitgliederinformation";
$lang['member_list']     			= "Aufführen";
$lang['member_name']     			= "Name";
$lang['member_father_name']     	= "Der Name des Vaters";
$lang['member_mother_name']     	= "Name der Mutter";
$lang['member_date_of_birth']     	= "Date of Birth";
$lang['member_nid_no']     			= "Nationaler Personalausweis";
$lang['member_nid_no_tooltip']     	= "Nationale Identifikationsnummer";
$lang['member_membership']     		= "Mitgliedschaft";
$lang['member_membership_tooltip']	= "Wählen Sie die Art der Mitgliedschaft aus";
$lang['member_occupation']     		= "Besetzung";
$lang['member_parents']     		= "Eltern";
$lang['member_gender']     			= "Geschlecht";
$lang['member_male']     			= "Männlich";
$lang['member_female']     			= "Weiblich";
$lang['member_third_gender']     	= "Andere";
$lang['member_blood_group']     	= "Blutgruppe";
$lang['member_religion']     		= "Religion";
$lang['member_phone']     			= "Kontakt Nummer";
$lang['member_email']     			= "Email";
$lang['member_since']     			= "Eingetragenes Datum";
$lang['member_address']     		= "Adresse";
$lang['member_photo']     			= "Foto";
$lang['member_change_password']     = "Kennwort ändern";
$lang['member_new_password']     	= "Neues Kennwort";
$lang['member_confirm_password']    = "Kennwort bestätigen";
$lang['member_login_info']     		= "Anmeldeinformationen";
$lang['member_username']     		= "Nutzername";
$lang['member_password']     		= "Passwort";
$lang['member_confirm_password']    = "Kennwort bestätigen";
$lang['member_code']     			= "Code";
$lang['member_status']     			= "Status";
$lang['member_action'] 				= "Aktion";
$lang['member_details_list'] 		= "Einzelheiten";
$lang['member_circulation']			= "Auflagendetails";
$lang['member_payment_details']		= "Zahlungsdetails";

$lang['circulation_book_code']		= "Buchcode";
$lang['circulation_book_name']		= "Buchname";
$lang['circulation_writer_name']	= "Autorname";
$lang['circulation_issue_date']		= "Ausstellungsdatum";
$lang['circulation_expiry_date']	= "Letztes Datum der Rückkehr";
$lang['circulation_return_date']	= "Rückgabedatum";
$lang['circulation_penalty']		= "Elfmeter";
$lang['circulation_return_status']	= "Status";

$lang['payment_for']				= "Zahlung für";
$lang['payment_amount']				= "der Betrag";
$lang['payment_date']				= "Datum";

?>