<?php 
$lang['reports_reports']     	= "Berichte";
$lang['reports_member']     	= "Mitgliedsbericht";
$lang['reports_book']     		= "Buchbericht";
$lang['reports_payment']     	= "Zahlungsbericht";
$lang['reports_categories']     = "Kategorien Bericht";
$lang['reports_details']     	= "Details hier";
$lang['reports_date']     		= "Datum";
$lang['report_print']     		= "Drucken";
$lang['report_generated']     	= "Bericht generieren";
$lang['report_generate']     	= "Generieren";
$lang['report_status']     		= "Status";

$lang['member_name']     		= "Name";
$lang['member_type']     		= "Art";
$lang['member_occupation']     	= "Besetzung";
$lang['member_parents']     	= "Eltern";
$lang['member_phone']     		= "Kontakt Nummer";
$lang['member_address']     	= "Adresse";
$lang['report_photo']     		= "Foto";
$lang['report_code']     		= "Code";
$lang['member_nid']     		= "Nationale Identifikationsnummer";
$lang['report_remark'] 			= "Anmerkung";

$lang['reports_please_select']     = "Bitte auswählen";
$lang['reports_membership_type']   = "Art der Mitgliedschaft";
$lang['reports_member_Active']     = "Aktiv";
$lang['reports_member_Inactive']   = "Inaktiv";

$lang['reports_categories_type']   = "Kategorien";

$lang['book_name']     			= "Name";
$lang['book_writer']     		= "der Autor";
$lang['book_edition']     		= "Auflage";
$lang['book_quantity'] 			= "Menge";
$lang['book_availability'] 		= "Verfügbarkeit";
$lang['book_rack_no']    		= "Racknummer";
$lang['categories_name']     = "Name";
$lang['categories_book']     = "Bücher";

$lang['report_payment']     = "Zahlungsbericht";
$lang['reports_from_date']     = "Ab Datum";
$lang['reports_to_date']     = "Miteinander ausgehen";
$lang['reports_for']     = "Zum";
$lang['reports_lost_book']     = "Gebühr für verlorene Bücher";
$lang['reports_expiry_date']     = "Rückgabedatum überschreitet Gebühr exceed";
$lang['reports_membership_fee']     = "Mitgliedsbeitrag";
$lang['payment_member_name']    = "Mitgliedsname";
$lang['payment_for']     		= "Zahlung für";
$lang['payment_amount']     	= "Zu zahlender Betrag";
$lang['payment_date']     		= "Datum";

?>