<?php 

$lang['writer_writer']     = "der Autor";
$lang['writer_add_writer']     = "Autor hinzufügen";
$lang['writer_list']     = "Aufführen";

$lang['writer_name']       = "Name";
$lang['writer_note']  	  = "Hinweis";
$lang['writer_status']     = "Status";
$lang['writer_action'] 	  = "Aktion";

$lang['writer_insert'] = "Einfügen";
$lang['writer_update'] = "Aktualisieren";

?>