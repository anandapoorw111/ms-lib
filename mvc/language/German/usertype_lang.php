<?php 

$lang['usertype']     = "Benutzer-Rolle";
$lang['usertype_list']     = "Aufführen";
$lang['usertype_add_usertype']     = "Benutzerrolle hinzufügen";
$lang['usertype_action'] = "Aktion";
$lang['usertype_update'] = "Aktualisieren";
$lang['usertype_add']     = "Hinzufügen";
$lang['usertype_name']     = "Name";

$lang['user_dob']      = "Geburtsdatum";
$lang['user_gender']   = "Geschlecht";
$lang['user_please_select']   = "Bitte auswählen";
$lang['user_male']   = "Männlich";
$lang['user_female']   = "Weiblich";
$lang['user_religion'] = "Religion";
$lang['user_email']    = "Email";
$lang['user_phone']    = "Telefon";
$lang['user_address']  = "Adresse";
$lang['user_jod']      = "Eintrittsdatum";
$lang['user_photo']    = "Foto";
$lang['user_active']   = "Aktiv";
$lang['user_usertypeID'] = "Benutzertyp-ID";
$lang['user_username'] = "Nutzername";
$lang['user_password'] = "Passwort";
$lang['user_role'] 	= "Rolle";
$lang['user_status'] = "Status";
$lang['user_active'] = "aktivieren Sie";
$lang['user_deactive'] = "Deaktivieren";
$lang['user_profile'] = "Profil";
$lang['user_change_password'] = "Kennwort ändern";
$lang['user_new_password'] = "Neues Kennwort";
$lang['user_confirm_password'] = "Kennwort bestätigen";

?>