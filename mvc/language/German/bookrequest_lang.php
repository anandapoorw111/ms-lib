<?php 

$lang['bookrequest_bookrequest']     	= "Buchungsanfrage";
$lang['bookrequest_add_bookrequest']    = "Angefordertes Buch";
$lang['bookrequest_list']     			= "Aufführen";
$lang['bookrequest_name']       		= "Buchname";
$lang['bookrequest_writer_name']       	= "Autorname";
$lang['bookrequest_categories']       	= "Kategorien";
$lang['bookrequest_edition']       		= "Auflage";
$lang['bookrequest_note']  	  			= "Hinweis";
$lang['bookrequest_member']  	  		= "Mitgliedsname";
$lang['bookrequest_action'] 	  		= "Einfügen";
$lang['bookrequest_insert'] 			= "Einfügen";
$lang['bookrequest_update'] 			= "Aktualisieren";

?>