<?php 

$lang['genarateid_genarateid']     	 = "Ausweis erstellen";
$lang['genarateid_please_select']    = "Bitte auswählen";
$lang['genarateid_label']     	  	 = "Mitglied auswählen";
$lang['genarateid_generate']     	 = "Generieren";
$lang['genarateid_tooltip']     	 = "Mehrere Mitglieder auswählen";
$lang['genarateid_details']       	 = "Einzelheiten";
$lang['genarateid_print']       	 = "Herunterladen";
$lang['genarateid_name']       	 	 = "Name";
$lang['genarateid_joined']       	 = "Beigetreten";
$lang['genarateid_membership']       = "Mitgliedschaft";
$lang['genarateid_gender']       	 = "Geschlecht";

?>