<?php 

$lang['categories_categories']     = "Kategorien";
$lang['categories_add_categories']     = "Kategorie hinzufügen";
$lang['categories_list']     = "Aufführen";

$lang['categories_code']       = "Code";
$lang['categories_name']       = "Name";
$lang['categories_note']  	  = "Hinweis";
$lang['categories_status']     = "Status";
$lang['categories_action'] 	  = "Aktion";

$lang['categories_insert'] = "Einfügen";
$lang['categories_update'] = "Aktualisieren";

?>