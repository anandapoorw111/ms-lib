<?php 

$lang['import_import']     			= "CSV-Datei importieren";
$lang['import_list']     			= "Datei im CSV-Format importieren";
$lang['import_note']     			= "Wichtige Notizen";
$lang['download']     				= "Herunterladen";
$lang['select_file']     			= "CSV-Datei auswählen";
$lang['upload_btn']     			= "CSV-Datei importieren";
$lang['import_note_1']     			= "CSV-Dateiformat herunterladen";
$lang['import_note_2']     			= "Der Tabellentitel der CSV-Datei kann nicht geändert werden";
$lang['import_note_3']     			= "Verwenden Sie das Präfix des Buchcodes richtig";
$lang['import_note_4']     			= "Buchkategorie-ID muss korrekt sein";
$lang['import_note_5']     			= "Buchautor-ID muss korrekt sein";
$lang['import_note_6']     			= "Buchveröffentlichungs-ID muss korrekt sein";

?>