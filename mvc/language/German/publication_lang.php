<?php 

$lang['publication_publication']     = "Veröffentlichung";
$lang['publication_add_publication']     = "Publikation hinzufügen";
$lang['publication_list']     = "Aufführen";

$lang['publication_name']       = "Name";
$lang['publication_note']  	  = "Hinweis";
$lang['publication_status']     = "Status";
$lang['publication_action'] 	  = "Aktion";

$lang['publication_insert'] = "Einfügen";
$lang['publication_update'] = "Aktualisieren";

?>