<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	protected $_table_name  = '';
	protected $_primary_key = '';
	protected $_order_by    = '';

	function __construct() {
		parent::__construct();
	}

	public function get($array=NULL, $single=FALSE){
		if (inicompute($array) && is_array($array)) {
			$this->db->select($array);
		} else {
			$this->db->select('*');
		}
		if($single) {
			$method= "row";
		} else {
			$method= "result";
		}
		if(!empty($this->_order_by)) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
	}

	public function get_order_by($wherearray=NULL, $array=NULL, $single=FALSE) {
		if (inicompute($array) && is_array($array)) {
			$this->db->select($array);
		} else {
			$this->db->select('*');
		}
		if($single) {
			$method= "row";
		} else {
			$method= "result";
		}
		if(inicompute($wherearray) && is_array($wherearray)) {
			$this->db->where($wherearray);
		}
		if(!empty($this->_order_by)) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
	}

	public function get_single($wherearray=NULL, $array=NULL, $single=TRUE) {
		if (inicompute($array) && is_array($array)) {
			$this->db->select($array);
		} else {
			$this->db->select('*');
		}
		if($single) {
			$method= "row";
		} else {
			$method= "result";
		}
		if(inicompute($wherearray) && is_array($wherearray)) {
			$this->db->where($wherearray);
		} elseif((int)$wherearray) {
			$this->db->where($this->_primary_key,$wherearray);
		}

		if(!empty($this->_order_by)) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
	}

	public function insert($array) {
		$this->db->insert($this->_table_name, $array);
		return $this->db->insert_id();
	}


	public function update($array, $id) {
		if((int)$id){
			$this->db->set($array);
			$this->db->where($this->_primary_key, $id);
			return $this->db->update($this->_table_name);
		}
	}

	public function delete($id){
		if ((int)$id){
			$this->db->where($this->_primary_key, $id);
			$this->db->limit(1);
			return $this->db->delete($this->_table_name);
		}
	}


	public function insert_batch($array) {
		$this->db->insert_batch($this->_table_name, $array); 
        return $this->db->insert_id();
	}

	public function update_batch($array, $column) {
		$this->db->update_batch($this->_table_name, $array, $column); 
        return TRUE;
	}

	public function hash($string) {
		return hash("sha512", $string . config_item("encryption_key"));
	}

}
