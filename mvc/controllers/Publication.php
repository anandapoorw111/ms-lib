<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('publication_m');
		$lang = settings()->language;
		$this->lang->load('publication', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/publication.js',
				)
			);
		$this->data['publication'] = $this->publication_m->get_publication();
		$this->data['title'] = 'Publication';
		$this->data["subview"] = "publication/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'publication_name',
				'label'=> $this->lang->line('publication_name'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'publication_note',
				'label'=> $this->lang->line('publication_note'),
				'rules'=> 'trim|max_length[60]',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {

				$array = array(
					'publication_name'    => $this->input->post('publication_name'),
					'publication_note'  	=> $this->input->post('publication_note'),
					'publication_status'  => '1',
					'create_date'      	=> date('Y-m-d H:i:s'),
					'create_userID'    	=> $this->session->userdata('userID'),
					'create_usertypeID'	=> $this->session->userdata('usertypeID'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->publication_m->insert_publication($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('publication'));
		}
	}


	public function edit(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'publication_name'    => $this->input->post('publication_name'),
					'publication_note'  	=> $this->input->post('publication_note'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->publication_m->update_publication($array, $this->input->post('id'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			} 
		}else {
			redirect(base_url('expenses'));
		}
	}

	public function retrive() {
		$publicationID = $this->input->post('id');
		if((int)$publicationID){
			$get_data = $this->publication_m->get_single_publication(array('publicationID' => $publicationID));
			if(inicompute($get_data)){
				$json = array(
					"confirmation" => 'Success',
					'id' => $get_data->publicationID, 
					'publication_name' => $get_data->publication_name,
					'publication_note' => $get_data->publication_note,
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}else {
			redirect(base_url('publication'));
		}
	}

	public function status(){
		$array = array(
			'publication_status'    => $this->input->post('status'),
			);
		$this->publication_m->update_publication($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
		
	}

	public function delete() {
		if($this->publication_m->delete_publication($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}
}
