<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('dashboard_m');
		$this->load->model('member_m');
		$this->load->model('book_m');
		$this->load->model('categories_m');
		$this->load->model('membership_m');
		$this->load->model('circulation_m');
		$this->load->model('wastage_m');
		$this->load->model('publication_m');
		$this->load->model('writer_m');
		$lang = settings()->language;
		$this->lang->load('dashboard', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
			),
			'js' => array(
				'assets/bower_components/chart.js/Chart.js',
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				// 'assets/pages_js/dashboard.js',
			)
		); 
		$this->data['yearlyissued'] = $this->issued();
		$this->data['yearlyreturned'] = $this->returned();
		$this->data['member'] = $this->member_m->get_order_by(array('member_status' => '1'));
		$this->data['book'] = $this->book_m->get_order_by(array('book_status' => '1'));
		$this->data['categories'] = $this->categories_m->get_order_by(array('categories_status' => '1'));
		$this->data['membership'] = $this->membership_m->get_order_by(array('membership_status' => '1'));
		$this->data['publication'] = $this->publication_m->get_order_by(array('publication_status' => '1'));
		$this->data['writer'] = $this->writer_m->get_order_by(array('writer_status' => '1'));
		$this->data['circulation'] = $this->circulation_m->get_order_by(array('return_status' => '0'));
		$this->data['circulation_issue'] = $this->dashboard_m->get_circulation(array('return_status' => '0'));
		$this->data['circulation_return'] = $this->dashboard_m->get_circulation(array('return_status' => '1'));
		$this->data['wastage'] = $this->wastage_m->get_wastage();

		$this->data['membername'] = pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');
		$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
		$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
		$this->data['writer_name'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');

		$this->data['title'] = 'Dashboard';
		$this->data["subview"] = "dashboard/index";
		$this->load->view('_main_layout', $this->data);
	}


	public function notfound() {
		$this->data["subview"] = "_not_found";
		$this->load->view('_main_layout', $this->data);
	}


	private function issued(){
		return  $this->monthManager(pluck($this->dashboard_m->get_issued_chart(date('Y')), 'circulationID', 'circulation_month'));
	}

	private function returned(){
		return  $this->monthManager(pluck($this->dashboard_m->get_returned_chart(date('Y')), 'circulationID', 'circulation_month'));
	}


	private function monthManager($arrays = []){
		$myMonths = [];
		for($i = 1; $i<=12; $i++){
			$myMonths[$i] = date('M', strtotime('01-'.sprintf('%02d', $i).'-2019')); 
		}
		$string = '';
		foreach($myMonths as $myMonthKey => $myMonth) {
			// dd($arrays[$myMonthKey]);
			if($myMonthKey != '12') {
				if(array_key_exists($myMonthKey, $arrays)) {
					$string .= $arrays[$myMonthKey].','; 
				} else {
					$string .= '0,';
				}
			} else {
				if(array_key_exists($myMonthKey, $arrays)) {
					$string .= $arrays[$myMonthKey]; 
				} else {
					$string .= '0';
				}
			}
			
		}
		return $string;

	}




}

