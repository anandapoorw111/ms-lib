<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissionmodule extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('permissionmodule_m');
		// $lang = $this->session->get_userdata('language');
		$lang = settings()->language;
		$this->lang->load('permissionmodule', $lang);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'name',
				'label'=> 'Name',
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'description',
				'label'=> 'Description',
				'rules'=> 'trim|required|min_length[5]|max_length[255]',
			),
			array(
				'field'=> 'active',
				'label'=> 'Active',
				'rules'=> 'trim|required|in_list[1,2]',
			)
		);
		return $rules;
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css'
			),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'
			)
		);
		$this->data['permissionmodules'] = $this->permissionmodule_m->get_permissionmodule();

		$this->data["subview"] = "permissionmodule/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function add() {
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "permissionmodule/add";
				$this->load->view('_main_layout', $this->data);
			} else {
				$array = []; 
				$array['name'] = $this->input->post('name'); 
				$array['description'] = $this->input->post('description'); 
				$array['active'] = $this->input->post('active'); 

				$this->permissionmodule_m->insert_permissionmodule($array);
				$this->session->set_flashdata('message', 'Message');
				redirect('permissionmodule/index');
			}
		} else {
			$this->data["subview"] = "permissionmodule/add";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function edit() {
		$id = escapeString($this->uri->segment('3'));
		if((int)$id) {
			$this->data['permissionmodule'] = $this->permissionmodule_m->get_single_permissionmodule($id);
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "permissionmodule/edit";
					$this->load->view('_main_layout', $this->data);
				} else {
					$array = []; 
					$array['name'] = $this->input->post('name'); 
					$array['description'] = $this->input->post('description'); 
					$array['active'] = $this->input->post('active'); 
					
					$this->permissionmodule_m->update_permissionmodule($array,$id);
					$this->session->set_flashdata('message', 'Message');
					redirect('permissionmodule/index');
				}
			} else {
				$this->data["subview"] = "permissionmodule/edit";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function delete() {
		$id = escapeString($this->uri->segment('3'));
		if((int)$id) {
			$this->permissionmodule_m->delete_permissionmodule($id);
			$this->session->set_flashdata('message', 'Message');
			redirect('permissionmodule/index');
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

}
