<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('book_m');
		$this->load->model('writer_m');
		$this->load->model('categories_m');
		$this->load->model('publication_m');
		$lang = settings()->language;
		$this->lang->load('book', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/book.js',
				)
			);
		$this->data['book'] = $this->book_m->get_book();	
		$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');	
		$this->data['title'] = 'Book';
		$this->data["subview"] = "book/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'name',
				'label'=> $this->lang->line('book_name'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'writer',
				'label'=> $this->lang->line('book_writer'),
				'rules'=> 'trim|required|max_length[60]|required_no_zero',
			),
			array(
				'field'=> 'publication',
				'label'=> $this->lang->line('book_publication'),
				'rules'=> 'trim|required|required_no_zero',
			),
			array(
				'field'=> 'categories',
				'label'=> $this->lang->line('book_categories'),
				'rules'=> 'trim|required|required_no_zero',
			),
			array(
				'field'=> 'book_code',
				'label'=> $this->lang->line('book_code'),
				'rules'=> 'trim|required|max_length[5]|callback_check_unique_code',
			),
			array(
				'field'=> 'book_isbn',
				'label'=> $this->lang->line('book_isbn'),
				'rules'=> 'trim|required|required_no_zero|callback_check_unique_isbn',
			),
			array(
				'field'=> 'photo',
				'label'=> $this->lang->line('book_photo'),
				'rules'=> 'trim|callback_photo_upload',
			),
			array(
				'field'=> 'price',
				'label'=> $this->lang->line('book_price'),
				'rules'=> 'trim|required|max_length[20]',
			),
			array(
				'field'=> 'book_edition',
				'label'=> $this->lang->line('book_edition'),
				'rules'=> 'trim|max_length[20]',
			),
			array(
				'field'=> 'book_edition_year',
				'label'=> $this->lang->line('book_edition_year'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'quantity',
				'label'=> $this->lang->line('book_quantity'),
				'rules'=> 'trim|required',
			),

			array(
				'field'=> 'issued_quantity',
				'label'=> $this->lang->line('book_issued_quantity'),
				'rules'=> 'trim|required|callback_less_than_equal_to',
			),

			array(
				'field'=> 'rack_no',
				'label'=> $this->lang->line('book_rack_no'),
				'rules'=> 'trim|required',
			)
		);
		return $rules;
	}


	public function add(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/book.js',
				)
			);
		if($_POST){
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['title'] = 'Book Add';
				$this->data["subview"] = "book/add";
				$this->data["writer"] = $this->writer_m->get_order_by(array('writer_status' => '1'));
				$this->data["categories"] = $this->categories_m->get_order_by(array('categories_status' => '1'));
				$this->data["publication"] = $this->publication_m->get_order_by(array('publication_status' => '1'));
				$this->load->view('_main_layout', $this->data);
			} else {
				
				$array = array(
					'book_code'    			=> settings()->book_prefixe.$this->input->post('book_code'),
					'book_isbn_no'    		=> $this->input->post('book_isbn'),
					'book_name'     		=> $this->input->post('name'),
					'book_category'  		=> $this->input->post('categories'),
					'book_photo'			=> $this->upload_data['photo']['file_name'],
					'book_price'			=> $this->input->post('price'),
					'book_writerID'		  	=> $this->input->post('writer'),
					'book_publication'   	=> $this->input->post('publication'),
					'book_edition' 			=> $this->input->post('book_edition'),
					'book_edition_year'     => $this->input->post('book_edition_year'),
					'book_quantity'  		=> $this->input->post('quantity'),
					'book_issued_quantity'  => $this->input->post('issued_quantity'),
					'book_availability'  	=> $this->input->post('quantity'),
					'book_rack_no'			=> $this->input->post('rack_no'),
					'book_status'  			=> '1',
					'create_date'      		=> date('Y-m-d H:i:s'),
					'create_userID'    		=> $this->session->userdata('userID'),
					'create_usertypeID'		=> $this->session->userdata('usertypeID'),
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
				$this->book_m->insert_book($array);
				$this->session->set_flashdata('message','Success');
				redirect('book/index');
			}
		} else {
			$this->data['title'] = 'Book Add';
			$this->data["subview"] = "book/add";
			$this->data["writer"] = $this->writer_m->get_order_by(array('writer_status' => '1'));
			$this->data["categories"] = $this->categories_m->get_order_by(array('categories_status' => '1'));
			$this->data["publication"] = $this->publication_m->get_order_by(array('publication_status' => '1'));
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function edit(){
		$bookID = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$bookID) {
			$book = $this->book_m->get_single_book(array('bookID' => $bookID));
			if(inicompute($book)) {
				$this->data['headerassets'] = array(
					'css' => array(
						'assets/bower_components/select2/select2.css',
						'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
						),
					'js' => array(
						'assets/bower_components/select2/select2.js',
						'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
						'assets/pages_js/book.js',
						)
					);
				$this->data["writer"] = $this->writer_m->get_order_by(array('writer_status' => '1'));
				$this->data["categories"] = $this->categories_m->get_order_by(array('categories_status' => '1'));
				$this->data["publication"] = $this->publication_m->get_order_by(array('publication_status' => '1'));
				$this->data['book']  = $book;
				if($_POST) {
					$rules = $this->rules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data['title'] = 'Book Update';
						$this->data["subview"] = "book/edit";
						$this->load->view('_main_layout', $this->data);
					} else {
						$quantity = $this->input->post('quantity');

						$array['book_code']    			= substr($book->book_code, 0, 2).$this->input->post('book_code');
						$array['book_isbn_no']    		= $this->input->post('book_isbn');
						$array['book_name']     		= $this->input->post('name');
						$array['book_category']  		= $this->input->post('categories');
						$array['book_photo']			= $this->upload_data['photo']['file_name'];
						$array['book_price']			= $this->input->post('price');
						$array['book_writerID']  		= $this->input->post('writer');
						$array['book_publication']   	= $this->input->post('publication');
						$array['book_edition'] 			= $this->input->post('book_edition');
						$array['book_edition_year']    	= $this->input->post('book_edition_year');
						$array['book_quantity']  		= $quantity;
						$array['book_issued_quantity']	= $this->input->post('issued_quantity');
						$array['book_availability']		= (($quantity + $book->book_availability) - $book->book_quantity);
						$array['book_rack_no']			= $this->input->post('rack_no');
						$array['modify_date']      		= date('Y-m-d H:i:s');
						$array['modify_userID']    		= $this->session->userdata('userID');
						$array['modify_usertypeID']		= $this->session->userdata('usertypeID');
						$this->book_m->update_book($array, $bookID);
						$this->session->set_flashdata('message','Success');
						redirect('book/index');
					}
				} else {
					$this->data['title'] = 'Book Update';
					$this->data["subview"] = "book/edit";
					$this->load->view('_main_layout', $this->data);
				}
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

	public function view(){
		$bookID = $this->input->post('id');
		if((int)$bookID) {
			$get_details = $this->book_m->get_single_book(array('bookID' => $bookID));
			$writer = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');	
			$publication = pluck($this->publication_m->get_publication(),'publication_name','publicationID');
			$categories = pluck($this->categories_m->get_categories(),'categories_name','categoriesID');
			if(inicompute($get_details)) {
				$html  = '';
				if(isset($get_details)){
					$html .= '<div class="col-md-12">
						<div class="col-md-5">
							<img src="'.book_img($get_details->book_photo).'"class="img-thumbnail img-responsive"/>
						</div>
						<div class="col-md-7">
					<table  class="table dt-responsive zero-border">';
					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_code').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_code.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_name').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_name.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_writer').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.(isset($writer[$get_details->book_writerID]) ? $writer[$get_details->book_writerID] : '').'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_categories').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td class="text-capitalize">'.(isset($categories[$get_details->book_category]) ? $categories[$get_details->book_category] : '').'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .= '<td><b>'.$this->lang->line('book_publication').'</b></td>';
					$html .= '<td><b>:</b></td>';
					$html .= '<td class="text-capitalize">'.(isset($publication[$get_details->book_publication]) ? $publication[$get_details->book_publication] : '').'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_isbn').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_isbn_no.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_edition').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_edition.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_edition_year').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.date('M Y',strtotime($get_details->book_edition_year)).'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_price').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td class="text-capitalize">'.settings()->currency_symbol.$get_details->book_price.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_quantity').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_quantity.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_issued_quantity').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_issued_quantity.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_availability').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_availability.'</td>';
					$html .= '</tr>';

					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_rack_no').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.$get_details->book_rack_no.'</td>';
					$html .= '</tr>';

					
					$html .= '<tr>';
					$html .=  '<td><b>'.$this->lang->line('book_entry').'</b></td>';
					$html .=  '<td><b>:</b></td>';
					$html .=  '<td>'.date('d-M-Y',strtotime($get_details->create_date)).'</td>';
					$html .= '</tr>';

					
					$html .= '</table></div></div>';
				}
				echo $html;
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

	public function delete() {
		if($this->book_m->delete_book($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}

	public function photo_upload(){
		$itemID = htmlentities(escapeString($this->uri->segment(3)));
		$item = array();
		if((int)$itemID) {
			$item = $this->book_m->get_single_book(array('bookID' => $itemID));
		}

		$new_file = "default.png";
		if($_FILES["photo"]['name'] !="") {
			$file_name = $_FILES["photo"]['name'];
			$random = rand(1, 10000000000000000);
			$file_name_rename = hash('sha512', $random.$this->input->post('username') . config_item("encryption_key"));
			$explode = explode('.', $file_name);
			if(inicompute($explode) >= 2) {
				$new_file = $file_name_rename.'.'.end($explode);
				$config['upload_path'] = "./uploads/book/";
				$config['allowed_types'] = "gif|jpg|png";
				$config['file_name'] = $new_file;
				$config['max_size'] = '2048';
				$config['max_width'] = '2000';
				$config['max_height'] = '2000';
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload("photo")) {
					$this->form_validation->set_message("photo_upload", $this->upload->display_errors());
					return FALSE;
				} else {
					$this->upload_data['photo'] =  $this->upload->data();
					return TRUE;
				}
			} else {
				$this->form_validation->set_message("photo_upload", "Invalid file");
				return FALSE;
			}
		} else {
			if(inicompute($item)) {
				$this->upload_data['photo'] = array('file_name' => $item->book_photo);
				return TRUE;
			} else {
				$this->upload_data['photo'] = array('file_name' => $new_file);
				return TRUE;
			}
		}
	}

	public function check_unique_code($data) {
		$bookID = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$bookID) {
			$get_book = $this->book_m->get_single_book(array('book_code' => settings()->book_prefixe.$data, 'bookID !=' => $bookID));
			if(inicompute($get_book)) {
				$this->form_validation->set_message("check_unique_code", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$get_book = $this->book_m->get_single_book(array('book_code' => settings()->book_prefixe.$data));
			if(inicompute($get_book)) {
				$this->form_validation->set_message("check_unique_code", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function less_than_equal_to($value){
		$quantity = $this->input->post('quantity');
		if ($quantity >= $value){
			return TRUE;
		}else{
			$this->form_validation->set_message("callback_less_than_equal_to", "The Issued Quantity field must contain a number less than or equal to Quantity..");
			return FALSE;
		}
	}

	public function check_unique_isbn($data){
		$bookID = htmlentities(escapeString($this->uri->segment(3)));
		if((int)$bookID) {
			$get_book = $this->book_m->get_single_book(array('book_isbn_no' => $data, 'bookID !=' => $bookID));
			if(inicompute($get_book)) {
				$this->form_validation->set_message("check_unique_isbn", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$get_book = $this->book_m->get_single_book(array('book_isbn_no' => $data));
			if(inicompute($get_book)) {
				$this->form_validation->set_message("check_unique_isbn", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function status(){
		$array = array('book_status' => $this->input->post('status'));
		$this->book_m->update_book($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
	}

	public function barcode(){
		if($this->input->post('id')){
			$get_data = $this->book_m->get_single_book($this->input->post('id'));
			if(inicompute($get_data)) {
				$temp = $get_data->book_code;
				$img = $this->generate_barcode($temp);
				$json = array(
					"confirmation" => 'success',
					"company_name" => settings()->company_name,
					'name' => $get_data->book_name,
					'price' => settings()->currency_symbol.$get_data->book_price,
					'img' => $this->generate_barcode($temp)
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			}
		}
	}

	function generate_barcode($text){
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		$file = Zend_Barcode::draw('code128', 'image', array('text' => $text), array());
		$text = time().$text;
		$store_image = imagepng($file,"uploads/barcode/$text.png");
		return base_url('uploads/barcode/'.$text.'.png');
	}


}
