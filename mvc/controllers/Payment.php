<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('payment_m');
		$this->load->model('member_m');
		$lang = settings()->language;
		$this->lang->load('payment', $lang);
	}

	public function index(){
		$this->data['headerassets'] = array(
		'css' => array(
			'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/bower_components/select2/select2.css',
			'assets/custom/css/hidetable.css',
			),
		'js' => array(
			'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
			'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
			'assets/bower_components/select2/select2.js',
			'assets/pages_js/payment.js',
			)
		);
		$this->data['payment'] 			= $this->payment_m->get_payment();
		$this->data['membername'] 		= pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');
		$this->data['title'] 			= 'Payment';
		$this->data["subview"] 			= "payment/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function view(){
		$paymentID = $this->input->post('id');
		// dd($bookID);
		if((int)$paymentID) {
			$get_payment_info = $this->payment_m->get_single_payment(array('paymentID' => $paymentID));
			$membername = pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');
			$membercode = pluck($this->member_m->get_member(array('memberID','member_code')),'member_code','memberID');

			// $categories = pluck($this->categories_m->get_categories(),'categories_name','categoriesID');
			if(inicompute($get_payment_info)) {
				$html  = '';
				if(isset($get_payment_info)){
					 $html .= '	<div class="row">
						        	<div class="col-md-12">
							            <div class="receipt-header col-md-6 col-md-offset-3">
							              	<h2 class="text-center">'.settings()->company_name.'</h2>
							              	<h6 class="text-center">'.settings()->address.'</h6>
							              	<h6 class="text-center">'.$this->lang->line('payment_email').':'.settings()->email.'</h6>
							              	<h6 class="text-center">'.$this->lang->line('payment_phone').':'.settings()->phone.'</h6>
							              	<h3 class="text-center">'.$this->lang->line('payment_money_receipt').'</h3>
							            </div>
						          	</div>
						        </div>
						        <div class="row">
						          	<div class="col-md-12">
						            	<p>'.$this->lang->line('payment_mr').':#'.$get_payment_info->paymentID.' <samp class="pull-right">'.$this->lang->line('payment_date').':'.date('d-M-Y',strtotime($get_payment_info->create_date)).'</samp></p>
						          	</div>
						        </div>

						        <div class="row">
						          	<div class="col-xs-12">
						            	<div class="col-xs-8 zero-padding">
						              		<div class="col-xs-4 zero-padding"><label>'.$this->lang->line('payment_from').':</label></div>
						              		<div class="col-xs-8 zero-padding"><p class="border-dotted">&nbsp;&nbsp;&nbsp;&nbsp;'.$membercode[$get_payment_info->payment_memberID].'-'.ucwords($membername[$get_payment_info->payment_memberID]).'</p></div>
						            	</div>
						            	<div class="col-xs-4 zero-padding">
						              		<div class="col-xs-2 zero-padding text-center"><label>'.$this->lang->line('payment_of').'&nbsp'.settings()->currency_symbol.'</label></div>
						              		<div class="col-xs-10 zero-padding"><p class="border-dotted">&nbsp;&nbsp;&nbsp;&nbsp;'.$get_payment_info->payment_amount.'</p></div>
						            	</div>
						          	</div>
						        </div> 

						        <div class="row">
						            <div class="col-xs-12 ">
						                <div class="col-xs-1 zero-padding"><label>'.$this->lang->line('payment_for').':</label></div>
						                <div class="col-xs-11 zero-padding"><p class="border-dotted">&nbsp;&nbsp;&nbsp;&nbsp;'.ucwords(str_replace('_', ' ', $get_payment_info->payment_by)).'&nbsp;Fee</p></div>
						            </div>
						         </div> 

						        <div class="row">
					            	<div class="col-xs-12">
					              		<div class="col-xs-1 zero-padding"><label>'.$this->lang->line('payment_inword').':</label></div>
					              		<div class="col-xs-11 zero-padding"><p class="border-dotted">&nbsp;&nbsp;&nbsp;&nbsp;'.convert_number_to_words($get_payment_info->payment_amount).' Only</p></div>
					            	</div>
					          	</div>
					          	<div class="row"><br><br>
						            <div class="col-xs-12">
						              <div class="col-xs-2 zero-padding col-xs-offset-10 border-top text-center"><label>'.$this->lang->line('payment_received_by').'</label></div>
						            </div>
						         </div>';

				}
				echo $html;
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}
}
