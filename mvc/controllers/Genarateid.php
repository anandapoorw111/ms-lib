<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	require 'mvc/libraries/vendor/autoload.php';
	use Mpdf\Mpdf;
class genarateid extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('member_m');
		$this->load->model('membership_m');
		$lang = settings()->language;
		$this->lang->load('genarateid', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/select2/select2.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/pages_js/genarateid.js',
				)
			);
		$this->data['find_data'] = 'false';
		if($_POST){
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() != FALSE){
				$memberID['id'] = $this->input->post('memberid[]');
				$data = array();
				foreach ($memberID['id'] as $Key=> $value){
				 	$data[]  = $this->member_m->get_single_member(array('memberID' =>$value));
				}
				$this->data['member_details'] = $data;
				$this->data['find_data'] = 'true';
			}
			
		}
		$this->data['ids'] = (!empty($memberID['id'])?$memberID['id']:'&nbsp;');
		$this->data['genarateid'] = $this->member_m->get_order_by_member( array('member_status' => 1));
		$this->data['membership'] = pluck($this->membership_m->get_membership(),'membership_name','membershipID');
		$this->data['title'] = 'Genarate ID';
		$this->data["subview"] = "genarateid/index";
		$this->load->view('_main_layout', $this->data);
	}
	protected function rules() {
		$rules = array(
			array(
				'field'=> 'memberid[]',
				'label'=> $this->lang->line('genarateid_label'),
				'rules'=> 'trim|required',
				)
			);
		return $rules;
	}


	public function print(){
		require 'mvc/libraries/vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P']);


		$memberID['id'] = $this->input->post('ids[]');
		$this->data['membership'] = pluck($this->membership_m->get_membership(),'membership_name','membershipID');
		$data = array();
		foreach ($memberID['id'] as $Key=> $value){
		 	$data[]  = $this->member_m->get_single_member(array('memberID' =>$value));
		}
		$this->data['member_details'] = $data;
		$this->data['qr_code'] = '<barcode  type="QR" class="barcode" code="'.$data->member_code.'" size="0.8" error="M"/>';




		$stylesheet  = file_get_contents(base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'));
		$stylesheet .= file_get_contents(base_url('assets/dist/css/AdminLTE.min.css'));
		$stylesheet .= file_get_contents(base_url('assets/custom/css/pdf/idcard.css'));

		$mpdf->WriteHTML($stylesheet,1);
		
		$html = $this->load->view('genarateid/pdf', $this->data, true);
		$mpdf->SetHTMLFooter('
		<table width="100%">
		    <tr>
		        <td width="33%">{DATE j-m-Y}</td>
		        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
		        <td width="33%" style="text-align: right;">Genarate ID Card</td>
		    </tr>
		</table>');

		// $mpdf->WriteHTML($html, 2);
		$mpdf->WriteHTML($html);
		$mpdf->Output();

	}


	// public function print(){

	// 	$mpdf = new Mpdf();
	// 	$mpdf->autoScriptToLang = true;
	// 	$mpdf->autoLangToFont = true;


	// 	$memberID['id'] = $this->input->post('ids[]');
	// 	$this->data['membership'] = pluck($this->membership_m->get_membership(),'membership_name','membershipID');
	// 	$data = array();
	// 	foreach ($memberID['id'] as $Key=> $value){
	// 	 	$data[]  = $this->member_m->get_single_member(array('memberID' =>$value));
	// 	}
	// 	$this->data['member_details'] = $data;


	// 	$html = $this->load->view('genarateid/pdf', $this->data, true);

	// 	$mpdf->SetHTMLFooter('
	// 	<table width="100%">
	// 	    <tr>
	// 	        <td width="33%">{DATE j-m-Y}</td>
	// 	        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
	// 	        <td width="33%" style="text-align: right;">Genarate ID Card</td>
	// 	    </tr>
	// 	</table>');

	// 	$stylesheet = file_get_contents(base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'));
	// 	$mpdf->WriteHTML($stylesheet);

	// 	$mpdf->WriteHTML($html, 2);
	// 	$mpdf->Output('lol' . '.pdf','I');
	// }


	public function qrcode($value){
	    QRcode::png(
	        $value,
	        $outfile = false,
	        $level = QR_ECLEVEL_H,
	        $size = 3,
	        $margin = 1
	    );
	}
}
