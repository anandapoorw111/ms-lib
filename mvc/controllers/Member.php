<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class member extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('member_m');
		$this->load->model('membership_m');
		$this->load->model('circulation_m');
		$this->load->model('book_m');
		$this->load->model('writer_m');
		$this->load->model('payment_m');
		$lang = settings()->language;
		$this->lang->load('member', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/custom/js/bootstrap-show-password.min.js',
				'assets/pages_js/member.js',
				)
			);
		$this->data['member'] = $this->member_m->get_member();	
		$this->data['membershiptypes'] = pluck($this->membership_m->get_membership(array('membershipID','membership_name')),'membership_name','membershipID');
		$this->data['title'] = 'member';
		$this->data["subview"] = "member/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'name',
				'label'=> $this->lang->line('member_name'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'father_name',
				'label'=> $this->lang->line('member_father_name'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'mother_name',
				'label'=> $this->lang->line('member_mother_name'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'member_date_of_birth',
				'label'=> $this->lang->line('member_date_of_birth'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'member_nid_no',
				'label'=> $this->lang->line('member_nid_no'),
				'rules'=> 'trim|max_length[20]',
			),
			array(
				'field'=> 'member_occupation',
				'label'=> $this->lang->line('member_occupation'),
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'membership',
				'label'=> $this->lang->line('member_membership'),
				'rules'=> 'trim|required|required_no_zero',
			),
			array(
				'field'=> 'member_gender',
				'label'=> $this->lang->line('member_edition'),
				'rules'=> 'trim|required|required_no_zero',
			),
			array(
				'field'=> 'member_blood_group',
				'label'=> $this->lang->line('member_blood_group'),
				'rules'=> 'trim',
			),
			array(
				'field'=> 'member_religion',
				'label'=> $this->lang->line('member_religion'),
				'rules'=> 'trim|required|required_no_zero',
			),
			array(
				'field'=> 'member_phone',
				'label'=> $this->lang->line('member_phone'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'member_email',
				'label'=> $this->lang->line('member_email'),
				'rules'=> 'trim|max_length[60]|valid_email',
			),
			array(
				'field'=> 'member_since',
				'label'=> $this->lang->line('member_since'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'member_address',
				'label'=> $this->lang->line('member_address'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'photo',
				'label'=> $this->lang->line('member_photo'),
				'rules'=> 'trim|callback_photo_upload',
			),
			array(
				'field'=> 'member_username',
				'label'=> $this->lang->line('member_username'),
				'rules'=> 'trim|required|callback_check_unique_username',
			),
			array(
				'field'=> 'member_password',
				'label'=> $this->lang->line('member_password'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'member_confirm_password',
				'label'=> $this->lang->line('member_confirm_password'),
				'rules'=> 'trim|required|matches[member_password]',
			),
		);
		return $rules;
	}


	public function add(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/member.js',
				)
			);
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['title'] = 'member Add';
				$this->data["subview"] = "member/add";
				$this->data["membership"] = $this->membership_m->get_order_by(array('membership_status' => '1'));
				$this->load->view('_main_layout', $this->data);
			} else {
				$array = array(
					'member_code'    		=> $this->input->post('member_code'),
					'member_name'     		=>$this->input->post('name'),
					'member_father_name'  	=>$this->input->post('father_name'),
					'member_mother_name'	=>$this->input->post('mother_name'),
					'member_date_of_birth'	=>date('Y-m-d',strtotime($this->input->post('member_date_of_birth'))),
					'member_nid_no'  		=>$this->input->post('member_nid_no'),
					'member_membership_type'=>$this->input->post('membership'),
					'member_occupation' 	=>$this->input->post('member_occupation'),
					'member_gender' 		=>$this->input->post('member_gender'),
					'member_blood_group'  	=>$this->input->post('member_blood_group'),
					'member_religion'  		=>$this->input->post('member_religion'),
					'member_phone'			=>$this->input->post('member_phone'),
					'member_email'			=>$this->input->post('member_email'),
					'member_photo'			=>$this->upload_data['photo']['file_name'],
					'member_since_date'		=>date('Y-m-d',strtotime($this->input->post('member_since'))),
					'member_address'		=>$this->input->post('member_address'),
					'member_status'  		=> '0',
					'username'  			=> $this->input->post('member_username'),
					'password'  			=> $this->member_m->hash($this->input->post('member_password')),
					'create_date'      		=> date('Y-m-d H:i:s'),
					'create_userID'    		=> $this->session->userdata('userID'),
					'create_usertypeID'		=> $this->session->userdata('usertypeID'),
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
				$this->member_m->insert_member($array);
				$this->session->set_flashdata('message','Success');
				redirect('member/index');
			}
		} else {
			$this->data['title'] = 'member Add';
			$this->data["subview"] = "member/add";
			$this->data["membership"] = $this->membership_m->get_order_by(array('membership_status' => '1'));
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function edit(){
		$memberID = htmlentities(escapeString(base64_decode($this->uri->segment(3))));
		if((int)$memberID) {
			$member = $this->member_m->get_single_member(array('memberID' => $memberID));
			if(inicompute($member)) {
				$this->data['headerassets'] = array(
					'css' => array(
						'assets/bower_components/select2/select2.css',
						'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
						),
					'js' => array(
						'assets/bower_components/select2/select2.js',
						'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
						'assets/pages_js/member.js',
						)
					);
				$this->data["membership"] = $this->membership_m->get_order_by(array('membership_status' => '1'));
				$this->data['member']  = $member;
				if($_POST) {
					$rules = $this->rules();
					unset($rules[16]);
					unset($rules[17]);
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data['title'] = 'member Update';
						$this->data["subview"] = "member/edit";
						$this->load->view('_main_layout', $this->data);
					} else {

						$array['member_name']     		=	$this->input->post('name');
						$array['member_father_name']  	=	$this->input->post('father_name');
						$array['member_mother_name']	=	$this->input->post('mother_name');
						$array['member_date_of_birth']	=	date('Y-m-d',strtotime($this->input->post('member_date_of_birth')));
						$array['member_nid_no']  		=	$this->input->post('member_nid_no');
						$array['member_membership_type']=	$this->input->post('membership');
						$array['member_occupation'] 	=	$this->input->post('member_occupation');
						$array['member_gender'] 		=	$this->input->post('member_gender');
						$array['member_blood_group']  	=	$this->input->post('member_blood_group');
						$array['member_religion']  		=	$this->input->post('member_religion');
						$array['member_phone']			=	$this->input->post('member_phone');
						$array['member_email']			=	$this->input->post('member_email');
						$array['member_photo']			=	$this->upload_data['photo']['file_name'];
						$array['member_since_date']		=	date('Y-m-d',strtotime($this->input->post('member_since')));
						$array['member_address']		=	$this->input->post('member_address');
						$array['username']				=	$this->input->post('member_username');

						$array['modify_date']      		= date('Y-m-d H:i:s');
						$array['modify_userID']    		= $this->session->userdata('userID');
						$array['modify_usertypeID']		= $this->session->userdata('usertypeID');
							

						$this->member_m->update_member($array, $memberID);
						$this->session->set_flashdata('message','Success');
						redirect('member/index');
					}
				} else {
					$this->data['title'] = 'member Update';
					$this->data["subview"] = "member/edit";
					$this->load->view('_main_layout', $this->data);
				}
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

	public function view(){
		$memberID = htmlentities(escapeString(base64_decode($this->uri->segment(3))));
		if((int)$memberID) {
			$member = $this->member_m->get_single_member(array('memberID' => $memberID));
			if(inicompute($member)) {
				$this->data['headerassets'] = array(
					'css' => array(
						'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
						'assets/custom/css/hidetable.css',
						),
					'js' => array(
						'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
						'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
						'assets/pages_js/member.js',
						)
					);
				$this->data["membership"] = pluck($this->membership_m->get_membership(array('membershipID','membership_name')),'membership_name','membershipID');
				$this->data['member'] = $member;
				$this->data['title'] = 'Member Details';
				$this->data["subview"] = "member/view";
				$this->data['circulations'] = $this->circulation_m->get_order_by(array('memberID' => $memberID));
				$this->data['bookcode'] = pluck($this->book_m->get_book(array('bookID','book_code')),'book_code','bookID');
				$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
				$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
				$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');
				$this->data['payment'] 	= $this->payment_m->get_order_by(array('payment_memberID' => $memberID)); 

				$this->load->view('_main_layout', $this->data);
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

	public function delete() {
		if($this->member_m->delete_member($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}

	public function photo_upload(){
		$itemID = htmlentities(escapeString($this->uri->segment(3)));
		$item = array();
		if((int)$itemID) {
			$item = $this->member_m->get_single_member(array('memberID' => $itemID));
		}

		$new_file = "default.png";
		if($_FILES["photo"]['name'] !="") {
			$file_name = $_FILES["photo"]['name'];
			$random = rand(1, 10000000000000000);
			$file_name_rename = hash('sha512', $random.$this->input->post('username') . config_item("encryption_key"));
			$explode = explode('.', $file_name);
			if(inicompute($explode) >= 2) {
				$new_file = $file_name_rename.'.'.end($explode);
				$config['upload_path'] = "./uploads/member/";
				$config['allowed_types'] = "gif|jpg|png";
				$config['file_name'] = $new_file;
				$config['max_size'] = '2048';
				$config['max_width'] = '2000';
				$config['max_height'] = '2000';
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload("photo")) {
					$this->form_validation->set_message("photo_upload", $this->upload->display_errors());
					return FALSE;
				} else {
					$this->upload_data['photo'] =  $this->upload->data();
					return TRUE;
				}
			} else {
				$this->form_validation->set_message("photo_upload", "Invalid file");
				return FALSE;
			}
		} else {
			if(inicompute($item)) {
				$this->upload_data['photo'] = array('file_name' => $item->member_photo);
				return TRUE;
			} else {
				$this->upload_data['photo'] = array('file_name' => $new_file);
				return TRUE;
			}
		}
	}

	public function check_unique_username($username) {
		$memberID = htmlentities(escapeString(base64_decode($this->uri->segment(3))));
		if((int)$memberID){
			$member = $this->member_m->get_single_member(array('username' => $username, 'memberID !=' => $memberID));
			if(inicompute($member)) {
				$this->form_validation->set_message("check_unique_username", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$member = $this->member_m->get_single_member(array('username' => $username));
			if(inicompute($member)) {
				$this->form_validation->set_message("check_unique_username", "The %s is already exits.");
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}

	public function status(){
		$array = array('member_status' => $this->input->post('status'));
		$this->member_m->update_member($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
	}


	protected function changepasswordrules() {
		$rules = array(
			array(
				'field'=> 'new_password',
				'label'=> $this->lang->line('member_new_password'),
				'rules'=> 'required|max_length[15]',
				),
			array(
				'field'=> 'confirm_password',
				'label'=> $this->lang->line('member_confirm_password'),
				'rules'=> 'required|matches[new_password]',
				)
			);
		return $rules;
	}

	public function changepassword(){
		if($_POST) {
			$this->form_validation->set_rules($this->changepasswordrules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$memberID = $this->input->post('id');
				$array = array(
					'password'    		=> $this->member_m->hash($this->input->post('new_password')),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->member_m->update_member($array, $memberID);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('user'));
		}
	}


}
