<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wastage extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('wastage_m');
		$this->load->model('member_m');
		$this->load->model('book_m');
		$this->load->model('writer_m');
		$lang = settings()->language;
		$this->lang->load('wastage', $lang);
	}

	public function index(){
		$this->data['headerassets'] = array(
		'css' => array(
			'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/bower_components/select2/select2.css',
			'assets/custom/css/hidetable.css',
			),
		'js' => array(
			'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
			'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
			'assets/bower_components/select2/select2.js',
			'assets/pages_js/wastage.js',
			)
		);
		$this->data['wastage'] 			= $this->wastage_m->get_wastage();
		$this->data['book'] 			= $this->book_m->get_book();
		$this->data['bookphoto'] 		= pluck($this->book_m->get_book(array('bookID','book_photo')),'book_photo','bookID');
		$this->data['bookname'] 		= pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
		$this->data['bookcode'] 		= pluck($this->book_m->get_book(array('bookID','book_code')),'book_code','bookID');
		$this->data['writerID'] 		= pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
		$this->data['writername'] 		= pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');
		$this->data['membername'] 		= pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');
		$this->data['title'] 			= 'Wastage';
		$this->data["subview"] 			= "wastage/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules(){
		$rules = array(
			array(
				'field'=> 'bookID',
				'label'=> $this->lang->line('wastage_book_name'),
				'rules'=> 'trim|required|max_length[25]|required_no_zero',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'wastage_bookID'    => $this->input->post('bookID'),
					'wastage_note'  	=> $this->input->post('wastage_note'),
					'wastage_quantity'  => '1',
					'wastage_by'  		=> '1',
					'create_date'      	=> date('Y-m-d H:i:s'),
					'create_userID'    	=> $this->session->userdata('userID'),
					'create_usertypeID'	=> $this->session->userdata('usertypeID'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
				);
				if($this->wastage_m->insert_wastage($array)){
					$book = $this->book_m->get_single_book(array('bookID' => $this->input->post('bookID')));
					$updateArray = array(
						'book_quantity' => $book->book_quantity - 1,
						'modify_date'      	=> date('Y-m-d H:i:s'),
						'modify_userID'    	=> $this->session->userdata('userID'),
						'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->book_m->update_book($updateArray, $this->input->post('bookID'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);	
					
				}
			}
		} else {
			redirect(base_url('wastage'));
		}
	}


	public function restore() {
		$wastage = $this->wastage_m->get_single_wastage($this->input->post('id'));
		if($this->wastage_m->delete_wastage($this->input->post('id'))){
			$book = $this->book_m->get_single_book(array('bookID' => $wastage->wastage_bookID));
			$arr = array('book_quantity' => $book->book_quantity +  1);
			$this->book_m->update_book($arr, $book->bookID);
			$response['status']  = 'success';
			$response['message'] = 'restored Successfully ...';
			echo json_encode($response);
		}
	}
}
