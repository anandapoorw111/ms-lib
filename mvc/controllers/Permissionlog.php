<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissionlog extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('permissionlog_m');
		$this->load->model('permissionmodule_m');
		// $lang = $this->session->get_userdata('language');
		$lang = settings()->language;
		$this->lang->load('permissionlog', $lang);

		// if(config_item('demo') == FALSE || ENVIRONMENT == 'production') {
		// 	redirect('dashboard/index');
		// }
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'name',
				'label'=> 'Name',
				'rules'=> 'trim|required|max_length[60]',
			),
			array(
				'field'=> 'description',
				'label'=> 'Description',
				'rules'=> 'trim|required|min_length[4]|max_length[255]',
			),
			array(
				'field'=> 'permissionmoduleID',
				'label'=> 'Permission Module',
				'rules'=> 'trim|required|is_natural',
			),
			array(
				'field'=> 'active',
				'label'=> 'Active',
				'rules'=> 'trim|required|in_list[yes,no]',
			)
		);
		return $rules;
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css'
			),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'
			)
		);

		$this->data['permissionmodules'] = pluck( $this->permissionmodule_m->get_order_by_permissionmodule(array('active'=>1)),'name','permissionmoduleID');
		$this->data['permissionlogsArray'] = pluck_multi_array_key($this->permissionlog_m->get_permissionlog(),'obj','permissionmoduleID','permissionlogID');

		$this->data["subview"] = "permissionlog/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function add() {
		$this->data['permissionmodules'] = $this->permissionmodule_m->get_order_by_permissionmodule(array('active'=>1));
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "permissionlog/add";
				$this->load->view('_main_layout', $this->data);
			} else {
				$array = []; 
				$array['name']        = $this->input->post('name'); 
				$array['description'] = $this->input->post('description'); 
				$array['permissionmoduleID'] = $this->input->post('permissionmoduleID'); 
				$array['active']      = $this->input->post('active'); 

				$this->permissionlog_m->insert_permissionlog($array);
				$this->session->set_flashdata('message', 'Message');
				redirect('permissionlog/index');
			}
		} else {
			$this->data["subview"] = "permissionlog/add";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function edit() {
		$id = escapeString($this->uri->segment('3'));
		$this->data['permissionmodules'] = $this->permissionmodule_m->get_order_by_permissionmodule(array('active'=>1));
		if((int)$id) {
			$this->data['permissionlog'] = $this->permissionlog_m->get_single_permissionlog($id);
			if($_POST) {
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data["subview"] = "permissionlog/edit";
					$this->load->view('_main_layout', $this->data);
				} else {
					$array = []; 
					$array['name'] = $this->input->post('name'); 
					$array['description'] = $this->input->post('description'); 
					$array['permissionmoduleID'] = $this->input->post('permissionmoduleID'); 
					$array['active'] = $this->input->post('active');
					
					$this->permissionlog_m->update_permissionlog($array,$id);
					$this->session->set_flashdata('message', 'Message');
					redirect('permissionlog/index');
				}
			} else {
				$this->data["subview"] = "permissionlog/edit";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function delete() {
		$id = escapeString($this->uri->segment('3'));
		if((int)$id) {
			$this->permissionlog_m->delete_permissionlog($id);
			$this->session->set_flashdata('message', 'Message');
			redirect('permissionlog/index');
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

}
