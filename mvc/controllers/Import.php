<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('book_m');
		// $this->load->model('writer_m');
		// $this->load->model('categories_m');
		// $this->load->model('publication_m');
		$this->load->library('csvimport');
		$lang = settings()->language;
		$this->lang->load('import', $lang);
	}

	public function index() {
		if($_POST){
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data['title'] = 'Import';
				$this->data["subview"] = "import/index";
				$this->load->view('_main_layout', $this->data);
			} else {
			$file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
			foreach($file_data as $row)
			{
				$data[] = array(
					'book_code'				=>	$row["book_code"],
					'book_isbn_no'			=>	$row["book_isbn_no"],
					'book_name'				=>	$row["book_name"],
					'book_category'			=>	$row["book_categoryID"],
					'book_photo'			=>	'default.png',
					'book_price'			=>	$row["book_price"],
					'book_writerID'			=>	$row["book_writerID"],
					'book_publication'		=>	$row["book_publicationID"],
					'book_edition'			=>	$row["book_edition"],
					'book_edition_year'		=>	$row["book_edition_year"],
					'book_quantity'			=>	$row["book_quantity"],
					'book_issued_quantity'	=>	$row["book_issued_quantity"],
					'book_availability'		=>	$row["book_issued_quantity"],
					'book_rack_no'			=>	$row["book_rack_no"],
					'book_status'			=>	'1',
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
			}
			$this->book_m->insert_batch($data);
			$this->session->set_flashdata('message','Success');
			redirect('import/index');	
		}
	}else {
		$this->data['title'] = 'Import';
		$this->data["subview"] = "import/index";
		$this->load->view('_main_layout', $this->data);
	}
}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'csv_file',
				'label'=> $this->lang->line('import_import'),
				'rules'=> 'trim|callback_valid_file',
			)
		);
		return $rules;
	}



	public function valid_file(){
		$file_name = $_FILES["csv_file"]['name'];
		if (inicompute($file_name)){
			$explode = explode('.', $file_name);
			if($_FILES["csv_file"]['name'] !="") {
				$file_name = $_FILES["csv_file"]['name'];
				$explode = explode('.', $file_name);
				if(end($explode) == 'csv') {
					return TRUE;
				} else {
					$this->form_validation->set_message("valid_file", "Invalid file");
					return FALSE;
				}
			}else {
				$this->form_validation->set_message("valid_file", "Invalid file");
				return FALSE;
			} 
		}else{
			$this->form_validation->set_message("valid_file", "Upload valid file format");
			return FALSE;
		}
	}



// export Demo file

	public function exportbooklist(){
		$file_name = 'book_category_list_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$file_name"); 
        header("Content-Type: application/csv;");
        // get data 
        $book_category_list = $this->book_m->get_export_book_list();
        // file creation 
        $file = fopen('php://output', 'w');
        $header = array("Categories ID","Categories Code","Categories Name"); 
        fputcsv($file, $header);
        foreach ($book_category_list->result_array() as $key => $value){ 
	       fputcsv($file, $value); 
	    }
        fclose($file); 
        exit; 
	}


	public function exportwriterlist(){
		$file_name = 'book_writer_list_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$file_name"); 
        header("Content-Type: application/csv;");
        // get data 
        $book_category_list = $this->book_m->get_export_writer_list();
        // file creation 
        $file = fopen('php://output', 'w');
        $header = array("Writer ID","Writer Name"); 
        fputcsv($file, $header);
        foreach ($book_category_list->result_array() as $key => $value){ 
	       fputcsv($file, $value); 
	    }
        fclose($file); 
        exit; 
	}


	public function exportpublicationlist(){
		$file_name = 'book_publication_list_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$file_name"); 
        header("Content-Type: application/csv;");
        // get data 
        $book_category_list = $this->book_m->get_export_publication_list();
        // file creation 
        $file = fopen('php://output', 'w');
        $header = array("Publication ID","Publication Name"); 
        fputcsv($file, $header);
        foreach ($book_category_list->result_array() as $key => $value){ 
	       fputcsv($file, $value); 
	    }
        fclose($file); 
        exit; 
	}
}
