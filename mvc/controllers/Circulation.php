<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Circulation extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('circulation_m');
		$this->load->model('member_m');
		$this->load->model('writer_m');
		$this->load->model('membership_m');
		$this->load->model('book_m');
		$this->load->model('payment_m');
		$this->load->model('wastage_m');
		$lang = settings()->language;
		$this->lang->load('circulation', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/circulation.js',
				)
			);
		$this->data['circulations'] = $this->circulation_m->get_circulation();
		$this->data['membercode'] = pluck($this->member_m->get_member(array('memberID','member_code')),'member_code','memberID');
		$this->data['membername'] = pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');
		$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
		$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
		$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');	
		$this->data['title'] = 'Circulation';
		$this->data["subview"] = "circulation/index";
		$this->load->view('_main_layout', $this->data);
	}


	protected function rules() {
		$rules = array(
			array(
				'field'=> 'book_code',
				'label'=> $this->lang->line('book_code'),
				'rules'=> 'trim|required|max_length[25]|callback_check_book_list',
				)
			);
		return $rules;
	}

	public function view() {
		$memberID = htmlentities(escapeString(base64_decode($this->uri->segment(3))));
		if((int)$memberID){
			$member = $this->member_m->get_single_member(array('memberID' => $memberID));
			if(inicompute($member)){
				$this->data['headerassets'] = array(
					'css' => array(
						'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
						'assets/custom/css/hidetable.css',
						),
					'js' => array(
						'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
						'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
						'assets/pages_js/circulation.js',
						)
				);
				if($_POST){
					$rules = $this->rules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data['title'] = 'Issue & Reture';
						$this->data['member'] = $member;
						$this->data['circulations'] = $this->circulation_m->get_order_by_circulation(array('memberID' => $memberID, 'return_status' => '0'));
						$this->data['membership'] =pluck($this->membership_m->get_membership(array('membershipID','membership_name')),'membership_name','membershipID');
						$this->data['bookphoto'] = pluck($this->book_m->get_book(array('bookID','book_photo')),'book_photo','bookID');
						$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
						$this->data['bookcode'] = pluck($this->book_m->get_book(array('bookID','book_code')),'book_code','bookID');
						$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
						$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');
						$this->data["subview"] = "circulation/view";
						$this->load->view('_main_layout', $this->data);
					} else {
						$this->data['title'] = 'Issue & Reture';
						$this->data['member'] = $member;
						$this->data['circulations'] = $this->circulation_m->get_order_by_circulation(array('memberID' => $memberID, 'return_status' => '0'));
						$this->data['membership'] =pluck($this->membership_m->get_membership(array('membershipID','membership_name')),'membership_name','membershipID');
						$this->data["book"] = $this->book_m->get_single_book(array('book_code' => $this->input->post('book_code')));
						$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
						$this->data['bookcode'] = pluck($this->book_m->get_book(array('bookID','book_code')),'book_code','bookID');
						$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
						$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');
						$this->data["subview"] = "circulation/view";
						$this->load->view('_main_layout', $this->data);
					}
				} else {
					$this->data['title'] = 'Issue & Reture';
					$this->data['member'] = $member;
					$this->data['circulations'] = $this->circulation_m->get_order_by_circulation(array('memberID' => $memberID, 'return_status' => '0'));
					$this->data['membership'] =pluck($this->membership_m->get_membership(array('membershipID','membership_name')),'membership_name','membershipID');
					$this->data['bookphoto'] = pluck($this->book_m->get_book(array('bookID','book_photo')),'book_photo','bookID');
					$this->data['bookname'] = pluck($this->book_m->get_book(array('bookID','book_name')),'book_name','bookID');
					$this->data['bookcode'] = pluck($this->book_m->get_book(array('bookID','book_code')),'book_code','bookID');
					$this->data['writerID'] = pluck($this->book_m->get_book(array('bookID','book_writerID')),'book_writerID','bookID');
					$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');
					$this->data["subview"] = "circulation/view";
					$this->load->view('_main_layout', $this->data);
				}
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

	public function search(){
		$data = $this->circulation_m->get_search($this->input->post('post_data'));
		$html  = '';
		foreach ($data as $value) {
			$html .=  '<a href="'.base_url('circulation/view/'.base64_encode($value->memberID)).'" id="'.$value->memberID.'" ><li id="'.$value->memberID.'" class="memberID">'.$value->member_code.'-'.ucfirst($value->member_name).'</li></a>';
		}
		echo $html;
	}

	public function check_book_list($bookCode){
		$book = $this->book_m->get_single_book(array('book_code' => $bookCode));
		if (inicompute($book)){
			$non_issue_book_count = ($book->book_quantity - $book->book_issued_quantity);
			if(!inicompute($book)){
					$this->form_validation->set_message("check_book_list",  $this->lang->line('circulation_not_available'));
					return FALSE;
			}elseif($non_issue_book_count == $book->book_availability){
				$this->form_validation->set_message("check_book_list",  $this->lang->line('circulation_not_available'));
				return FALSE;
			}else {
				return TRUE;
			}
		}else{
			$this->form_validation->set_message("check_book_list",  $this->lang->line('circulation_not_available'));
			return FALSE;
		}
	}

	public function add(){
		if($_POST){
			$getMembership = $this->membership_m->get_single_membership(array('membershipID' => $this->input->post('membership_type')));
			$getIssuedBook = $this->circulation_m->get_order_by_circulation(array('memberID' => $this->input->post('memberID'), 'return_status' => 0));
			$expiryDate    = date('d-m-Y', strtotime("".$getMembership->membership_days_limit ."days"));

			if((count($getIssuedBook)+1) <= ($getMembership->membership_books_limit)){
				$array = array(
					'memberID' 				=> $this->input->post('memberID'), 
					'circulation_code' 		=> circulationcode(), 
					'bookID'   				=> $this->input->post('bookID'),
					'circulation_year'      => date('Y'),
					'circulation_month'     => intval(ltrim(date('m'), '0')),
					'issue_date'			=> date('Y-m-d H:i:s'),
					'expiry_date'			=> date('Y-m-d',strtotime($expiryDate)),
					'create_date'      		=> date('Y-m-d H:i:s'),
					'create_userID'    		=> $this->session->userdata('userID'),
					'create_usertypeID'		=> $this->session->userdata('usertypeID'),
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
				if($this->circulation_m->insert_circulation($array)){
					$bookDetails = $this->book_m->get_single_book(array('bookID' => $this->input->post('bookID')));
					$update_array = array('book_availability' => $bookDetails->book_availability - 1,);
					$this->book_m->update_book($update_array,$this->input->post('bookID'));
					redirect(base_url('circulation/view/'.base64_encode($this->input->post('memberID'))));
				}

			};
		}
		redirect(base_url('circulation/view/'.base64_encode($this->input->post('memberID'))));
	}

	public function renewal(){
		$circulationID 	= htmlentities(escapeString($this->input->post('id')));
		$expiry_date 	= htmlentities(strtotime($this->input->post('expiry_date')));
		$today_date 	= strtotime(date('Y-m-d H:i:s'));

		if((int)$circulationID){
			$get_circulation = $this->circulation_m->get_single_circulation(array('circulationID' => $circulationID));
			$get_membership  = $this->membership_m->get_single_membership(array('membershipID' => $this->input->post('membershipID')));
			$get_circulation_code = (inicompute($this->circulation_m->get_order_by_circulation(array('circulation_code' => $get_circulation->circulation_code))) - 1);

			if ($get_membership->renew_limit > $get_circulation_code){
				if($expiry_date >= $today_date){
					$array = array(
						'return_date'			=> date('Y-m-d H:i:s'),
						'return_status'			=> '1',
						'circulation_year'      => date('Y'),
						'circulation_month'     => intval(ltrim(date('m'), '0')),
						'modify_date'      		=> date('Y-m-d H:i:s'),
						'modify_userID'    		=> $this->session->userdata('userID'),
						'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
					);
					if($this->circulation_m->update_circulation($array,$circulationID)){
						$array_insert = array(
							'circulation_code' 			=> $get_circulation->circulation_code,
							'memberID' 					=> $get_circulation->memberID,
							'bookID'   					=> $get_circulation->bookID,
							'issue_date'				=> date('Y-m-d H:i:s'),
							'expiry_date'				=> date('Y-m-d H:i:s', strtotime("".$get_membership->membership_days_limit ."days")),
							'create_date'      			=> date('Y-m-d H:i:s'),
							'create_userID'    			=> $this->session->userdata('userID'),
							'create_usertypeID'			=> $this->session->userdata('usertypeID'),
							'modify_date'      			=> date('Y-m-d H:i:s'),
							'modify_userID'    			=> $this->session->userdata('userID'),
							'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
						);
						$this->circulation_m->insert_circulation($array_insert);
						$json = array("confirmation" => 'Success');
						header("Content-Type: application/json", true);
						echo json_encode($json);
					}
				}else{
					$expiry_date 			= htmlentities(strtotime($this->input->post('expiry_date')));
					$today_date 			= strtotime(date('Y-m-d H:i:s'));-+
					$datediff 				= $today_date - $expiry_date;
					$no_of_days 			=  floor($datediff / (60 * 60 * 24));
					$calculate_penalty_fee 	= $get_membership->penalty_fee * $no_of_days;

					$json = array(
						"confirmation" 	  => 'penalty',
						'circulationID'   => $get_circulation->circulationID,
						'circulationCode' => $get_circulation->circulation_code,
						'memberID' 	   	  => $get_circulation->memberID,
						'bookID'   	   	  => $get_circulation->bookID,
						'date_count' 	  => $no_of_days,
						'penalty_fee' 	  => $get_membership->penalty_fee,
						'book_limit_days' => $get_membership->membership_days_limit,
						'calculate_fee'   => $calculate_penalty_fee,
					);
					header("Content-Type: application/json", true);
					echo json_encode($json);
					exit;
				}
			}else{
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}
	}

	public function return(){
		$circulationID 	= htmlentities(escapeString($this->input->post('id')));
		$expiry_date 	= htmlentities(strtotime($this->input->post('expiry_date')));
		$today_date 	= strtotime(date('Y-m-d H:i:s'));

		if((int)$circulationID){
			$get_circulation = $this->circulation_m->get_single_circulation(array('circulationID' => $circulationID));
			$get_membership  = $this->membership_m->get_single_membership(array('membershipID' => $this->input->post('membershipID')));
			if($expiry_date >= $today_date){
				$array = array(
					'return_date'			=> date('Y-m-d H:i:s'),
					'return_status'			=> '1',
					'circulation_year'      => date('Y'),
					'circulation_month'     => intval(ltrim(date('m'), '0')),
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
				if($this->circulation_m->update_circulation($array,$circulationID)){
					$bookDetails = $this->book_m->get_single_book(array('bookID' => $get_circulation->bookID));
					$update_array = array('book_availability' => $bookDetails->book_availability + 1,);
					$this->book_m->update_book($update_array,$bookDetails->bookID);
					$json = array("confirmation" => 'Success');
					header("Content-Type: application/json", true);
					echo json_encode($json);
				}
			}else{
				$expiry_date 			= htmlentities(strtotime($this->input->post('expiry_date')));
				$today_date 			= strtotime(date('Y-m-d H:i:s'));-+
				$datediff 				= $today_date - $expiry_date;
				$no_of_days 			=  floor($datediff / (60 * 60 * 24));
				$calculate_penalty_fee 	= $get_membership->penalty_fee * $no_of_days;
				$json = array(
					"confirmation" 	  => 'penalty',
					'circulationID'   => $get_circulation->circulationID,
					'circulationCode' => $get_circulation->circulation_code,
					'memberID' 	   	  => $get_circulation->memberID,
					'bookID'   	   	  => $get_circulation->bookID,
					'date_count' 	  => $no_of_days,
					'penalty_fee' 	  => $get_membership->penalty_fee,
					'book_limit_days' => $get_membership->membership_days_limit,
					'calculate_fee'   => $calculate_penalty_fee,
				);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			}
		}
	}


	public function penalty(){
		if($_POST){
			$array_payment = array(
				'payment_memberID' 			=> $this->input->post('member_id'),
				'payment_bookID' 			=> $this->input->post('book_id'),
				'payment_by' 				=> 'expiry_date',
				'payment_amount' 			=> $this->input->post('penalty_amount'),
				'no_of_days'  				=> $this->input->post('no_of_days'),
				'create_date'      			=> date('Y-m-d H:i:s'),
				'create_userID'    			=> $this->session->userdata('userID'),
				'create_usertypeID'			=> $this->session->userdata('usertypeID'),
				'modify_date'      			=> date('Y-m-d H:i:s'),
				'modify_userID'    			=> $this->session->userdata('userID'),
				'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
			);
			if($this->payment_m->insert_payment($array_payment)){
				$array = array(
					'return_date'			=> date('Y-m-d H:i:s'),
					'return_status'			=> '1',
					'circulation_year'      => date('Y'),
					'circulation_month'     => intval(ltrim(date('m'), '0')),
					'penalty_amount'  		=> $this->input->post('penalty_amount'),
					'no_of_days'  			=> $this->input->post('no_of_days'),
					'penalty_by'  			=> 'expiry_date',
					'modify_date'      		=> date('Y-m-d H:i:s'),
					'modify_userID'    		=> $this->session->userdata('userID'),
					'modify_usertypeID'		=> $this->session->userdata('usertypeID'),
				);
				if($this->circulation_m->update_circulation($array,$this->input->post('circulation_id'))){

					if ($this->input->post('penalty_by') == 'returned'){
						$bookDetails = $this->book_m->get_single_book(array('bookID' => $this->input->post('book_id')));
						$update_array = array('book_availability' => $bookDetails->book_availability + 1,);
						$this->book_m->update_book($update_array,$bookDetails->bookID);
					}else{
						$array_insert = array(
							'circulation_code' 			=> $this->input->post('circulation_code'),
							'memberID' 					=> $this->input->post('member_id'),
							'bookID'   					=> $this->input->post('book_id'),
							'issue_date'				=> date('Y-m-d H:i:s'),
							'expiry_date'				=> date('Y-m-d H:i:s', strtotime("".$this->input->post('book_limit_days') ."days")),
							'create_date'      			=> date('Y-m-d H:i:s'),
							'create_userID'    			=> $this->session->userdata('userID'),
							'create_usertypeID'			=> $this->session->userdata('usertypeID'),
							'modify_date'      			=> date('Y-m-d H:i:s'),
							'modify_userID'    			=> $this->session->userdata('userID'),
							'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
						);
						$this->circulation_m->insert_circulation($array_insert);
					}
					$json = array("confirmation" => 'Success');
					header("Content-Type: application/json", true);
					echo json_encode($json);
				}
			}
		}
	}

	public function lost(){

		if($_POST){
			if (!inicompute($this->input->post('book_price'))) {
				$circulationID = $this->circulation_m->get_single_circulation(array('circulationID' => $this->input->post('id')));
				$book = $this->book_m->get_single_book(array('bookID' => $circulationID->bookID));
				$response = array(
					"confirmation" 	  => 'success',
					'circulationID'   => $this->input->post('id'),
					'memberID' 	   	  => $circulationID->memberID,
					'bookID'   	   	  => $circulationID->bookID,
					'book_price' 	  => $book->book_price,
				);
				header("Content-Type: application/json", true);
				echo json_encode($response);				
			}else{
				$array_payment = array(
					'payment_memberID' 			=> $this->input->post('memberID'),
					'payment_bookID' 			=> $this->input->post('bookID'),
					'payment_by' 				=> 'lost_book',
					'payment_amount' 			=> $this->input->post('payable_amount'),
					'create_date'      			=> date('Y-m-d H:i:s'),
					'create_userID'    			=> $this->session->userdata('userID'),
					'create_usertypeID'			=> $this->session->userdata('usertypeID'),
					'modify_date'      			=> date('Y-m-d H:i:s'),
					'modify_userID'    			=> $this->session->userdata('userID'),
					'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
				);
				if($this->payment_m->insert_payment($array_payment)){
					$array_update_circulation  = array(
						'return_status'		=> '1',
						'circulation_year'  => date('Y'),
						'circulation_month' => intval(ltrim(date('m'), '0')),
						'penalty_amount'  	=> $this->input->post('payable_amount'),
						'penalty_by'  		=> 'lost_book',
						'modify_date'      	=> date('Y-m-d H:i:s'),
						'modify_userID'    	=> $this->session->userdata('userID'),
						'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
					if($this->circulation_m->update_circulation($array_update_circulation,$this->input->post('circulationID'))){
						$book = $this->book_m->get_single_book(array('bookID' => $this->input->post('bookID')));
						$array_update_book  = array(
							'book_quantity' 			=> $book->book_quantity - 1,
							'modify_date'      			=> date('Y-m-d H:i:s'),
							'modify_userID'    			=> $this->session->userdata('userID'),
							'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
						);
						if($this->book_m->update_book($array_update_book,$this->input->post('bookID'))){
							$array_insert = array(
								'wastage_memberID' 			=> $this->input->post('memberID'),
								'wastage_bookID'   			=> $this->input->post('bookID'),
								'wastage_quantity'   		=> '1',
								'wastage_price'   			=> $this->input->post('payable_amount'),
								'wastage_note'   			=> $this->input->post('lost_note'),
								'create_date'      			=> date('Y-m-d H:i:s'),
								'create_userID'    			=> $this->session->userdata('userID'),
								'create_usertypeID'			=> $this->session->userdata('usertypeID'),
								'modify_date'      			=> date('Y-m-d H:i:s'),
								'modify_userID'    			=> $this->session->userdata('userID'),
								'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
							);
							$this->wastage_m->insert_wastage($array_insert);
							$json = array("confirmation" => 'Success');
							header("Content-Type: application/json", true);
							echo json_encode($json);
						}	
					}		
				}
			}
		}
	}

}
