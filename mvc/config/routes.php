<?php
defined('BASEPATH') OR exit('No direct script access allowed');


spl_autoload_register(function($class) {
    if(strpos($class, 'CI_') !== 0) {
		$file = APPPATH . 'libraries/' . $class . '.php';
		if(file_exists($file) && is_file($file)) {
			@include_once($file);
		}
	}
});




// $route['default_controller'] = 'dashboard';
// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;


if (config_item('installed') == 'false')
{
    $route["default_controller"] = "install";
} else {
	$route['default_controller'] = "login";
}
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
