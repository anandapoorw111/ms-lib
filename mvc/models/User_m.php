<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends MY_Model {

	protected $_table_name  = 'msit_tb_user';
	protected $_primary_key = 'userID';
	protected $_order_by    = "userID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_user($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_user($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_user($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_user($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_user($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_user($id){
		parent::delete($id);
	}

	public function hash($string) {
		return parent::hash($string);
	}
}
