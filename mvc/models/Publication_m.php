<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication_m extends MY_Model {

	protected $_table_name  = 'msit_tb_publication';
	protected $_primary_key = 'publicationID';
	protected $_order_by    = "publicationID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_publication($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_publication($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_publication($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_publication($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_publication($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_publication($id){
		parent::delete($id);
		return TRUE;
	}
}
