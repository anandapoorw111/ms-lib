-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2021 at 05:58 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `msit_library_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_book`
--

CREATE TABLE `msit_tb_book` (
  `bookID` int(11) NOT NULL,
  `book_code` varchar(20) NOT NULL,
  `book_isbn_no` varchar(55) DEFAULT NULL,
  `book_name` varchar(128) NOT NULL,
  `book_category` int(11) DEFAULT NULL,
  `book_photo` varchar(255) DEFAULT NULL,
  `book_price` varchar(60) NOT NULL,
  `book_writerID` varchar(128) DEFAULT NULL,
  `book_publication` varchar(128) DEFAULT NULL,
  `book_edition` varchar(60) DEFAULT NULL,
  `book_edition_year` varchar(60) DEFAULT NULL,
  `book_quantity` varchar(60) DEFAULT NULL,
  `opening_book_quantity` varchar(60) DEFAULT NULL,
  `book_issued_quantity` varchar(11) DEFAULT NULL,
  `book_availability` int(11) DEFAULT NULL,
  `book_rack_no` varchar(60) DEFAULT NULL,
  `book_status` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_bookrequest`
--

CREATE TABLE `msit_tb_bookrequest` (
  `bookrequestID` int(11) NOT NULL,
  `bookrequest_name` varchar(255) NOT NULL,
  `bookrequest_writer_name` varchar(128) DEFAULT NULL,
  `bookrequest_categories` varchar(128) DEFAULT NULL,
  `bookrequest_edition` int(11) DEFAULT NULL,
  `bookrequest_note` varchar(128) DEFAULT NULL,
  `bookrequest_memberID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_categories`
--

CREATE TABLE `msit_tb_categories` (
  `categoriesID` int(11) NOT NULL,
  `categories_code` varchar(50) DEFAULT NULL,
  `categories_name` varchar(255) NOT NULL,
  `categories_note` varchar(128) DEFAULT NULL,
  `categories_status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_circulation`
--

CREATE TABLE `msit_tb_circulation` (
  `circulationID` int(11) NOT NULL,
  `circulation_code` int(11) DEFAULT NULL,
  `memberID` varchar(11) NOT NULL,
  `bookID` varchar(11) NOT NULL,
  `issue_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `penalty_amount` int(11) DEFAULT NULL,
  `penalty_by` varchar(128) DEFAULT NULL,
  `no_of_days` varchar(128) DEFAULT NULL COMMENT 'How many days are being penalty',
  `return_status` int(1) DEFAULT 0 COMMENT '1 = returned. 0= not return',
  `return_date` datetime DEFAULT NULL,
  `circulation_year` varchar(20) DEFAULT NULL,
  `circulation_month` varchar(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_userID` int(11) DEFAULT NULL,
  `create_usertypeID` int(11) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `modify_userID` int(11) DEFAULT NULL,
  `modify_usertypeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_emailsetting`
--

CREATE TABLE `msit_tb_emailsetting` (
  `fieldoption` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msit_tb_emailsetting`
--

INSERT INTO `msit_tb_emailsetting` (`fieldoption`, `value`) VALUES
('email_engine', 'sendmail'),
('smtp_password', '123456'),
('smtp_port', '7210'),
('smtp_security', '435jhh3423'),
('smtp_server', 'localhost'),
('smtp_username', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_member`
--

CREATE TABLE `msit_tb_member` (
  `memberID` int(11) NOT NULL,
  `member_code` varchar(50) DEFAULT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_father_name` varchar(255) DEFAULT NULL,
  `member_mother_name` varchar(255) DEFAULT NULL,
  `member_date_of_birth` datetime DEFAULT NULL,
  `member_nid_no` varchar(128) DEFAULT NULL,
  `member_membership_type` varchar(11) DEFAULT '0',
  `member_occupation` varchar(128) DEFAULT NULL,
  `member_gender` varchar(128) DEFAULT NULL,
  `member_blood_group` varchar(128) DEFAULT NULL,
  `member_religion` varchar(128) DEFAULT NULL,
  `member_email` varchar(128) DEFAULT NULL,
  `member_phone` varchar(20) DEFAULT NULL,
  `member_photo` varchar(255) DEFAULT NULL,
  `member_since_date` datetime NOT NULL,
  `member_address` longtext NOT NULL,
  `member_status` int(11) DEFAULT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_membership`
--

CREATE TABLE `msit_tb_membership` (
  `membershipID` int(11) NOT NULL,
  `membership_code` varchar(50) DEFAULT NULL,
  `membership_name` varchar(255) NOT NULL,
  `membership_books_limit` varchar(11) NOT NULL,
  `membership_days_limit` varchar(11) DEFAULT NULL,
  `membership_fee` varchar(11) DEFAULT '0',
  `penalty_fee` varchar(128) DEFAULT NULL,
  `renew_limit` varchar(128) DEFAULT NULL,
  `membership_status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_menu`
--

CREATE TABLE `msit_tb_menu` (
  `menuID` int(11) NOT NULL,
  `menuname` varchar(128) NOT NULL,
  `menulink` varchar(128) NOT NULL,
  `menuicon` varchar(128) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT 0,
  `parentmenuID` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msit_tb_menu`
--

INSERT INTO `msit_tb_menu` (`menuID`, `menuname`, `menulink`, `menuicon`, `priority`, `parentmenuID`, `status`) VALUES
(1, 'dashboard', 'dashboard', 'icofont-dashboard', 1, 0, 1),
(2, 'member', '#', 'fa fa-users', 0, 0, 1),
(3, 'book', '#', 'icofont-book-alt', 0, 0, 1),
(4, 'circulation', 'circulation', 'icofont-loop', 0, 0, 1),
(5, 'payment', 'payment', 'fa fa-money', 0, 0, 1),
(6, 'bookrequest', 'bookrequest', 'fa fa-book', 0, 0, 1),
(7, 'report', 'report', 'icofont-pie-chart', 0, 0, 1),
(14, 'permissions', '#', 'icofont-paw', 3, 0, 1),
(15, 'usertype', 'usertype', 'fa fa-leaf', 4, 14, 1),
(16, 'permissions', 'permissions', 'fa fa-leaf', 5, 14, 1),
(17, 'permissionlog', 'permissionlog', 'fa fa-leaf', 6, 14, 1),
(18, 'permissionmodule', 'permissionmodule', 'fa fa-leaf', 7, 14, 1),
(19, 'menu', 'menu', 'fa fa-leaf', 8, 14, 1),
(23, 'Settings', '#', 'icofont-ui-settings', 20, 0, 1),
(24, 'membership', 'membership', 'fa fa-leaf', 21, 23, 1),
(25, 'categories', 'categories', 'fa fa-leaf', 0, 3, 1),
(26, 'sitesettings', 'settings', 'fa fa-leaf', 3, 23, 1),
(27, 'user', 'user', 'fa fa-users', 2, 23, 1),
(36, 'publication', 'publication', 'fa fa-leaf', 0, 23, 1),
(41, 'book_archive', 'book', 'fa fa-leaf', 0, 3, 1),
(42, 'wastage', 'wastage', 'fa fa-leaf', 0, 3, 1),
(45, 'writer', 'writer', 'fa fa-leaf', 0, 3, 1),
(46, 'emailsetting', 'emailsetting', 'fa fa-leaf', 0, 23, 1),
(47, 'memberlist', 'member', 'fa fa-leaf', 0, 2, 1),
(48, 'genarateid', 'genarateid', 'fa fa-leaf', 0, 2, 1),
(49, 'import', 'import', 'fa fa-leaf', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_payment`
--

CREATE TABLE `msit_tb_payment` (
  `paymentID` int(11) NOT NULL,
  `payment_code` varchar(20) DEFAULT NULL,
  `payment_memberID` int(11) DEFAULT NULL,
  `payment_bookID` int(11) NOT NULL,
  `payment_by` varchar(128) DEFAULT NULL COMMENT 'membership_fee, date_expiry_fee, loss_book_fee',
  `payment_amount` varchar(255) DEFAULT NULL,
  `no_of_days` varchar(128) DEFAULT NULL COMMENT 'How many days are being penalty',
  `payment_status` int(11) DEFAULT 1 COMMENT 'this column used for.membership fee payment. 1=active, 0=inactive',
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_permissionlog`
--

CREATE TABLE `msit_tb_permissionlog` (
  `permissionlogID` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissionmoduleID` int(11) NOT NULL,
  `active` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msit_tb_permissionlog`
--

INSERT INTO `msit_tb_permissionlog` (`permissionlogID`, `name`, `description`, `permissionmoduleID`, `active`) VALUES
(1, 'dashboard', 'dashboard', 0, 'yes'),
(2, 'user', 'user module', 1, 'yes'),
(3, 'user_add', 'user add', 1, 'yes'),
(4, 'user_edit', 'user edit', 1, 'yes'),
(5, 'user_delete', 'user delete', 1, 'yes'),
(6, 'usertype', 'usertype', 3, 'yes'),
(7, 'usertype_add', 'usertype add', 3, 'yes'),
(8, 'usertype_edit', 'usertype edit', 3, 'yes'),
(9, 'usertype_delete', 'usertype delete', 3, 'yes'),
(14, 'permissions', 'permissions', 0, 'yes'),
(51, 'user_view', 'User View', 1, 'yes'),
(88, 'settings', 'settings', 20, 'yes'),
(89, 'settings_edit', 'settings edit', 20, 'yes'),
(166, 'membership', 'membership', 34, 'yes'),
(167, 'membership_add', 'membership add', 34, 'yes'),
(168, 'membership_edit', 'membership edit', 34, 'yes'),
(169, 'membership_delete', 'membership delete', 34, 'yes'),
(170, 'categories', 'categories', 33, 'yes'),
(171, 'categories_add', 'categories add', 33, 'yes'),
(172, 'categories_edit', 'categories edit', 33, 'yes'),
(173, 'categories_delete', 'categories delete', 33, 'yes'),
(174, 'membership_retrive', 'membership retrive', 34, 'yes'),
(175, 'membership_status', 'membership status', 34, 'yes'),
(176, 'categories_status', 'categories status', 33, 'yes'),
(177, 'categories_retrive', 'categories retrive', 33, 'yes'),
(178, 'book', 'book', 35, 'yes'),
(179, 'book_add', 'book add', 35, 'yes'),
(180, 'book_edit', 'book edit', 35, 'yes'),
(181, 'book_delete', 'book delete', 35, 'yes'),
(182, 'book_view', 'book view', 35, 'yes'),
(183, 'book_status', 'book status', 35, 'yes'),
(184, 'book_retrive', 'book retrive', 35, 'yes'),
(185, 'publication', 'publication', 36, 'yes'),
(186, 'publication_add', 'publication add', 36, 'yes'),
(187, 'publication_edit', 'publication edit', 36, 'yes'),
(188, 'publication_delete', 'publication delete', 36, 'yes'),
(189, 'publication_status', 'publication status', 36, 'yes'),
(190, 'publication_retrive', 'publications retrive', 36, 'yes'),
(191, 'book_barcode', 'book barcode', 35, 'yes'),
(192, 'member', 'member', 37, 'yes'),
(193, 'member_add', 'member add', 37, 'yes'),
(194, 'member_edit', 'member edit', 37, 'yes'),
(195, 'member_delete', 'member delete', 37, 'yes'),
(196, 'member_view', 'member view', 37, 'yes'),
(197, 'member_status', 'member status', 37, 'yes'),
(198, 'member_changepassword', 'member changepassword', 37, 'yes'),
(199, 'user_changepassword', 'user_changepassword', 1, 'yes'),
(200, 'circulation', 'circulation', 38, 'yes'),
(201, 'circulation_search', 'circulation search', 38, 'yes'),
(202, 'circulation_view', 'circulation view', 38, 'yes'),
(203, 'circulation_add', 'circulation add', 38, 'yes'),
(204, 'circulation_renewal', 'circulation renewal', 38, 'yes'),
(205, 'circulation_return', 'circulation_return', 38, 'yes'),
(206, 'circulation_penalty', 'circulation penalty', 38, 'yes'),
(207, 'circulation_lost', 'circulation lost', 38, 'yes'),
(208, 'wastage', 'wastage', 39, 'yes'),
(209, 'wastage_add', 'wastage add', 39, 'yes'),
(210, 'wastage_restore', 'wastage restore', 39, 'yes'),
(211, 'payment', 'payment', 40, 'yes'),
(212, 'payment_status', 'payment status', 40, 'yes'),
(213, 'payment_view', 'payment view', 40, 'yes'),
(214, 'report', 'report', 27, 'yes'),
(215, 'report_member', 'report member', 27, 'yes'),
(216, 'report_book', 'report book', 27, 'yes'),
(217, 'report_payment', 'report payment', 27, 'yes'),
(218, 'report_categories', 'report categories', 27, 'yes'),
(219, 'writer', 'writer', 43, 'yes'),
(220, 'writer_add', 'writer add', 43, 'yes'),
(221, 'writer_edit', 'writer edit', 43, 'yes'),
(222, 'writer_delete', 'writer delete', 43, 'yes'),
(223, 'writer_status', 'writer status', 43, 'yes'),
(224, 'writer_retrive', 'writer retrive', 43, 'yes'),
(225, 'emailsetting', 'emailsetting', 44, 'yes'),
(226, 'bookrequest', 'bookrequest', 45, 'yes'),
(227, 'bookrequest_add', 'bookrequest add', 45, 'yes'),
(228, 'bookrequest_edit', 'bookrequest  edit', 45, 'yes'),
(229, 'bookrequest_delete', 'bookrequest delete', 45, 'yes'),
(230, 'bookrequest_retrive', 'bookrequest retrive', 45, 'yes'),
(231, 'genarateid', 'genarateid', 46, 'yes'),
(232, 'genarateid_qrcode', 'genarateid qrcode', 46, 'yes'),
(233, 'genarateid_print', 'Genarateid  print', 46, 'yes'),
(234, 'import', 'import', 47, 'yes'),
(235, 'import_exportbooklist', 'export book list', 47, 'yes'),
(236, 'import_exportwriterlist', 'export writer list', 47, 'yes'),
(237, 'import_exportpublicationlist', 'export publication list', 47, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_permissionmodule`
--

CREATE TABLE `msit_tb_permissionmodule` (
  `permissionmoduleID` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msit_tb_permissionmodule`
--

INSERT INTO `msit_tb_permissionmodule` (`permissionmoduleID`, `name`, `description`, `active`) VALUES
(1, 'User', 'User Module', 1),
(3, 'Usertype', 'user type', 1),
(4, 'Permissionlog', 'permissionlog', 1),
(5, 'Permissionmodule', 'permissionmodule', 1),
(6, 'Menu', 'menu module', 1),
(18, 'taxs', 'tax Module', 1),
(19, 'units', 'Units module', 1),
(20, 'settings', 'settings', 1),
(27, 'report', 'report_module', 1),
(33, 'categories', 'categories module', 1),
(34, 'membership', 'membership module', 1),
(35, 'book', 'book module', 1),
(36, 'publication', 'publication module', 1),
(37, 'member', 'member module', 1),
(38, 'circulation', 'circulation module', 1),
(39, 'wastage', 'wastage module', 1),
(40, 'payment', 'payment module', 1),
(43, 'writer', 'writer module', 1),
(44, 'emailsetting', 'emailsetting module', 1),
(45, 'bookrequest', 'bookrequest module', 1),
(46, 'genarateid', 'genarateid module', 1),
(47, 'import', 'import', 1);

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_permissions`
--

CREATE TABLE `msit_tb_permissions` (
  `permissionsID` int(11) NOT NULL,
  `permissionlogID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `msit_tb_permissions`
--

INSERT INTO `msit_tb_permissions` (`permissionsID`, `permissionlogID`, `usertypeID`) VALUES
(2181, 1, 10),
(2182, 166, 10),
(2183, 170, 10),
(2184, 178, 10),
(2185, 182, 10),
(2186, 184, 10),
(2187, 191, 10),
(2188, 185, 10),
(2189, 192, 10),
(2190, 193, 10),
(2191, 196, 10),
(2192, 198, 10),
(2193, 200, 10),
(2194, 203, 10),
(2195, 202, 10),
(2196, 201, 10),
(2197, 204, 10),
(2198, 205, 10),
(2199, 206, 10),
(2200, 207, 10),
(2201, 208, 10),
(2202, 211, 10),
(2203, 213, 10),
(2204, 214, 10),
(2205, 215, 10),
(2206, 216, 10),
(2207, 217, 10),
(2208, 218, 10),
(2209, 219, 10),
(2210, 231, 10),
(2211, 232, 10),
(2293, 1, 11),
(2294, 14, 11),
(2295, 2, 11),
(2296, 3, 11),
(2297, 4, 11),
(2298, 5, 11),
(2299, 51, 11),
(2300, 199, 11),
(2301, 6, 11),
(2302, 7, 11),
(2303, 8, 11),
(2304, 9, 11),
(2305, 88, 11),
(2306, 89, 11),
(2307, 170, 11),
(2308, 171, 11),
(2309, 172, 11),
(2310, 173, 11),
(2311, 176, 11),
(2312, 177, 11),
(2313, 178, 11),
(2314, 179, 11),
(2315, 180, 11),
(2316, 181, 11),
(2317, 182, 11),
(2318, 183, 11),
(2319, 184, 11),
(2320, 191, 11),
(2321, 185, 11),
(2322, 186, 11),
(2323, 187, 11),
(2324, 188, 11),
(2325, 189, 11),
(2326, 190, 11),
(2327, 192, 11),
(2328, 193, 11),
(2329, 194, 11),
(2330, 195, 11),
(2331, 196, 11),
(2332, 197, 11),
(2333, 198, 11),
(2334, 200, 11),
(2335, 203, 11),
(2336, 202, 11),
(2337, 201, 11),
(2338, 204, 11),
(2339, 205, 11),
(2340, 206, 11),
(2341, 207, 11),
(2342, 208, 11),
(2343, 209, 11),
(2344, 210, 11),
(2345, 214, 11),
(2346, 215, 11),
(2347, 216, 11),
(2348, 217, 11),
(2349, 218, 11),
(2350, 219, 11),
(2351, 220, 11),
(2352, 221, 11),
(2353, 222, 11),
(2354, 223, 11),
(2355, 224, 11),
(2356, 225, 11),
(2357, 226, 11),
(2358, 227, 11),
(2359, 228, 11),
(2360, 229, 11),
(2361, 230, 11),
(2362, 231, 11),
(2363, 232, 11),
(2364, 233, 11),
(2530, 1, 1),
(2531, 14, 1),
(2532, 2, 1),
(2533, 3, 1),
(2534, 4, 1),
(2535, 5, 1),
(2536, 51, 1),
(2537, 199, 1),
(2538, 6, 1),
(2539, 7, 1),
(2540, 8, 1),
(2541, 9, 1),
(2542, 88, 1),
(2543, 89, 1),
(2544, 166, 1),
(2545, 167, 1),
(2546, 168, 1),
(2547, 169, 1),
(2548, 174, 1),
(2549, 175, 1),
(2550, 170, 1),
(2551, 171, 1),
(2552, 172, 1),
(2553, 173, 1),
(2554, 176, 1),
(2555, 177, 1),
(2556, 178, 1),
(2557, 179, 1),
(2558, 180, 1),
(2559, 181, 1),
(2560, 182, 1),
(2561, 183, 1),
(2562, 184, 1),
(2563, 191, 1),
(2564, 185, 1),
(2565, 186, 1),
(2566, 187, 1),
(2567, 188, 1),
(2568, 189, 1),
(2569, 190, 1),
(2570, 192, 1),
(2571, 193, 1),
(2572, 194, 1),
(2573, 195, 1),
(2574, 196, 1),
(2575, 197, 1),
(2576, 198, 1),
(2577, 200, 1),
(2578, 203, 1),
(2579, 202, 1),
(2580, 201, 1),
(2581, 204, 1),
(2582, 205, 1),
(2583, 206, 1),
(2584, 207, 1),
(2585, 208, 1),
(2586, 209, 1),
(2587, 210, 1),
(2588, 211, 1),
(2589, 213, 1),
(2590, 212, 1),
(2591, 214, 1),
(2592, 215, 1),
(2593, 216, 1),
(2594, 217, 1),
(2595, 218, 1),
(2596, 219, 1),
(2597, 220, 1),
(2598, 221, 1),
(2599, 222, 1),
(2600, 223, 1),
(2601, 224, 1),
(2602, 225, 1),
(2603, 226, 1),
(2604, 227, 1),
(2605, 228, 1),
(2606, 229, 1),
(2607, 230, 1),
(2608, 231, 1),
(2609, 232, 1),
(2610, 233, 1),
(2611, 234, 1),
(2612, 235, 1),
(2613, 236, 1),
(2614, 237, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_publication`
--

CREATE TABLE `msit_tb_publication` (
  `publicationID` int(11) NOT NULL,
  `publication_name` varchar(255) NOT NULL,
  `publication_note` varchar(128) DEFAULT NULL,
  `publication_status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_settings`
--

CREATE TABLE `msit_tb_settings` (
  `settingsID` int(11) NOT NULL,
  `field_option` varchar(255) NOT NULL,
  `value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_units`
--

CREATE TABLE `msit_tb_units` (
  `unitsID` int(11) NOT NULL,
  `units_name` varchar(255) NOT NULL,
  `units_note` varchar(60) NOT NULL,
  `units_status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_user`
--

CREATE TABLE `msit_tb_user` (
  `userID` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `religion` varchar(30) DEFAULT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `jod` date DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(128) NOT NULL,
  `forgot_password` varchar(64) DEFAULT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_usertype`
--

CREATE TABLE `msit_tb_usertype` (
  `usertypeID` int(11) UNSIGNED NOT NULL,
  `usertype` varchar(30) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msit_tb_usertype`
--

INSERT INTO `msit_tb_usertype` (`usertypeID`, `usertype`, `create_date`, `create_userID`, `create_usertypeID`, `modify_date`, `modify_userID`, `modify_usertypeID`) VALUES
(1, 'admin', '2018-11-20 00:00:00', 11, 11, '2020-03-03 23:14:39', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_wastage`
--

CREATE TABLE `msit_tb_wastage` (
  `wastageID` int(11) NOT NULL,
  `wastage_memberID` int(11) DEFAULT NULL,
  `wastage_bookID` int(11) NOT NULL,
  `wastage_quantity` varchar(60) NOT NULL,
  `wastage_price` varchar(60) DEFAULT NULL,
  `wastage_note` varchar(60) NOT NULL,
  `wastage_by` int(11) NOT NULL DEFAULT 0 COMMENT '1= library, 0 = member',
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msit_tb_writer`
--

CREATE TABLE `msit_tb_writer` (
  `writerID` int(11) NOT NULL,
  `writer_code` varchar(50) DEFAULT NULL,
  `writer_name` varchar(255) NOT NULL,
  `writer_note` varchar(128) DEFAULT NULL,
  `writer_status` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_userID` int(11) NOT NULL,
  `modify_usertypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `msit_tb_book`
--
ALTER TABLE `msit_tb_book`
  ADD PRIMARY KEY (`bookID`);

--
-- Indexes for table `msit_tb_bookrequest`
--
ALTER TABLE `msit_tb_bookrequest`
  ADD PRIMARY KEY (`bookrequestID`);

--
-- Indexes for table `msit_tb_categories`
--
ALTER TABLE `msit_tb_categories`
  ADD PRIMARY KEY (`categoriesID`);

--
-- Indexes for table `msit_tb_circulation`
--
ALTER TABLE `msit_tb_circulation`
  ADD PRIMARY KEY (`circulationID`);

--
-- Indexes for table `msit_tb_emailsetting`
--
ALTER TABLE `msit_tb_emailsetting`
  ADD PRIMARY KEY (`fieldoption`);

--
-- Indexes for table `msit_tb_member`
--
ALTER TABLE `msit_tb_member`
  ADD PRIMARY KEY (`memberID`);

--
-- Indexes for table `msit_tb_membership`
--
ALTER TABLE `msit_tb_membership`
  ADD PRIMARY KEY (`membershipID`);

--
-- Indexes for table `msit_tb_menu`
--
ALTER TABLE `msit_tb_menu`
  ADD PRIMARY KEY (`menuID`);

--
-- Indexes for table `msit_tb_payment`
--
ALTER TABLE `msit_tb_payment`
  ADD PRIMARY KEY (`paymentID`);

--
-- Indexes for table `msit_tb_permissionlog`
--
ALTER TABLE `msit_tb_permissionlog`
  ADD PRIMARY KEY (`permissionlogID`);

--
-- Indexes for table `msit_tb_permissionmodule`
--
ALTER TABLE `msit_tb_permissionmodule`
  ADD PRIMARY KEY (`permissionmoduleID`);

--
-- Indexes for table `msit_tb_permissions`
--
ALTER TABLE `msit_tb_permissions`
  ADD PRIMARY KEY (`permissionsID`);

--
-- Indexes for table `msit_tb_publication`
--
ALTER TABLE `msit_tb_publication`
  ADD PRIMARY KEY (`publicationID`);

--
-- Indexes for table `msit_tb_settings`
--
ALTER TABLE `msit_tb_settings`
  ADD PRIMARY KEY (`settingsID`),
  ADD UNIQUE KEY `field_option` (`field_option`);

--
-- Indexes for table `msit_tb_units`
--
ALTER TABLE `msit_tb_units`
  ADD PRIMARY KEY (`unitsID`);

--
-- Indexes for table `msit_tb_user`
--
ALTER TABLE `msit_tb_user`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `msit_tb_usertype`
--
ALTER TABLE `msit_tb_usertype`
  ADD PRIMARY KEY (`usertypeID`);

--
-- Indexes for table `msit_tb_wastage`
--
ALTER TABLE `msit_tb_wastage`
  ADD PRIMARY KEY (`wastageID`);

--
-- Indexes for table `msit_tb_writer`
--
ALTER TABLE `msit_tb_writer`
  ADD PRIMARY KEY (`writerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `msit_tb_book`
--
ALTER TABLE `msit_tb_book`
  MODIFY `bookID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_bookrequest`
--
ALTER TABLE `msit_tb_bookrequest`
  MODIFY `bookrequestID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_categories`
--
ALTER TABLE `msit_tb_categories`
  MODIFY `categoriesID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_circulation`
--
ALTER TABLE `msit_tb_circulation`
  MODIFY `circulationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_member`
--
ALTER TABLE `msit_tb_member`
  MODIFY `memberID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_membership`
--
ALTER TABLE `msit_tb_membership`
  MODIFY `membershipID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_menu`
--
ALTER TABLE `msit_tb_menu`
  MODIFY `menuID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `msit_tb_payment`
--
ALTER TABLE `msit_tb_payment`
  MODIFY `paymentID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_permissionlog`
--
ALTER TABLE `msit_tb_permissionlog`
  MODIFY `permissionlogID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=238;

--
-- AUTO_INCREMENT for table `msit_tb_permissionmodule`
--
ALTER TABLE `msit_tb_permissionmodule`
  MODIFY `permissionmoduleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `msit_tb_permissions`
--
ALTER TABLE `msit_tb_permissions`
  MODIFY `permissionsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2615;

--
-- AUTO_INCREMENT for table `msit_tb_publication`
--
ALTER TABLE `msit_tb_publication`
  MODIFY `publicationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_settings`
--
ALTER TABLE `msit_tb_settings`
  MODIFY `settingsID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_units`
--
ALTER TABLE `msit_tb_units`
  MODIFY `unitsID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_user`
--
ALTER TABLE `msit_tb_user`
  MODIFY `userID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_usertype`
--
ALTER TABLE `msit_tb_usertype`
  MODIFY `usertypeID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `msit_tb_wastage`
--
ALTER TABLE `msit_tb_wastage`
  MODIFY `wastageID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msit_tb_writer`
--
ALTER TABLE `msit_tb_writer`
  MODIFY `writerID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
