<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_m extends MY_Model {


	function get_circulation($value){
		$query = $this->db->limit('5')->order_by('circulationID','desc')->get_where('msit_tb_circulation',$value);
		return $query->result();
	}

	public function get_issued_chart($value){
		$this->db->group_by('circulation_month');
		$this->db->select('count(circulationID) as circulationID, circulation_month');
		$query = $this->db->get_where('msit_tb_circulation',array('circulation_year' => $value, 'return_status' => 0));
		return $query->result();
	}
	
	public function get_returned_chart($value){
		$this->db->group_by('circulation_month');
		$this->db->select('count(circulationID) as circulationID, circulation_month');
		$query = $this->db->get_where('msit_tb_circulation',array('circulation_year' => $value, 'return_status' => 1));
		return $query->result();
	}
}
