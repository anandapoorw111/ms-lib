<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_end_m extends CI_Model{

	public function get_slider(){
		$query = $this->db->get_where('msit_tb_slider', array('status' => '1'));
		return  $query->result();
	}

	public function get_testimonials(){
		$query = $this->db->get_where('msit_tb_testimonials', array('testimonials_status' => '1'));
		return  $query->result();
	}

	public function get_sisterconcern(){
		$query = $this->db->get_where('msit_tb_sisterconcern', array('status' => '1'));
		return  $query->result();
	}

	public function get_last3_news(){
		$query = $this->db->limit('3')->order_by('newsID','desc')->get('msit_tb_news');
		return $query->result();
	}

	public function get_single_news($id){
		$query = $this->db->get_where('msit_tb_news', array('newsID' => $id));
		return $query->row();
	}

	public function get_last3_events(){
		$query = $this->db->limit('3')->order_by('eventsID','desc')->get('msit_tb_events');
		return $query->result();
	}

	public function get_single_event($id){
		$query = $this->db->get_where('msit_tb_events', array('eventsID' => $id));
		return $query->row();
	}

	public function insert_newsletter($array){
		$this->db->insert('msit_tb_newsletter', $array);
		return TRUE;
	}	

	public function insert_email($array){
		$this->db->insert('msit_tb_email', $array);
		return TRUE;
	}
}
