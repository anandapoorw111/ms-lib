<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends MY_Model {

	protected $_table_name  = 'msit_tb_user';
	protected $_primary_key = 'userID';
	protected $_order_by    = "userID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_login($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_login($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_login($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_login($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_login($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_login($id){
		parent::delete($id);
	}

	public function get_order_by_reset_data($array=NULL) {
		$query = parent::get_order_by($array);
		$query = $this->db->get_where('msit_tb_user', array('forgot_password' => $array));
     	return $query->row();
	}

}
