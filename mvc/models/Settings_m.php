<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_m extends MY_Model{

	protected $_table_name  = 'msit_tb_settings';
	protected $_primary_key = 'settingsID';
	protected $_order_by    = "settingsID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_settings($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}	

	public function get_settings_data($id = 1) {
		$array = array();
		$query = $this->db->get('msit_tb_settings');
		if(inicompute($query))
		{
			foreach ($query->result() as $row)
			{
				$array[$row->field_option] = $row->value;
			}
		}
		return (object) $array;
	}

	public function get_order_by_settings($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_settings($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_settings($array){
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_settings($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_settings($id){
		parent::delete($id);
		return TRUE;
	}

	public function insert_or_update($arrays){
		foreach ($arrays as $key => $array) {
			$this->db->query("INSERT INTO msit_tb_settings(field_option, value) VALUES ('".$key."', '".$array."') ON DUPLICATE KEY UPDATE field_option='".$key."' , value='".$array."'");
		}
		return TRUE;
	}
}
