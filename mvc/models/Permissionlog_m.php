<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissionlog_m extends MY_Model {

	protected $_table_name  = 'msit_tb_permissionlog';
	protected $_primary_key = 'permissionlogID';
	protected $_order_by    = "permissionlogID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_permissionlog($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_permissionlog($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_permissionlog($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_permissionlog($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_permissionlog($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_permissionlog($id){
		parent::delete($id);
	}

}
