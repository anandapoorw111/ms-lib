<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wastage_m extends MY_Model {

	protected $_table_name  = 'msit_tb_wastage';
	protected $_primary_key = 'wastageID';
	protected $_order_by    = "wastageID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_wastage($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_wastage($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_wastage($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_wastage($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_wastage($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_wastage($id){
		parent::delete($id);
		return TRUE;
	}

	public function hash($string) {
		return parent::hash($string);
	}
}
