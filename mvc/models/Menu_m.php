<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_m extends MY_Model {

	protected $_table_name  = 'msit_tb_menu';
	protected $_primary_key = 'menuID';
	protected $_order_by    = "menuID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_menu($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_menu($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_menu($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_menu($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_menu($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_menu($id){
		parent::delete($id);
	}

	public function get_where_in_menu_by_menulink($array, $column, $whereArray) {
		$this->db->select('*');
		$this->db->where_in($column, $array);
		if(count($whereArray)) {
			$this->db->where($whereArray);
		}
		return $this->db->get($this->_table_name)->result();
	}

}
