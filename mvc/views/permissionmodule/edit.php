<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Permission Module
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=base_url('/permissionmodule')?>"><i class="fa fa-dashboard"></i> Permission Module</a></li>
        <li><a href="<?=base_url('/permissionmodule/add')?>"><i class="fa fa-dashboard"></i> Edit</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="row">
          <div class="col-md-6">
            <!-- /.box-header -->
            <form role="form" method="POST">
              <div class="box-body">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="form-group <?=form_error('name') ? 'has-error' : ''?>">
                  <label for="name">Name</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('name',$permissionmodule->name)?>" id="name" name="name"/>
                  <?=form_error('name')?>
                </div>
                <div class="form-group <?=form_error('description') ? 'has-error' : ''?>">
                  <label for="description">Description</label> <span class="text-red">*</span>
                  <textarea name="description" id="description" cols="30" rows="3" class="form-control"><?=set_value('description',$permissionmodule->description)?></textarea>
                  <?=form_error('description')?>
                </div>
                <div class="form-group <?=form_error('active') ? 'has-error' : ''?>">
                  <label for="active">Active</label> <span class="text-red">*</span>
                  <select name="active" id="active" class="form-control">
                    <option value="0" <?=set_select('active','0',($permissionmodule->active == 0) ? TRUE : FALSE)?>>Please Select</option>
                    <option value="1" <?=set_select('active','1',($permissionmodule->active == 1) ? TRUE : FALSE)?>>Active</option>
                    <option value="2" <?=set_select('active','2',($permissionmodule->active == 2) ? TRUE : FALSE)?>>Disable</option>
                  </select>
                  <?=form_error('active')?>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update Permissionmodule</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->