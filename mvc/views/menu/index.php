<div class="content-wrapper">
    <section class="content-header">
		<h1>Menu</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Menu</li>
		</ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <a href="<?=base_url('menu/add')?>" class="btn btn-inline btn-primary btn-md"><i class="fa fa-plus"></i> Add Menu</a>
            </div>
            <div class="box-body">
                <div id="hide-table">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Menu Link</th>
                                <th>Menu Icon</th>
                                <th>Priority</th>
                                <th>Parent Menu</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($menus)) { $i=0; foreach($menus as $menu) { $i++; ?>
                            <tr>
                                <td data-title="#"><?=$i?></td>
                                <td data-title="Menu Name"><?=ucfirst($menu->menuname)?></td>
                                <td data-title="Menu Link"><?=$menu->menulink?></td>
                                <td data-title="Menu Icon"><?=$menu->menuicon?></td>
                                <td data-title="Priority"><?=$menu->priority?></td>
                                <td data-title="Parent Menu"><?=isset($menusArray[$menu->parentmenuID]) ? ucfirst($menusArray[$menu->parentmenuID]) : ''?></td>
                                <td data-title="Status"><?=($menu->status ==1) ? "<span class='btn btn-success btn-sm'>Active</span>" : "<span class='btn btn-danger btn-sm'>Block</span>"?></td>
                                <td data-title="Action">
                                    <?=btn_edit_show('menu/edit/'.$menu->menuID,'Edit')?>
                                    <?=btn_delete_show('menu/delete/'.$menu->menuID,'Delete')?>
                                </td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Menu Link</th>
                                <th>Menu Icon</th>
                                <th>Priority</th>
                                <th>Parent Menu</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
  $(function () {
    $('#example1').DataTable({
      'pageLength':25,
    });
  })
</script>