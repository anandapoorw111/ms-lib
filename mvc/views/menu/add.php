<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
  		<h1>
  			Menu 
  		</h1>
  		<ol class="breadcrumb">
        <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
  			<li><a href="<?=base_url('/menu')?>"><i class="fa fa-dashboard"></i> Menu</a></li>
  			<li class="active">Add</li>
  		</ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="row">
          <div class="col-md-6">
            <!-- /.box-header -->
            <form role="form" method="POST">
              <div class="box-body">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="form-group <?=form_error('menuname') ? 'has-error' : ''?>">
                  <label for="menuname">Menu Name</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('menuname')?>" id="menuname" name="menuname"/>
                  <?=form_error('menuname')?>
                </div>
                <div class="form-group <?=form_error('menulink') ? 'has-error' : ''?>">
                  <label for="menulink">Menu Link</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('menulink')?>" id="menulink" name="menulink"/>
                  <?=form_error('menulink')?>
                </div>
                <div class="form-group <?=form_error('menuicon') ? 'has-error' : ''?>">
                  <label for="menuicon">Menu Icon</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('menuicon','fa fa-leaf')?>" id="menuicon" name="menuicon"/>
                  <?=form_error('menuicon')?>
                </div>
                <div class="form-group <?=form_error('priority') ? 'has-error' : ''?>">
                  <label for="priority">Priority</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('priority','0')?>" id="priority" name="priority"/>
                  <?=form_error('priority')?>
                </div>
                <div class="form-group <?=form_error('parentmenuID') ? 'has-error' : ''?>">
                  <label for="parentmenuID">Parent Menu ID</label> <span class="text-red">*</span>
                  <?php
                    $parentmenuArray['0'] = "Please Select";
                    if(count($parentmenus)) {
                      foreach($parentmenus as $parentmenu) {
                        $parentmenuArray[$parentmenu->menuID] = $parentmenu->menuname;
                      }
                    }
                    echo form_dropdown('parentmenuID', $parentmenuArray, set_value('status') , 'class="form-control"');
                    form_error('parentmenuID');
                  ?>
                </div>
                <div class="form-group <?=form_error('status') ? 'has-error' : ''?>">
                  <label for="status">Status</label> <span class="text-red">*</span>
                  <?php 
                    $statusArray['0'] = "Please Select"; 
                    $statusArray['1'] = "Active"; 
                    $statusArray['2'] = "Disable"; 
                    echo form_dropdown('status', $statusArray, set_value('status') , 'class="form-control"'); ?>
                  <?=form_error('status')?>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add Menu</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper