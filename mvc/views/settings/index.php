
<div class="content-wrapper">
    <section class="content-header">
      <h1><?=$this->lang->line('settings_settings')?></h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
        <li><a href="<?=base_url('/user')?>"><?=$this->lang->line('settings_settings')?></a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid">
        <div class="box-header header-custom">
            <h5 class="box-title"><i class="icofont-ui-settings"></i> <?=$this->lang->line('settings_settings')?></h5>
        </div>
        <div class="box-body">
            <form method="post" enctype="multipart/form-data">
                <fieldset class="border-fieldset">
                    <legend class="border-legend"><?=$this->lang->line('settings_general_setting')?></legend>

                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('company_name') ? 'has-error' : ''?>">
                                    <label for="company_name"><?=$this->lang->line('settings_company_name')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="company_name" name="company_name" value="<?=set_value('company_name',(!empty(settings()->company_name)?settings()->company_name:''))?>" placeholder="<?=$this->lang->line('settings_company_name')?>">
                                    <?=form_error('company_name', '<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tag_line"><?=$this->lang->line('settings_tag_line')?></label>
                                    <input type="text" class="form-control" id="tag_line" name="tag_line" value="<?=set_value('tag_line',(!empty(settings()->tag_line)?settings()->tag_line:''))?>" placeholder="<?=$this->lang->line('settings_tag_line')?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                             <div class="form-group">
                                <label for="type"><?=$this->lang->line('settings_type')?></label>
                                <input type="text" class="form-control" id="type" name="type" value="<?=set_value('type',(!empty(settings()->type)?settings()->type:''))?>" placeholder="<?=$this->lang->line('settings_type')?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('owner') ? 'has-error' : ''?>">
                                <label for="owner"><?=$this->lang->line('settings_owner')?></label> <span class="text-red">*</span>
                                <!-- < ?=(settings()->owner ? set_value('owner', settings()->owner))?> -->
                                <input type="text" class="form-control" id="owner" name="owner" value="<?=set_value('owner',(!empty(settings()->owner)?settings()->owner:''))?>" placeholder="<?=$this->lang->line('settings_owner')?>">
                                <?=form_error('owner','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('mobile_no') ? 'has-error' : ''?>">
                                <label for="mobile_no"><?=$this->lang->line('settings_mobile')?></label> <span class="text-red">*</span>
                                <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="<?=set_value('mobile_no',(!empty(settings()->mobile_no)?settings()->mobile_no:''))?>" placeholder="<?=$this->lang->line('settings_mobile')?>">
                                <?=form_error('mobile_no','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="phone"><?=$this->lang->line('settings_phone')?></label>
                                <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone',(!empty(settings()->phone)?settings()->phone:''))?>" placeholder="<?=$this->lang->line('settings_phone')?>">

                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="fax_no"><?=$this->lang->line('settings_fax')?></label>
                                <input type="text" class="form-control" id="fax_no" name="fax_no" value="<?=set_value('fax_no',(!empty(settings()->fax_no)?settings()->fax_no:''))?>" placeholder="<?=$this->lang->line('settings_fax')?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('email') ? 'has-error' : ''?>">
                                <label for="email"><?=$this->lang->line('settings_email')?></label> <span class="text-red">*</span>
                                <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email',(!empty(settings()->email)?settings()->email:''))?>" placeholder="<?=$this->lang->line('settings_email')?>">
                                <?=form_error('email','<div class="text-red">', '</div>')?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('currency_code') ? 'has-error' : ''?>">
                                <label for="currency_code"><?=$this->lang->line('settings_currency_code')?></label> <span class="text-red">*</span>
                                <input name="currency_code" value="<?=set_value('currency_code',(!empty(settings()->currency_code)?settings()->currency_code:''))?>" id="currency_code" class="form-control" placeholder="<?=$this->lang->line('settings_currency_code')?>">
                                <?=form_error('currency_code','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('currency_symbol') ? 'has-error' : ''?>">
                                <label for="currency_symbol"><?=$this->lang->line('settings_currency_symbol')?></label> <span class="text-red">*</span>
                                <input name="currency_symbol" value="<?=set_value('currency_symbol',(!empty(settings()->currency_symbol)?settings()->currency_symbol:''))?>" id="currency_symbol" class="form-control" placeholder="<?=$this->lang->line('settings_currency_symbol')?>">
                                <?=form_error('currency_symbol','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('time_zone') ? 'has-error' : ''?>">
                                <label for="time_zone"><?=$this->lang->line("settings_time_zone")?>&nbsp; <span class="text-red">*</span></label>
                                <?php
                                $path = APPPATH."config/timezones_class.php";
                                if(@include($path)){
                                    $timezones_cls = new Timezones();
                                    $timezones = $timezones_cls->get_timezones();
                                    unset($timezones['']);
                                    $selectTimeZone['0'] = $this->lang->line('settings_time_zone');
                                    $timeZones = array_merge($selectTimeZone, $timezones);
                                    // , settings()->time_zone
                                    echo form_dropdown("time_zone", $timeZones, set_value("time_zone",(!empty(settings()->time_zone)?settings()->time_zone:'')), "id='time_zone' class='form-control select2'");
                                }
                                ?>
                                <span class="control-label">
                                    <?=form_error('time_zone'); ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('logo') ? 'has-error' : ''?>">
                                <label for="logo"><?=$this->lang->line('settings_logo')?></label> 
                                <input type="file" class="form-control" id="logo" name="logo"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tax_no"><?=$this->lang->line('settings_tax')?></label>
                                <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?=set_value('tax_no',(!empty(settings()->tax_no)?settings()->tax_no:''))?>" placeholder="<?=$this->lang->line('settings_tax')?>">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><?=$this->lang->line('settings_language')?></label>
                                <?php
                                    $languageArray['Arabic']    = "Arabic";
                                    $languageArray['Bangla']    = "Bangla";
                                    $languageArray['English']   = "English";
                                    $languageArray['French']    = "French";
                                    $languageArray['German']    = "Germany";
                                    $languageArray['Hindi']     = "Hindi";
                                    $languageArray['Spanish']   = "Spanish";
                                    $languageArray['Swedish']   = "Swedish";
                                    $languageArray['Urdu']     = "Urdhu";
                                    echo form_dropdown('language', $languageArray,set_value('language',settings()->language),'id="language" class="form-control"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group <?=form_error('address') ? 'has-error' : ''?>">
                                <label for="address"><?=$this->lang->line('settings_address')?></label> <span class="text-red">*</span>
                                <input name="address" value="<?=set_value('address',(!empty(settings()->address)?settings()->address:''))?>" id="" class="form-control" placeholder="<?=$this->lang->line('settings_address')?>">
                                <?=form_error('address','<div class="text-red">', '</div>')?>
                            </div>
                        </div>

                    </div>
                </fieldset>
                <fieldset class="border-fieldset">
                    <legend class="border-legend"><?=$this->lang->line('settings_prefixes')?></legend>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('category_prefixe') ? 'has-error' : ''?>">
                                    <label for="category_prefixe"><?=$this->lang->line('category_prefixe')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="category_prefixe" name="category_prefixe" value="<?=set_value('category_prefixe',(!empty(settings()->category_prefixe)?settings()->category_prefixe:''))?>" placeholder="<?=$this->lang->line('category_prefixe')?>">
                                    <?=form_error('category_prefixe','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('member_prefixe') ? 'has-error' : ''?>">
                                    <label for="member_prefixe"><?=$this->lang->line('member_prefixe')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="member_prefixe" name="member_prefixe" value="<?=set_value('member_prefixe',(!empty(settings()->member_prefixe)?settings()->member_prefixe:''))?>" placeholder="<?=$this->lang->line('member_prefixe')?>">
                                    <?=form_error('member_prefixe','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                             <div class="form-group <?=form_error('book_prefixe') ? 'has-error' : ''?>">
                                <label for="book_prefixe"><?=$this->lang->line('book_prefixe')?></label> <span class="text-red">*</span>
                                <input type="text" class="form-control" id="book_prefixe" name="book_prefixe" value="<?=set_value('book_prefixe',(!empty(settings()->book_prefixe)?settings()->book_prefixe:''))?>" placeholder="<?=$this->lang->line('book_prefixe')?>">
                                <?=form_error('book_prefixe','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group <?=form_error('membership_prefixe') ? 'has-error' : ''?>">
                                <label for="membership_prefixe"><?=$this->lang->line('membership_prefixe')?></label> <span class="text-red">*</span>
                                <input type="text" class="form-control" id="membership_prefixe" name="membership_prefixe" value="<?=set_value('membership_prefixe',(!empty(settings()->membership_prefixe)?settings()->membership_prefixe:''))?>" placeholder="<?=$this->lang->line('membership_prefixe')?>">
                                <?=form_error('membership_prefixe','<div class="text-red">', '</div>')?>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="box-footer">
                <?if(permissionChecker('settings_edit')){?>
                <button type="submit" class="btn btn-primary"><?=$this->lang->line('settings_update')?></button>
                <?}?>
            </div>
        </form>
    </div>
</div>
</div>
</section>
</div>

<script type="text/javascript">
$(function () {
    $('#example1').DataTable({
      'pageLength':15,
  });
})
$(document).ready(function(){
    $('.select2').select2();
});
</script>