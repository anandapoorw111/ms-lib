<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('usertype')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><?=$this->lang->line('usertype')?></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('usertype_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('usertype_add',$this->lang->line('usertype_add_usertype'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('usertype')?></th>
                <th><?=$this->lang->line('usertype_action')?></th>
              </tr>
            </thead>
            <tbody >
             <?php if(count($usertypes)) { $i=0; foreach($usertypes as $usertype) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('usertype')?>"><?=$usertype->usertype?></td>
                <td data-title="<?=$this->lang->line('usertype_action')?>">
                  <?=edit_btn('usertype_edit', $usertype->usertypeID);?>&nbsp;
                  <?=delete_btn('usertype_delete', $usertype->usertypeID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>




<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-user-plus"></i></span>&nbsp;<?=$this->lang->line('usertype_add')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('usertype_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="usertype" id="usertype" placeholder="<?=$this->lang->line('usertype_name')?>"/>
          <span class="text-red" id="error_usertype_name"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('usertype_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="usertypeID" id="usertypeID" readonly/>
          <div class="form-group error-name-up">
            <label><?=$this->lang->line('usertype_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="usertype_up" id="usertype_up" placeholder="<?=$this->lang->line('usertype_name')?>"/>
            <span class="text-red" id="error_usertype_up"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('usertype_update')?></button>
      </div>
    </div>
  </div>
</div>

