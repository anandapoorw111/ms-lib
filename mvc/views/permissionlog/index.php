<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			Permissionlog
		</h1>
		<ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url('/permissionlog/index')?>"><i class="fa fa-dashboard"></i> Permissionlog</a></li>
		</ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
              <a href="<?=base_url('permissionlog/add')?>" class="btn btn-inline btn-primary btn-md"><i class="fa fa-plus"></i> Add Permissionlog</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="hide-table">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($permissionlogsArray)) { $i=0; foreach($permissionlogsArray as $permissionmoduleID => $permissionlogs) { ?>
                      <tr>
                        <td class="sorting_disabled text-bold"><?=isset($permissionmodules[$permissionmoduleID]) ? $permissionmodules[$permissionmoduleID] : 'Main Module'?></td>
                        <td class="sorting_disabled">&nbsp;</td>
                        <td class="sorting_disabled">&nbsp;</td>
                        <td class="sorting_disabled">&nbsp;</td>
                        <td class="sorting_disabled">&nbsp;</td>
                      </tr>
                      <?php if(count($permissionlogs)) { foreach($permissionlogs as $permissionlog) { $i++; ?>
                        <tr>
                          <td data-title="#"><?=$i?></td>
                          <td data-title="Name"><?=$permissionlog->name?></td>
                          <td data-title="Description"><?=$permissionlog->description?></td>
                          <td data-title="Status"><?=$permissionlog->active?></td>
                          <td data-title="Action">
                            <?=btn_edit_show('permissionlog/edit/'.$permissionlog->permissionlogID,'Edit')?>
                            <?=btn_delete_show('permissionlog/delete/'.$permissionlog->permissionlogID,'Delete')?>
                          </td>
                        </tr>
                      <?php } } ?>
                    <?php } } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(function () {
    $('#example1').DataTable({
      'pageLength':100,
      'ordering': false
    });
  })
</script>