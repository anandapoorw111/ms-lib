<style>
.tab-content{
    min-height: 310px !important;
}
.profile-view-tab {
    font-size: 16px;
}
.profile-view-tab span {
    font-weight: 600;
}

.profile-view-tab p span {
    display: inline-block;
    width: 35%;
}
.nav-tabs-custom > .nav-tabs > li.active {
    border-top-color: #d1d6d9 !important;
}
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$this->lang->line('member_profile')?></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
            <li><a href="<?=base_url('/member')?>"><?=$this->lang->line('member_member')?></a></li>
            <li class="active"><?=$this->lang->line('member_profile')?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body box-profile">
                        <img src="<?=member_img($member->member_photo)?>" class="profile_user_img img-responsive img-circle " alt="">
                        <h3 class="profile-membername text-center"><?=ucwords($member->member_name)?></h3>
                        <p class="text-muted text-center"><?=ucfirst(isset($membership[$member->member_membership_type]) ? $membership[$member->member_membership_type] : '')?></p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b><?=$this->lang->line('member_gender')?></b> <a class="pull-right"><?=ucfirst($member->member_gender)?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?=$this->lang->line('member_date_of_birth')?></b> <a class="pull-right"><?=app_date($member->member_date_of_birth)?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?=$this->lang->line('member_phone')?></b> <a class="pull-right"><?=$member->member_phone;?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="profile">
                            <div class="panel-body profile-view-dis">
                                <div class="row">
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_father_name')?> </span>: <?=ucwords($member->member_father_name)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_mother_name')?> </span>: <?=ucwords($member->member_mother_name)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_code')?> </span>: <?=$member->member_code?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_occupation')?> </span>: <?=ucwords($member->member_occupation)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_blood_group')?> </span>: <?=ucwords($member->member_blood_group)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_nid_no')?> </span>: <?=ucwords($member->member_nid_no)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_religion')?> </span>: <?=ucwords($member->member_religion)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_since')?> </span>: <?=app_date($member->member_since_date)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_address')?> </span>: <?=ucwords($member->member_address)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_email')?> </span>: <?=$member->member_email?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_username')?> </span>: <?=$member->username?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span><?=$this->lang->line('member_status')?></span>: <?=($member->member_status ==1) ? "<span class='text-green'>Active</span>" : "<span class='text-red'>Block</span>"?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="box box-solid">
            <div class="box-header header-custom">
                <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('member_details_list')?></h5>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#circulation_details" data-toggle="tab"><?=$this->lang->line('member_circulation')?></a></li>
                        <li><a href="#payment_details" data-toggle="tab"><?=$this->lang->line('member_payment_details')?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="circulation_details">
                            <div class="panel-body profile-view-dis">
                                <div class="row">
                                    <div id="hide-table">
                                        <table class="table display table-bordered table-striped">
                                            <thead>
                                              <tr>
                                                <th>#</th>
                                                <th><?=$this->lang->line('circulation_book_code')?></th>
                                                <th><?=$this->lang->line('circulation_book_name')?></th>
                                                <th><?=$this->lang->line('circulation_writer_name')?></th>
                                                <th><?=$this->lang->line('circulation_issue_date')?></th>
                                                <th><?=$this->lang->line('circulation_expiry_date')?></th>
                                                <th><?=$this->lang->line('circulation_return_date')?></th>
                                                <th><?=$this->lang->line('circulation_penalty')?></th>
                                                <th><?=$this->lang->line('circulation_return_status')?></th>
                                               
                                              </tr>
                                            </thead>
                                            <tbody >
                                              <?php if(count($circulations)) { $i=0; foreach($circulations as $val) { $i++; ?>
                                              <tr>
                                                <td data-title="#"><?=$i?></td>
                                                <td data-title="<?=$this->lang->line('circulation_book_code')?>"><?=(isset($bookcode[$val->bookID])?$bookcode[$val->bookID]:'&nbsp;');?></td>
                                                <td data-title="<?=$this->lang->line('circulation_book_name')?>"><?=(isset($bookname[$val->bookID])?ucwords($bookname[$val->bookID]): '&nbsp;');?></td>
                                                <td data-title="<?=$this->lang->line('circulation_writer_name')?>"><?=(isset($writerID[$val->bookID])?ucwords($writer[$writerID[$val->bookID]]): '&nbsp;');?></td>
                                                <td data-title="<?=$this->lang->line('circulation_issue_date')?>"><?=(!empty($val->issue_date)?app_date($val->issue_date):'&nbsp;')?></td>
                                                <td data-title="<?=$this->lang->line('circulation_expiry_date')?>"><?=app_date($val->expiry_date)?></td>
                                                <td data-title="<?=$this->lang->line('circulation_return_date')?>"><?=app_date($val->return_date)?></td>
                                                <td data-title="<?=$this->lang->line('circulation_penalty')?>" class='text-danger'><?=(!empty($val->penalty_amount)?'<strike>'.$val->penalty_amount.'</strike>':'&nbsp;')?></td>
                                                <td data-title="<?=$this->lang->line('circulation_return_status')?>"><?=($val->return_status == 1)?"<small class='label label-success'>YES</small>" : "<small class='label label-danger'>NO</small>"?></td>

                                              </tr>
                                              <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="payment_details">
                            <div class="panel-body profile-view-dis">
                                <div class="row">
                                    <div id="hide-table">
                                        <table class="table display table-bordered table-striped">
                                            <thead>
                                              <tr>
                                                <th>#</th>
                                                <th><?=$this->lang->line('payment_date')?></th>
                                                <th><?=$this->lang->line('payment_for')?></th>
                                                <th><?=$this->lang->line('payment_amount')?></th>
                                              </tr>
                                            </thead>
                                            <tbody >
                                              <?php if(count($payment)) { $i=0; foreach($payment as $val) { $i++; ?>
                                              <tr>
                                                <td data-title="#"><?=$i?></td>
                                                <td data-title="<?=$this->lang->line('payment_date')?>"><?=app_date($val->create_date)?></td>
                                                <td data-title="<?=$this->lang->line('payment_for')?>"><?=ucwords(str_replace('_', ' ', $val->payment_by));?></td>
                                                <td data-title="<?=$this->lang->line('payment_amount')?>"><?= settings()->currency_symbol.$val->payment_amount?></td>
                                              </tr>
                                              <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $('table.display').DataTable();
} );
</script>