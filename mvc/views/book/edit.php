<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$this->lang->line('book_book')?></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
            <li><a href="<?=base_url('/book')?>"> <?=$this->lang->line('book_book')?></a></li>
            <li class="active"><?=$this->lang->line('book_update')?></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-custom-border">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                        <div class="box-body">
                         <fieldset class="border-fieldset">
                            <legend class="border-legend"><?=$this->lang->line('book_info')?></legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('name') ? 'has-error' : ''?>">
                                        <label for="name"><?=$this->lang->line('book_name')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $book->book_name)?>" placeholder="<?=$this->lang->line('book_name')?>">
                                        <?=form_error('name','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('writer') ? 'has-error' : ''?>">
                                        <label for="writer"><?=$this->lang->line('book_writer')?></label> <span class="text-red">*</span>
                                        <?php 
                                        $writerArray[0] = $this->lang->line('book_please_select');
                                        if(count($writer)){
                                            foreach ($writer as $val){
                                                $writerArray[$val->writerID] = $val->writer_name;
                                            }
                                        }
                                        echo form_dropdown('writer', $writerArray,set_value('writer',$book->book_writerID),'id="writer" class="form-control select2"');
                                        ?>
                                        <?=form_error('writer','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                                
                                <div class="col-md-4">

                                    <div class="form-group <?=form_error('publication') ? 'has-error' : ''?>">
                                        <label for="publication"><?=$this->lang->line('book_publication')?></label> <span class="text-red">*</span>
                                        <?php 
                                        $unitArray[0] = $this->lang->line('book_please_select');
                                        if(count($publication)){
                                            foreach ($publication as $val){
                                                $unitArray[$val->publicationID] = $val->publication_name;
                                            }
                                        }
                                        echo form_dropdown('publication', $unitArray,set_value('publication',$book->book_publication),'id="publication" class="form-control select2"');
                                        ?>
                                        <?=form_error('publication','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                        <div class="form-group <?=form_error('categories') ? 'has-error' : ''?>">
                                            <label for="categories"><?=$this->lang->line('book_categories')?></label> <span class="text-red">*</span>
                                            <?php 
                                            $categoryArray[0] = $this->lang->line('book_please_select');
                                            if(count($categories)){
                                              foreach ($categories as $category){
                                                $categoryArray[$category->categoriesID] = $category->categories_name;
                                            }
                                        }
                                        echo form_dropdown('categories', $categoryArray,set_value('categories', $book->book_category),'id="categories" class="form-control select2"');
                                        ?>
                                        <?=form_error('categories','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('book_code') ? 'has-error' : ''?>">
                                        <label for="book_code"><?=$this->lang->line('book_code')?>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('book_code_tooltip')?>"></i>
                                        </label> <span class="text-red">*</span>
                                        <div class="input-group">
                                          <span class="input-group-addon"><?=substr($book->book_code, 0, 2);?></span>
                                          <input type="text" class="form-control only_number" id="book_code" name="book_code" value="<?=set_value('book_code', substr($book->book_code, 2))?>" placeholder="<?=$this->lang->line('book_code')?>">
                                        </div>
                                        <?=form_error('book_code','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('book_isbn') ? 'has-error' : ''?>">
                                        <label for="book_isbn"><?=$this->lang->line('book_isbn')?>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('isbn_tooltip')?>"></i></label> <span class="text-red">*</span></label>
                                        <input type="text" class="form-control" id="book_isbn" name="book_isbn" value="<?=set_value('book_isbn', $book->book_isbn_no)?>" placeholder="<?=$this->lang->line('book_isbn')?>">
                                        <?=form_error('book_isbn','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('photo') ? 'has-error' : ''?>">
                                        <label for="photo"><?=$this->lang->line('book_photo')?></label> 
                                        <input type="file" class="form-control" id="photo" name="photo"/>
                                        <?=form_error('photo','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>

                            <div class="col-md-4">
                                <div class="form-group <?=form_error('price') ? 'has-error' : ''?>">
                                    <label for="price"><?=$this->lang->line('book_price')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control is_numeric" id="price" name="price" value="<?=set_value('price',$book->book_price)?>" placeholder="<?=$this->lang->line('book_price')?>">
                                    <?=form_error('price','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="book_edition"><?=$this->lang->line('book_edition')?></label>
                                    <input type="text" class="form-control" id="book_edition" name="book_edition" value="<?=set_value('book_edition', $book->book_edition)?>" placeholder="<?=$this->lang->line('book_edition')?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('book_edition_year') ? 'has-error' : ''?>">
                                    <label for="book_edition_year"><?=$this->lang->line('book_edition_year')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control is_numeric datepicker" id="book_edition_year" name="book_edition_year" value="<?=set_value('book_edition_year',$book->book_edition_year)?>" placeholder="<?=$this->lang->line('book_edition_year')?>">
                                    <?=form_error('book_edition_year','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('quantity') ? 'has-error' : ''?>">
                                    <label for="quantity"><?=$this->lang->line('book_quantity')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control is_numeric" id="quantity" name="quantity" value="<?=set_value('quantity',$book->book_quantity)?>" placeholder="<?=$this->lang->line('book_quantity')?>">
                                    <?=form_error('quantity','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('issued_quantity') ? 'has-error' : ''?>">
                                    <label for="issued_quantity"><?=$this->lang->line('book_issued_quantity')?>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('book_issued_quantity_tooltip')?>"></i></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control is_numeric" id="issued_quantity" name="issued_quantity" value="<?=set_value('issued_quantity',$book->book_issued_quantity)?>" placeholder="<?=$this->lang->line('book_issued_quantity')?>">
                                    <?=form_error('issued_quantity','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group <?=form_error('rack_no') ? 'has-error' : ''?>">
                                    <label for="rack_no"><?=$this->lang->line('book_rack_no')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="rack_no" name="rack_no" value="<?=set_value('rack_no',$book->book_rack_no)?>" placeholder="<?=$this->lang->line('book_rack_no')?>">
                                    <?=form_error('rack_no','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-past"><?=$this->lang->line('book_update')?></button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            autoclose: true,
            format : 'mm-yyyy',
            viewMode: "months", 
            minViewMode: "months",
            startView: 2
        });
    });
</script>