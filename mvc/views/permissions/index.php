<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			Permission
		</h1>
		<ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url('/permission/index')?>"> Permission</a></li>
		</ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <?php if(count($usertypes)) { $i=0; foreach($usertypes as $usertype) { $i++; ?>
                  <li <?=($urlusertypeID == $usertype->usertypeID) ? 'class="active"' : (($i==1 && $urlusertypeID == 0) ? 'class="active"' : '')?>><a href="#usertype<?=$usertype->usertypeID?>" data-toggle="tab" aria-expanded="false"><?=$usertype->usertype?></a></li>
                <?php } } ?>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->
                <?php if(count($usertypes)) { $i=0; foreach($usertypes as $usertype) { $i++; ?>
                  <div class="tab-pane <?=($urlusertypeID==$usertype->usertypeID) ? 'active' : ($i==1  && $urlusertypeID == 0) ? 'active' : ''?>" id="usertype<?=$usertype->usertypeID?>">
                    <form method="post" action="<?=base_url('/permissions/save')?>">
                      <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                      <input type="hidden" name="permissionsusertypeID" value="<?=$usertype->usertypeID?>">
                      <div id="hide-table">
                        <table id="example111" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Module Name</th>
                              <th>Add</th>
                              <th>Edit</th>
                              <th>Delete</th>
                              <th>View</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php if(count($permissionsModuleArray)) { $j = 0; foreach($permissionsModuleArray as $permissionmoduleID => $permissionsModule) { ?>
                              <tr>
                                <td class="text-bold"><?=isset($permissionmodules[$permissionmoduleID]) ? $permissionmodules[$permissionmoduleID] : 'Main Module'?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                            <?php if(count($permissionsModule)) { foreach($permissionsModule as $permission) { $j++; ?>
                            <tr>
                              <td data-title="#"> 
                                <input type="checkbox" id="<?=$permission->name?>_<?=$usertype->usertypeID?>" name="<?=$permission->name?>" value="<?=$permission->permissionlogID?>"  <?=isset($permissions[$usertype->usertypeID][$permission->permissionlogID]) ? 'checked' : ''?> onclick="processCheck(this);" class="mainmodule"/> 
                              </td>
                              <td data-title="Module Name"><?=ucfirst($permission->name)?></td>
                              <td data-title="Add">
                                <?php 
                                  $permissionadd = $permission->name.'_add';
                                  if(isset($permissionlogsArray[$permissionadd])) { ?>
                                    <input type="checkbox" id="<?=$permissionadd?>_<?=$usertype->usertypeID?>" name="<?=$permissionadd?>" value="<?=$permissionlogsArray[$permissionadd]?>" <?=isset($permissions[$usertype->usertypeID][$permissionlogsArray[$permissionadd]]) ? 'checked' : ''?> />
                                  <?php } ?>
                              </td>
                              <td data-title="Edit">
                                <?php 
                                  $permissionedit = $permission->name.'_edit';
                                  if(isset($permissionlogsArray[$permissionedit])) { ?>
                                    <input type="checkbox" id="<?=$permissionedit?>_<?=$usertype->usertypeID?>" name="<?=$permissionedit?>" value="<?=$permissionlogsArray[$permissionedit]?>" <?=isset($permissions[$usertype->usertypeID][$permissionlogsArray[$permissionedit]]) ? 'checked' : ''?> />
                                <?php } ?>
                              </td>
                              <td data-title="Delete">
                                <?php 
                                  $permissiondelete = $permission->name.'_delete';
                                  if(isset($permissionlogsArray[$permissiondelete])) { ?>
                                    <input type="checkbox" id="<?=$permissiondelete?>_<?=$usertype->usertypeID?>" name="<?=$permissiondelete?>" value="<?=$permissionlogsArray[$permissiondelete]?>" <?=isset($permissions[$usertype->usertypeID][$permissionlogsArray[$permissiondelete]]) ? 'checked' : ''?> />
                                <?php } ?>
                              </td>
                              <td data-title="View">
                                <?php 
                                  $permissionview = $permission->name.'_view';
                                  if(isset($permissionlogsArray[$permissionview])) { ?>
                                    <input type="checkbox" id="<?=$permissionview?>_<?=$usertype->usertypeID?>" name="<?=$permissionview?>" value="<?=$permissionlogsArray[$permissionview]?>" <?=isset($permissions[$usertype->usertypeID][$permissionlogsArray[$permissionview]]) ? 'checked' : ''?> />
                                <?php } ?>
                              </td>
                            </tr>
                            <?php } } } } ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Module Name</th>
                              <th>Add</th>
                              <th>Edit</th>
                              <th>Delete</th>
                              <th>View</th>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      <button class="btn btn-large btn-primary">Save Permission For <span class="text-bold bg-black" style="padding: 2px 5px; border-radius: 5px;"><?=$usertype->usertype?></span></button>
                      </form>
                  </div>
                <?php } } ?>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
          </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->