<style type="text/css">textarea{resize: none}</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('publication_publication')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/publication')?>"><?=$this->lang->line('publication_publication')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('publication_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('publication_add', $this->lang->line('publication_add_publication'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('publication_name')?></th>
                <th><?=$this->lang->line('publication_note')?></th>
                <th><?=$this->lang->line('publication_status')?></th>
                <th><?=$this->lang->line('publication_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($publication)) { $i=0; foreach($publication as $category) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('publication_name')?>"><?=$category->publication_name?></td>
                <td data-title="<?=$this->lang->line('publication_note')?>"><?=(!empty($category->publication_note)?$category->publication_note:'&nbsp')?></td>
                <td data-title="<?=$this->lang->line('publication_status')?>">
                  <div class="onoffswitch-small" id="<?=$category->publicationID?>">
                    <input type="checkbox" id="myonoffswitch<?=$category->publicationID?>" class="onoffswitch-small-checkbox" name="publication_status" <?php if($category->publication_status === '1') echo "checked='checked'"; ?> <?=permissionChecker('publication_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$category->publicationID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('publication_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('publication_action')?>">
                  <?=edit_btn('publication_edit',$category->publicationID);?>&nbsp;
                  <?=delete_btn('publication_delete',$category->publicationID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('publication_publication')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('publication_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="publication_name" id="publication_name" placeholder="<?=$this->lang->line('publication_name')?>"/>
          <span class="text-red" id="error_publication_name"></span>
        </div>
        <div class="form-group">
          <label><?=$this->lang->line('publication_note')?></label>
          <textarea type="text" name='publication_note' id="publication_note" class="form-control"/ placeholder="<?=$this->lang->line('publication_note')?>"></textarea>
          <span class="text-red" id="error_publication_note"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('publication_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="publicationID" id="publicationID" readonly/>
          <div class="form-group error-class">
            <label><?=$this->lang->line('publication_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="publication_name" id="publication_name_up" placeholder="<?=$this->lang->line('publication_name')?>"/>
            <span class="text-red" id="error_publication_name_up"></span>
          </div>
          <div class="form-group">
            <label><?=$this->lang->line('publication_note')?></label>
            <textarea type="text" name='publication_note' id="publication_note_up" class="form-control" placeholder="<?=$this->lang->line('publication_note')?>"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('publication_update')?></button>
      </div>
    </div>
  </div>
</div>