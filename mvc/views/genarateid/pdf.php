<!DOCTYPE html>
<html>
	<head>
		<title>ID Card Download</title>
	</head>
	<body>
	    <div class="box-body" id='print_areas'>
	        <div class="row">
	            <?php foreach($member_details as $val){ ?>
	            <div class="idcard-box-size">
	                <div class="box box-border" style="background-color:#f4f4f4">
	                  	<div class="box-header with-border" style="padding-top: 5px;">
	                  		<div style="width:120px; float:left; border:1px solid#999999!important;">
	                  			<h3 class="box-title"><?=settings()->company_name?></h3>
	                  		</div>
							<div style="width:170px; float:left; border:1px solid#999999!important;">
								<img class="img-responsive" style="height:44px;float: right" src="<?=base_url('uploads/images/'.settings()->logo)?>">
							</div>
	                  	</div>
	                  	<div class="box-body">
	                    	<div class="idcard-img-resize" style="width:110px;float:left">
	                      		<img src="<?=member_img($val->member_photo)?>" class="img-thumbnail" alt="<?=$val->member_name?>">
	                      		<p align="center"><b><?=$val->member_code;?></b></p>
                    		</div>
	                    	<div class="idcard-font-resize" style="width:170px;float:left; padding-left:10px">
	                      		<p><?=$this->lang->line('genarateid_name')?></p>
	                      		<h4><b><?= ucwords($val->member_name)?></b></h4>

			                    <p><?=$this->lang->line('genarateid_membership')?></p>
			                    <h4><b><?= ucwords($membership[$val->member_membership_type])?></b></h4>

			                    <div style="width:70%;float:left;">
			                    <p><?=$this->lang->line('genarateid_gender')?></p>
			                    <h4><b><?= ucwords($val->member_gender)?></b></h4>

	                      		<p><?=$this->lang->line('genarateid_joined')?></p>
	                      		<h4><b><?= app_date($val->member_since_date)?></b></h4>
	                      		</div>
	                      		<div style="width:30%;">
	                      			<barcode style="line-height:10px" code="<?php echo $val->member_code;?>" size="0.6" type="QR" error="M" class="barcode" disableborder="1"/>
	                      		</div>
	                    	</div>
	                  	</div>
	                </div>
	            </div>
	            <?php } ?> 
	        </div>
	    </div>
	</body>
</html>