<style type="text/css">
textarea{resize:none;}
.width-css select option{width: 130px;}
.border-fieldset{padding: 7px !important;}
#autoSuggestionsList a> li {
    background: #F3F3F3;
    border-bottom: 1px solid #E3E3E3;
    list-style: none;
    padding: 3px 15px;
    text-align: left;
    color: #000;
}
#autoSuggestionsList a> li:hover {
   background-color: rgba(66, 139, 202);
   color: #fff;
   cursor: pointer;
   /*border: 1px dotted #ccc;*/
}
#suggestions{
    position: absolute;
    z-index: 9999;
    width: 100%;
}
.auto_list {
    position: absolute;
    margin-top: 34px;
    padding-right: 44px;
    width: 100%;
}
.box-profile{background-color: #ecf0f5 !important;}
.list-group-unbordered>.list-group-item {padding-left: 10px; padding-right: 10px;}
.nav-tabs-custom>.nav-tabs>li.active {border-top-color: #d2d6de;}
.nav-tabs-custom>.nav-tabs>li:first-of-type.active>a{background: #ecf0f5;}
.nav-tabs-custom>.tab-content{background: #ecf0f5 !important;}
.custom-input-field>input{padding: 25px 10px;font-size: 40px;}
.custom-input-field>.input-group-addon{font-size: 35px;}
.btn-search{background-color: #109aa8; color: #ddd;border-color: #109aa8;font-size:25px;line-height: 38px;}
.btn-search:hover{color: #ccc;}
.book_img{width: 85px; height: 85px; border-radius: 5px; border: 1px solid #ddd;}
.nav-tabs-custom>.tab-content{min-height: 309px;}
table>tbody>tr>td{vertical-align: middle !important;}
.btn-info {background-color: #109aa8; border-color: #109aa8;}
.btn-info:hover,.btn-info:active,.btn-info:visited{background-color:#109aa8e6 !important;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('circulation_issue_and_return')?></h1>
    <ol class="breadcrumb">
        <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
        <li><a href="<?=base_url('/circulation')?>"><?=$this->lang->line('circulation_circulation')?></a></li>
        <li class="active"><?=$this->lang->line('circulation_issue_and_return')?></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('circulation_details')?></h5>
        <div class="box-tools pull-right">
          <?=add_circulation_btn('circulation_add', $this->lang->line('circulation_issue_and_return'));?>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body box-profile">
                        <img src="<?=member_img($member->member_photo)?>" class="profile_user_img img-responsive img-circle " alt="">
                        <h3 class="profile-membername text-center"><?=ucfirst($member->member_name)?></h3>
                        <p class="text-muted text-center"><?= ucfirst(isset($membership[$member->member_membership_type]) ? $membership[$member->member_membership_type] : '')?></p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b><?=$this->lang->line('circulation_member_gender')?></b> <a class="pull-right"><?=ucfirst($member->member_gender)?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?=$this->lang->line('circulation_member_occupation')?></b> <a class="pull-right"><?=ucfirst($member->member_occupation)?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?=$this->lang->line('circulation_member_phone')?></b> <a class="pull-right"><?=$member->member_phone;?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#profile" data-toggle="tab"><?=$this->lang->line('circulation_issue')?></a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="active tab-pane">
                        <div class="panel-body">
                          <form method="post" enctype="multipart/form-data">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                            <label class="row" for="book_code"><?=$this->lang->line('circulation_book_code')?>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('circulation_book_code_tooltip')?>"></i></label>                             
                            <div class="row input-group custom-input-field <?=form_error('book_code') ? 'has-error' : ''?>">
                              <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                              <input type="text" class="form-control" placeholder="<?=$this->lang->line('circulation_search')?>" id="book_code" name="book_code" autocomplete="off" autofocus/>
                              <div class="input-group-btn btn-large">
                                <button class="btn btn-search" type="submit">
                                  <span class="fa fa-search"></span>
                                </button>
                              </div>
                            </div>
                            <?=form_error('book_code','<div class="text-red">', '</div>')?>
                          </form>
                          <br>
                          <div id="hide-table" class="row">
                            <table class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th><?=$this->lang->line('book_photo')?></th>
                                  <th><?=$this->lang->line('book_code')?></th>
                                  <th><?=$this->lang->line('book_name')?></th>
                                  <th><?=$this->lang->line('book_writer')?></th>
                                  <th><?=$this->lang->line('book_edition')?></th>
                                  <th><?=$this->lang->line('book_price')?></th>
                                  <th><?=$this->lang->line('book_availability')?></th>
                                  <th><?=$this->lang->line('book_rack_no')?></th>
                                  <th><?=$this->lang->line('book_action')?></th>
                                 
                                </tr>
                              </thead>
                              <tbody>
                                <tr>  
                                 <form method="post" action="<?=base_url('circulation/add')?>">
                                  <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />                            
                                  <input type="hidden" name="memberID" id="memberID" value="<?=$member->memberID?>"/>                                                    
                                  <input type="hidden" name="membership_type" id="membership_type" value="<?=$member->member_membership_type?>"/>
                                  <input type="hidden" name="bookID" id="bookID" value="<?=(!empty($book->bookID)? $book->bookID:'')?>"/>                                                      

                                  <td data-title="<?=$this->lang->line('book_photo')?>"><?=(!empty($book->book_photo)? '<img src="'.book_img($book->book_photo).'" class="book_img" alt="'.$book->book_name.'">' : '&nbsp')?>  </td>
                                  <td data-title="<?=$this->lang->line('book_code')?>"><?=(!empty($book->book_code)? $book->book_code : '&nbsp')?></td>
                                  <td data-title="<?=$this->lang->line('book_name')?>" class="text-capitalize"><?=(!empty($book->book_name)? ucwords($book->book_name) : '&nbsp')?></td>
                                  <td data-title="<?=$this->lang->line('book_writer')?>"><?=(!empty($book->book_writerID)? ucwords($writer[$book->book_writerID]) : '&nbsp')?></td>
                                  <td data-title="<?=$this->lang->line('book_edition')?>" class="text-capitalize"><?=(!empty($book->book_edition)? $book->book_edition : '&nbsp')?></td>
                                  <td data-title="<?=$this->lang->line('book_price')?>"><?=(!empty($book->book_price)? $book->book_price : '&nbsp')?></td>
                                  <td data-title="<?=$this->lang->line('book_availability')?>"><?=(!empty($book->book_availability)?$book->book_availability:'&nbsp;')?></td>
                                  <td data-title="<?=$this->lang->line('book_rack_no')?>"><?=(!empty($book->book_rack_no)?$book->book_rack_no:'&nbsp;')?></td>
                                  <td data-title="<?=$this->lang->line('book_action')?>">
                                    <?=(!empty($book->bookID)?'<input type="submit" class="btn btn-lg btn-info" value="'.$this->lang->line("circulation_issued").'">':'&nbsp;')?> 
                                  </td>
                                  </form>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            </div>

          </div>


          <div id="hide-table">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?=$this->lang->line('book_photo')?></th>
                  <th><?=$this->lang->line('circulation_book_code')?></th>
                  <th><?=$this->lang->line('circulation_book_name')?></th>
                  <th><?=$this->lang->line('circulation_writer_name')?></th>
                  <th><?=$this->lang->line('circulation_issue_date')?></th>
                  <th><?=$this->lang->line('circulation_expiry_date')?></th>
                  <th><?=$this->lang->line('circulation_action')?></th>
                </tr>
              </thead>
              <tbody>
                <?php if(count($circulations)) { $i=0; foreach($circulations as $val) { $i++; ?>
                <tr>
                  <td data-title="#"><?=$i?></td>
                  <td data-title="<?=$this->lang->line('book_photo')?>"><img src="<?=book_img(isset($bookphoto[$val->bookID])?ucwords($bookphoto[$val->bookID]): '&nbsp;');?>" class="profile_img"></td>
                  <td data-title="<?=$this->lang->line('circulation_book_code')?>"><?=(isset($bookcode[$val->bookID])?ucwords($bookcode[$val->bookID]): '&nbsp;');?></td>
                  <td data-title="<?=$this->lang->line('circulation_book_name')?>"><?=(isset($bookname[$val->bookID])?ucwords($bookname[$val->bookID]): '&nbsp;');?></td>
                  <td data-title="<?=$this->lang->line('circulation_writer_name')?>"><?=(isset($writerID[$val->bookID])?ucwords($writer[$writerID[$val->bookID]]): '&nbsp;');?></td>
                  <td data-title="<?=$this->lang->line('circulation_issue_date')?>"><?=(!empty($val->issue_date)?app_date($val->issue_date):'&nbsp;')?></td>
                  <td data-title="<?=$this->lang->line('circulation_expiry_date')?>"><?=app_date($val->expiry_date)?></td>
                  <td data-title="<?=$this->lang->line('circulation_action')?>">
                    <?php if(permissionChecker('circulation_renewal')){ ?>
                      <button type="submit" class="btn btn-inline btn-info btn-xs renewal" data-id="<?=$val->circulationID?>" data-date="<?=$val->expiry_date?>" data-membershipID="<?=$member->member_membership_type?>"><?=$this->lang->line('circulation_book_renewal')?></button>
                    <?php } ?>
                    <?php if(permissionChecker('circulation_return')){ ?>
                      <button type="submit" class="btn btn-inline btn-primary btn-xs return" data-id="<?=$val->circulationID?>" data-date="<?=$val->expiry_date?>" data-membershipID="<?=$member->member_membership_type?>"><?=$this->lang->line('circulation_book_return')?></button>
                    <?php } ?>

                    <?php if(permissionChecker('circulation_return')){ ?>
                      <button type="submit" class="btn btn-inline btn-warning btn-xs lost" id="<?=$val->circulationID?>"><?=$this->lang->line('circulation_book_lost')?></button>
                    <?php } ?>
                  </td>
                </tr>
                <?php } } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="search" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-search"></i></span>&nbsp;<?=$this->lang->line('circulation_member_search')?></h4>
      </div>
      <div class="modal-body">

        <div class="form-group error-name">
          <label><?=$this->lang->line('circulation_member_id')?> <span class='text-danger'>*</span></label>
          <div class="input-group">
            <span class="input-group-addon"><i class="icofont-user"></i></span>
            <input type="text" class="form-control ajaxSearch" name="member_id" id="member_id" autofocus="autofocus" autocomplete="off" placeholder="<?=$this->lang->line('circulation_member_id')?>"/>
            <div id="suggestions">
               <div id="autoSuggestionsList"></div>
            </div>
            <span class="text-red" id="error_member_id"></span>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <!-- <button type="submit" class="btn btn-default search">< ?=$this->lang->line('circulation_search')?></button> -->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="renewal_fee" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-horn"></i></span>&nbsp;<?=$this->lang->line('circulation_penalty')?></h4>
      </div>
      <div class="modal-body">
        <p class="text-red"><?=ucwords($this->lang->line('circulation_penalty_message'))?></p>
          <input type="hidden" class="form-control" name="penalty_by" id="penalty_by" readonly/>
          <input type="hidden" class="form-control" name="member_id_up" id="member_id_up" readonly/>
          <input type="hidden" class="form-control" name="circulation_id_up" id="circulation_id_up" readonly/>
          <input type="hidden" class="form-control" name="circulation_code_up" id="circulation_code_up" readonly/>
          <input type="hidden" class="form-control" name="book_id_up" id="book_id_up" readonly/>
          <input type="hidden" class="form-control" name="member_book_limit_days" id="member_book_limit_days" readonly/>

        <div class="form-group">
          <label><?=$this->lang->line('circulation_penalty_fee')?></label>
          <div class="input-group">
            <span class="input-group-addon"><?=settings()->currency_symbol?></span>
            <input type="text" class="form-control" name="member_penalty_fee" id="member_penalty_fee" readonly/>

          </div>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('circulation_no_of_days')?></label>
          <div class="input-group">
            <span class="input-group-addon"><i class="icofont-calendar"></i></span>
            <input type="text" class="form-control" name="no_of_days" id="no_of_days" readonly/>
          </div>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('member_penalty_amount')?></label>
          <div class="input-group">
            <span class="input-group-addon"><?=settings()->currency_symbol?></span>
            <input type="text" class="form-control" name="member_penalty_amount" id="member_penalty_amount" placeholder="<?=$this->lang->line('circulation_penalty_amount')?>" readonly/>
          </div>
            <span class="text-red" id="error_member_id"></span>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('circulation_penalty_note')?></label>
          <textarea type="text" class="form-control" name="member_penalty_note" id="member_penalty_note"/></textarea>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info penalty"><?=$this->lang->line('circulation_penalty')?></button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="lost_book" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-horn"></i></span>&nbsp;<?=$this->lang->line('circulation_book_lost')?></h4>
      </div>
      <div class="modal-body">
        <p class="text-red"><?=ucwords($this->lang->line('circulation_book_lost_message'))?></p>
          <input type="hidden" class="form-control" name="lost_circulationID" id="lost_circulationID" readonly/>
          <input type="hidden" class="form-control" name="lost_memberID" id="lost_memberID" readonly/>
          <input type="hidden" class="form-control" name="lost_bookID" id="lost_bookID" readonly/>
        <div class="form-group">
          <label><?=$this->lang->line('circulation_book_price')?></label>
          <div class="input-group">
            <span class="input-group-addon"><?=settings()->currency_symbol?></span>
            <input type="text" class="form-control" name="book_price" id="book_price" readonly/>

          </div>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('circulation_book_payable_amount')?></label>
          <div class="input-group">
            <span class="input-group-addon"><?=settings()->currency_symbol?></span>
            <input type="text" class="form-control" name="payable_amount" id="payable_amount" readonly/>
          </div>
            <span class="text-red" id="error_member_id"></span>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('circulation_penalty_note')?></label>
          <textarea type="text" class="form-control" name="book_lost_note" id="book_lost_note"/></textarea>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info lost_penalty"><?=$this->lang->line('circulation_penalty')?></button>
      </div>
    </div>
  </div>
</div>