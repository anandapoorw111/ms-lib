<style type="text/css">
textarea{resize:none;}
.width-css select option{width: 130px;}
.border-fieldset{padding: 7px !important;}
#autoSuggestionsList a> li {
    background: #F3F3F3;
    border-bottom: 1px solid #E3E3E3;
    list-style: none;
    padding: 3px 15px;
    text-align: left;
    color: #000;
}
#autoSuggestionsList a> li:hover {
   background-color: rgba(66, 139, 202);
   color: #fff;
   cursor: pointer;
   /*border: 1px dotted #ccc;*/
}
#suggestions{
    position: absolute;
    z-index: 9999;
    width: 100%;
}
.auto_list{
    position: absolute;
    margin-top: 34px;
    padding-right: 44px;
    width: 100%;
}
.label-success{background-color:#109aa8 !important;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('circulation_circulation')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/circulation')?>"><?=$this->lang->line('circulation_circulation')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('circulation_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_circulation_btn('circulation_add', $this->lang->line('circulation_issue_and_return'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('circulation_member_code')?></th>
                <th><?=$this->lang->line('circulation_member_name')?></th>
                <th><?=$this->lang->line('circulation_book_name')?></th>
                <th><?=$this->lang->line('circulation_writer_name')?></th>
                <th><?=$this->lang->line('circulation_issue_date')?></th>
                <th><?=$this->lang->line('circulation_expiry_date')?></th>
                <th><?=$this->lang->line('circulation_return_date')?></th>
                <th><?=$this->lang->line('circulation_penalty')?></th>
                <th><?=$this->lang->line('circulation_return_status')?></th>
               
              </tr>
            </thead>
            <tbody >
              <?php if(count($circulations)) { $i=0; foreach($circulations as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('circulation_member_code')?>"><?=(isset($membercode[$val->memberID])?$membercode[$val->memberID]:'&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('circulation_member_name')?>"><?=(isset($membername[$val->memberID])?ucwords($membername[$val->memberID]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('circulation_book_name')?>"><?=(isset($bookname[$val->bookID])?ucwords($bookname[$val->bookID]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('circulation_writer_name')?>"><?=(isset($writerID[$val->bookID])?ucwords($writer[$writerID[$val->bookID]]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('circulation_issue_date')?>"><?=(!empty($val->issue_date)?app_date($val->issue_date):'&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('circulation_expiry_date')?>"><?=app_date($val->expiry_date)?></td>
                <td data-title="<?=$this->lang->line('circulation_return_date')?>"><?=app_date($val->return_date)?></td>
                <td data-title="<?=$this->lang->line('circulation_penalty')?>" class='text-danger'><?=(!empty($val->penalty_amount)?'<strike>'.$val->penalty_amount.'</strike>':'&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('circulation_return_status')?>"><?=($val->return_status == 1)?"<small class='label label-success'>YES</small>" : "<small class='label label-danger'>NO</small>"?></td>

              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="search" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-search"></i></span>&nbsp;<?=$this->lang->line('circulation_member_search')?></h4>
      </div>
      <div class="modal-body">

        <div class="form-group error-name">
          <label><?=$this->lang->line('circulation_member_id')?> <span class='text-danger'>*</span></label>
          <div class="input-group">
            <span class="input-group-addon"><i class="icofont-user"></i></span>
            <input type="text" class="form-control ajaxSearch" name="member_id" id="member_id" autofocus="autofocus" autocomplete="off" placeholder="<?=$this->lang->line('circulation_member_id')?>"/>
            <div id="suggestions">
               <div id="autoSuggestionsList"></div>
            </div>
            <span class="text-red" id="error_member_id"></span>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <!-- <button type="submit" class="btn btn-default search">< ?=$this->lang->line('circulation_search')?></button> -->
      </div>
    </div>
  </div>
</div>