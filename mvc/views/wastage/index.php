<style type="text/css">
table.dataTable tbody td {vertical-align:middle;}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('wastage_wastage')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><?=$this->lang->line('wastage_wastage')?></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('wastage_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('wastage_add', $this->lang->line('wastage_add_wastage'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('wastage_photo')?></th>
                <th><?=$this->lang->line('wastage_book_code')?></th>
                <th><?=$this->lang->line('wastage_book_name')?></th>
                <th><?=$this->lang->line('wastage_writer_name')?></th>
                <th><?=$this->lang->line('wastage_wastager_by')?></th>
                <th><?=$this->lang->line('wastage_payable_amount')?></th>
                <th><?=$this->lang->line('wastage_note')?></th>
                <th><?=$this->lang->line('wastage_date')?></th>
                <th><?=$this->lang->line('wastage_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($wastage)) { $i=0; foreach($wastage as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('wastage_photo')?>"><img src="<?=book_img($bookphoto[$val->wastage_bookID])?>" class="profile_img" alt="<?=(isset($bookname[$val->wastage_bookID])?ucwords($bookname[$val->wastage_bookID]): '&nbsp;');?>"></td>
                <td data-title="<?=$this->lang->line('wastage_book_code')?>"><?=(isset($bookcode[$val->wastage_bookID])?ucwords($bookcode[$val->wastage_bookID]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('wastage_book_name')?>"><?=(isset($bookname[$val->wastage_bookID])?ucwords($bookname[$val->wastage_bookID]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('wastage_writer_name')?>"><?=(isset($writerID[$val->wastage_bookID])?ucwords($writername[$writerID[$val->wastage_bookID]]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('wastage_wastage_by')?>"><?=(isset($membername[$val->wastage_memberID])?ucwords($membername[$val->wastage_memberID]): 'Library');?></td>
                <td data-title="<?=$this->lang->line('wastage_payable_amount')?>"><?= (!empty($val->wastage_price)?settings()->currency_symbol.$val->wastage_price : '-')  ?></td>
                <td data-title="<?=$this->lang->line('wastage_note')?>"><?=(!empty($val->wastage_note)?$val->wastage_note : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('wastage_date')?>"><?=app_date($val->create_date)?></td>
                <td data-title="<?=$this->lang->line('wastage_action')?>">
                 <?php if(permissionChecker('wastage_restore')){if(!inicompute($val->wastage_memberID)){?>
                    <button id="<?=$val->wastageID;?>" title='Restore' class='btn btn-warning btn-xs restore'><i class='icofont-loop'></i></button>
                  <?php }}?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('wastage_wastage')?></h4>
      </div>
      <div class="modal-body">

        <div class="form-group error_name">
          <label for="book_name"><?=$this->lang->line('wastage_book_name')?></label><span class="text-red">*</span>
            <?php 
                $bookArray[0] = $this->lang->line('wastage_book_select');
                if(count($book)){
                    foreach ($book as $val){
                      $bookArray[$val->bookID] = $val->book_name;
                    }
                }
            echo form_dropdown('book_name', $bookArray,set_value('book_name'),'id="book_name" class="form-control select2"');
          ?>
          <span class="text-red" id="error_book_name"></span>
        </div>
        <div class="form-group">
          <label><?=$this->lang->line('wastage_note')?></label>
          <textarea type="text" name='wastage_note' id="wastage_note" class="form-control"/ placeholder="<?=$this->lang->line('wastage_note')?>"></textarea>
          <span class="text-red" id="error_wastage_note"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;< ?=$this->lang->line('wastage_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="wastageID" id="wastageID" readonly/>
          <div class="form-group error-class">
            <label>< ?=$this->lang->line('wastage_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="wastage_name" id="wastage_name_up" placeholder="< ?=$this->lang->line('wastage_name')?>"/>
            <span class="text-red" id="error_wastage_name_up"></span>
          </div>
          <div class="form-group">
            <label>< ?=$this->lang->line('wastage_note')?></label>
            <textarea type="text" name='wastage_note' id="wastage_note_up" class="form-control" placeholder="< ?=$this->lang->line('wastage_note')?>"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated">< ?=$this->lang->line('wastage_update')?></button>
      </div>
    </div>
  </div>
</div> -->

<script type="text/javascript">

</script>