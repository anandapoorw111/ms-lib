<style type="text/css">
/*image inpute script*/
.files input {
  outline: 2px dashed #92b0b3;
  outline-offset: -10px;
  -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
  transition: outline-offset .15s ease-in-out, background-color .15s linear;
  padding: 120px 0px 85px 35%;
  text-align: center !important;
  margin: 0;
  width: 100% !important;
}
.files input:focus{     outline: 2px dashed #92b0b3;  outline-offset: -10px;
  -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
  transition: outline-offset .15s ease-in-out, background-color .15s linear; border:1px solid #92b0b3;
}
.files{ position:relative}
.files:after {  pointer-events: none;
  position: absolute;
  top: 60px;
  left: 0;
  width: 50px;
  right: 0;
  height: 56px;
  content: "";
  background-image: url(https://image.flaticon.com/icons/png/128/109/109612.png);
  display: block;
  margin: 0 auto;
  background-size: 100%;
  background-repeat: no-repeat;
}
.color input{ background-color:#f1f1f1;}
.files:before {
  position: absolute;
  bottom: 10px;
  left: 0;  pointer-events: none;
  width: 100%;
  right: 0;
  height: 57px;
  content: " or drag it here. ";
  display: block;
  margin: 0 auto;
  color: #2ea591;
  font-weight: 600;
  text-transform: capitalize;
  text-align: center;
}
.box-body p{margin: 20px 5px 30px;font-size: 20px;}
.footer-custom-css{padding:15px 15px 0px 0px;}
.box-body-custom-css{padding: 0px}

</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('import_import')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/import')?>"><?=$this->lang->line('import_import')?></a></li>
    </ol>
  </section>



<section class="content">
    <div class="row">
    <section class="col-lg-7 connectedSortable">
      <div class="box box-solid">
        <div class="box-header header-custom">
          <h5 class="box-title"><i class="icofont-notepad"></i> <?=$this->lang->line('import_note')?></h5>
        </div>
        <div class="box-body">
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_1')?> <a href=<?=download_file();?> download><?=$this->lang->line('download')?></a></p>
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_2')?></a></p>
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_3')?></a></p>
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_4')?> <a href=<?=base_url('import/exportbooklist');?> download><?=$this->lang->line('download')?></a></p>
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_5')?> <a href=<?=base_url('import/exportwriterlist');?> download><?=$this->lang->line('download')?></a></p>
          <p><i class="icofont-tick-boxed"></i>&nbsp; <?=$this->lang->line('import_note_6')?> <a href=<?=base_url('import/exportpublicationlist');?> download><?=$this->lang->line('download')?></a></p>
        </div>
      </div>
    </section>
    <section class="col-lg-5 connectedSortable">
      <div class="box box-solid">
        <div class="box-header header-custom">
          <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('import_list')?></h5>
        </div>
        <div class="box-body box-body-custom-css">
            <form method="post" id="import_csv" enctype="multipart/form-data">
              <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
              <div class="modal-body">
                <div class="form-group files">
                  <label><?=$this->lang->line('select_file')?></label>
                  <input type="file" name="csv_file" id="csv_file" required accept=".csv" />                  
                </div>
                <?=form_error('csv_file','<div class="text-red">', '</div>')?>
                <br />
              </div>
              <div class="modal-footer footer-custom-css">               
                <button type="submit" name="import_csv" class="btn btn-primary" id="import_csv_btn"><i class="fa fa-upload"></i>&nbsp;<?=$this->lang->line('upload_btn')?></button>
              </div>
            </form>
            <br />
<!--             <div id="imported_csv_data"></div> -->
          </div>
        </div>
      </section>
    </div>
</section>
</div>

