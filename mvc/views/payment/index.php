<style type="text/css">
.receipt-header h6{line-height:7px !important;}
.receipt-header h3{line-height:1px !important;}
.border-dotted{border-bottom: 2px dotted}
.border-top{border-top: 2px solid #000000;padding-left: 25px;padding-right: 25px;}
.zero-padding{padding-left:0px; padding-right: 0px;}
.modal-body {padding: 0px 15px 0px 15px !important;}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('payment_payment')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><?=$this->lang->line('payment_payment')?></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('payment_list')?></h5>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>

                <th><?=$this->lang->line('payment_member_name')?></th>
                <th><?=$this->lang->line('payment_for')?></th>
                <th><?=$this->lang->line('payment_amount')?></th>
                <th><?=$this->lang->line('payment_date')?></th>
                <th><?=$this->lang->line('payment_status')?></th>
                <th><?=$this->lang->line('payment_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($payment)) { $i=0; foreach($payment as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('payment_member_name')?>"><?=(isset($membername[$val->payment_memberID])?ucwords($membername[$val->payment_memberID]): '&nbsp;');?></td>
                <td data-title="<?=$this->lang->line('payment_for')?>"><?=ucwords(str_replace('_', ' ', $val->payment_by));?></td>
                <td data-title="<?=$this->lang->line('payment_amount')?>"><?= settings()->currency_symbol.$val->payment_amount?></td>
                <td data-title="<?=$this->lang->line('payment_date')?>"><?=app_date($val->create_date)?></td>
                <td data-title="<?=$this->lang->line('payment_status')?>"></td>
                <td data-title="<?=$this->lang->line('payment_action')?>">
                  <?=view_btn('payment_view', $val->paymentID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>





<div class="modal fade" id="details" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-data"></i></span>&nbsp;<?=$this->lang->line('payment_receipt')?></h4>
      </div>
      <div class="modal-body" id='print_areas' style="background-image: url('<?=base_url('uploads/images/'.settings()->logo)?>'); background-position: top;
background-repeat: no-repeat;">
        <div id="showData"></div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-info prints" id="get_print"><span class="glyphicon glyphicon-print"></span>&nbsp;Print</button>
        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(function() {
    $('#example1').DataTable({
      'pageLength':10,
    });
  })
</script>