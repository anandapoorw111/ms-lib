<style type="text/css">textarea{resize: none}</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('bookrequest_bookrequest')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/bookrequest')?>"><?=$this->lang->line('bookrequest_bookrequest')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('bookrequest_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('bookrequest_add', $this->lang->line('bookrequest_add_bookrequest'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('bookrequest_name')?></th>
                <th><?=$this->lang->line('bookrequest_writer_name')?></th>
                <th><?=$this->lang->line('bookrequest_categories')?></th>
                <th><?=$this->lang->line('bookrequest_edition')?></th>
                <th><?=$this->lang->line('bookrequest_note')?></th>
                <th><?=$this->lang->line('bookrequest_member')?></th>
                <th><?=$this->lang->line('bookrequest_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($bookrequest)){ $i=0; foreach($bookrequest as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('bookrequest_name')?>"><?=ucwords($val->bookrequest_name)?></td>
                <td data-title="<?=$this->lang->line('bookrequest_writer_name')?>"><?=ucwords($val->bookrequest_writer_name)?></td>
                <td data-title="<?=$this->lang->line('bookrequest_categories')?>"><?=ucwords($val->bookrequest_categories)?></td>
                <td data-title="<?=$this->lang->line('bookrequest_edition')?>"><?=ucfirst($val->bookrequest_edition)?></td>
                <td data-title="<?=$this->lang->line('bookrequest_note')?>"><?=(!empty($val->bookrequest_note)?$val->bookrequest_note : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('bookrequest_member')?>"><?=ucwords(isset($memberID[$val->bookrequest_memberID])?$memberID[$val->bookrequest_memberID] : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('bookrequest_action')?>">
                  <?=edit_btn('bookrequest_edit',$val->bookrequestID);?>&nbsp;
                  <?=delete_btn('bookrequest_delete',$val->bookrequestID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('bookrequest_bookrequest')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('bookrequest_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="book_name" id="book_name" placeholder="<?=$this->lang->line('book_name')?>"/>
          <span class="text-red" id="error_bookrequest_name"></span>
        </div>

        <div class="form-group error-writer">
          <label><?=$this->lang->line('bookrequest_writer_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="writer_name" id="writer_name" placeholder="<?=$this->lang->line('writer_name')?>"/>
          <span class="text-red" id="error_bookrequest_writer"></span>
        </div>

        <div class="form-group error-categories">
          <label><?=$this->lang->line('bookrequest_categories')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="categories" id="categories" placeholder="<?=$this->lang->line('categories')?>"/>
          <span class="text-red" id="error_bookrequest_categories"></span>
        </div>

        <div class="form-group error-edition">
          <label><?=$this->lang->line('bookrequest_edition')?></label>
          <input type="text" class="form-control only_number" name="edition" id="edition" placeholder="<?=$this->lang->line('edition')?>"/>
          <span class="text-red" id="error_bookrequest_edition"></span>
        </div>

        <div class="form-group">
          <label><?=$this->lang->line('bookrequest_note')?></label>
          <textarea type="text" name='bookrequest_note' id="bookrequest_note" class="form-control"/ placeholder="<?=$this->lang->line('bookrequest_note')?>"></textarea>
          <span class="text-red" id="error_bookrequest_note"></span>
        </div>

        <?php if(empty($this->session->userdata('departmentID'))){?>
          <div class="form-group error-member">
              <label for="userdepartment"><?=$this->lang->line('bookrequest_member')?> <span class='text-danger'>*</span></label>
              <?php
                  $array[0] = "Please Select";
                  if(count($memberlist)) {
                        foreach ($memberlist as $val) {
                          $array[$val->memberID] = $val->member_code.'-'.ucwords($val->member_name);
                      }
                  }
                  echo form_dropdown('memberID', $array,set_value('memberID',$this->session->userdata('departmentID')),'id="memberID" class="form-control select2"'.(!empty($this->session->userdata('departmentID'))?"disabled":"").'');     
              ?>
              <span class="text-red" id="error_bookrequest_member"></span>
          </div>
        <?php } ?>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('bookrequest_update')?></h4>
      </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" name="bookrequestID" id="bookrequestID" readonly/>
          <div class="form-group error-name-up">
            <label><?=$this->lang->line('bookrequest_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="book_name" id="book_name_up" placeholder="<?=$this->lang->line('book_name')?>"/>
            <span class="text-red" id="error_bookrequest_name_up"></span>
          </div>

          <div class="form-group error-writer-up">
            <label><?=$this->lang->line('bookrequest_writer_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="writer_name" id="writer_name_up" placeholder="<?=$this->lang->line('writer_name')?>"/>
            <span class="text-red" id="error_bookrequest_writer_up"></span>
          </div>

          <div class="form-group error-categories-up">
            <label><?=$this->lang->line('bookrequest_categories')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="categories" id="categories_up" placeholder="<?=$this->lang->line('categories')?>"/>
            <span class="text-red" id="error_bookrequest_categories_up"></span>
          </div>

          <div class="form-group error-edition-up">
            <label><?=$this->lang->line('bookrequest_edition')?></label>
            <input type="text" class="form-control only_number" name="edition" id="edition_up" placeholder="<?=$this->lang->line('edition')?>"/>
            <span class="text-red" id="error_bookrequest_edition_up"></span>
          </div>

          <div class="form-group">
            <label><?=$this->lang->line('bookrequest_note')?></label>
            <textarea type="text" name='bookrequest_note' id="bookrequest_note_up" class="form-control"/ placeholder="<?=$this->lang->line('bookrequest_note')?>"></textarea>
            <span class="text-red" id="error_bookrequest_note_up"></span>
          </div>

          <?php if(empty($this->session->userdata('memberID'))){?>
            <div class="form-group error-member-up">
                <label for="userdepartment"><?=$this->lang->line('bookrequest_member')?> <span class='text-danger'>*</span></label>
                <?php
                    $array[0] = "Please Select";
                    if(count($memberlist)) {
                          foreach ($memberlist as $val) {
                            $array[$val->memberID] = $val->member_code.'-'.ucwords($val->member_name);
                        }
                    }
                    echo form_dropdown('memberID', $array,set_value('memberID',$this->session->userdata('memberID')),'id="memberID_up" class="form-control select2-up"'.(!empty($this->session->userdata('memberID'))?"disabled":"").'');     
                ?>
                <span class="text-red" id="error_bookrequest_member_up"></span>
            </div>
          <?php } ?>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('bookrequest_update')?></button>
      </div>
    </div>
  </div>
</div>