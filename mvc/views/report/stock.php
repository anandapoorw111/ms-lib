<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_stock')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/reports')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('reports_stock')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
        <div class="box-tools pull-right">
          <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
        </div>
      </div>
      <div class="box-body" id='print_areas'>
        <div>
          <div class="row">
            <div class="col-sm-7 col-xs-12">
              <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
              <div>
                <h4 class="text-capitalize"><?=settings()->company_name?></h4>
                <span><?=settings()->address?></span><br>
                <span>Phone: <?=settings()->phone?></span><br>
                <span>Email: <?=settings()->email?></span>
              </div>
            </div>
            <div class="col-sm-5 col-xs-12 text-center">
              <h3><?=$this->lang->line('reports_stock')?></h3>
              <?=date('d-M-Y')?>
            </div>
          </div>
        </div><br>
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('reports_items_photo')?></th>
                <th><?=$this->lang->line('reports_items_code')?></th>
                <th><?=$this->lang->line('reports_items_name')?></th>
                <th><?=$this->lang->line('reports_items_categories')?></th>
                <th><?=$this->lang->line('reports_items_units')?></th>
                <th><?=$this->lang->line('reports_items_tax')?></th>
                <th><?=$this->lang->line('reports_items_selling_price')?>(<?=settings()->currency_symbol;?>)</th>
                <th><?=$this->lang->line('reports_items_instock')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($result)){ $i=0; foreach($result as $val) { $i++; ?>
              <td data-title="#"><?=$i?></td>
              <td data-title="<?=$this->lang->line('reports_items_photo')?>"><img src="<?=item_img($val->items_photo)?>" class="profile_img" alt="<?=$val->items_name?>"></td>
              <td data-title="<?=$this->lang->line('reports_items_code')?>"><?=$val->items_code?></td>
              <td data-title="<?=$this->lang->line('reports_items_name')?>" class="text-capitalize"><?=$val->items_name?></td>
              <td data-title="<?=$this->lang->line('reports_items_categories')?>" class="text-capitalize"><?=isset($categories[$val->items_category]) ? $categories[$val->items_category] : ''?></td>
              <td data-title="<?=$this->lang->line('reports_items_units')?>" class="text-capitalize"><?=isset($units[$val->items_unit]) ? $units[$val->items_unit] : ''?></td>
              <td data-title="<?=$this->lang->line('reports_items_tax')?>" class="text-capitalize"><?=isset($taxs[$val->items_tax]) ? $taxs[$val->items_tax].'['.$val->items_tax_type.']': ''?></td>
              <td data-title="<?=$this->lang->line('reports_items_selling_price')?>"><?=$val->items_selling_price;?></td>
              <td data-title="<?=$this->lang->line('reports_items_instock')?>"><?=$val->items_quantity;?></td>
              </td>
            </tr>
            <?php } }?>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</section>
</div>



