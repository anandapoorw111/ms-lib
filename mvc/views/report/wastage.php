<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_wastage')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/reports')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('reports_wastage')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$this->lang->line('report_generated')?></h3>
      </div>
      <form method="post">
        <div class="box-body">
          <div class="row">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="col-md-4">
              <div>
                <label for="reports_items_name"><?=$this->lang->line('reports_items_name')?></label>
                <?php 
                $itemArray[''] = $this->lang->line('reports_please_select');
                if(count($items)){
                  foreach ($items as $item){
                    $itemArray[$item->itemsID] = $item->items_name;
                  }
                }
                echo form_dropdown('items', $itemArray,set_value('items'),'id="items" class="form-control select2"');
                ?>            
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="tag_line"><?=$this->lang->line('reports_from_date')?></label>
                <input type="text" class="form-control datepicker" id="from_date" name="from_date" value="<?=date('d-m-Y')?>" readonly>
              </div>
            </div>
            <div class="col-md-4">
             <div class="form-group">
              <label for="type"><?=$this->lang->line('reports_to_date')?></label>
              <input type="text" class="form-control datepicker" id="to_date" name="to_date" value="<?=date('d-m-Y')?>" readonly>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-success btn-flat"><?=$this->lang->line('report_generate')?></button>
      </div>
    </form>
  </div>


  <div class="box box-solid">
    <div class="box-header header-custom">
      <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
      <div class="box-tools pull-right">
        <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
      </div>
    </div>
    <div class="box-body" id='print_areas'>
      <div>
        <div class="row">
          <div class="col-sm-7 col-xs-12">
            <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
            <div>
              <h4 class="text-capitalize"><?=settings()->company_name?></h4>
              <span><?=settings()->address?></span><br>
              <span>Phone: <?=settings()->phone?></span><br>
              <span>Email: <?=settings()->email?></span>
            </div>
          </div>
          <div class="col-sm-5 col-xs-12 text-center">
            <h3><?=$this->lang->line('reports_wastage')?></h3>
            <?=(count($date)? '<p>'.date('d-M-Y',strtotime($date["from_date"])).'<b>&nbsp;To&nbsp;</b>'.date('d-M-Y',strtotime($date["to_date"])).'</p>' : '')?>
          </div>
        </div>
      </div><br>
      <div id="hide-table">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th><?=$this->lang->line('reports_items_photo')?></th>
              <th><?=$this->lang->line('reports_date')?></th>     
              <th><?=$this->lang->line('reports_items_code')?></th>
              <th><?=$this->lang->line('reports_items_name')?></th>
              <th><?=$this->lang->line('reports_items_quantity')?></th>
              <th><?=$this->lang->line('reports_note')?></th>
            </tr>
          </thead>
          <tbody >
            <?php if(count($result)){ $i=0; foreach($result as $val) { $i++; ?>
            <tr>
              <td data-title="#"><?=$i?></td>
              <td data-title="<?=$this->lang->line('reports_items_photo')?>"><img src="<?=item_img(isset($items_img[$val->wastage_itemsID]) ? $items_img[$val->wastage_itemsID] : '')?>" class="profile_img" alt="<?=isset($items_name[$val->wastage_itemsID]) ? $items_name[$val->wastage_itemsID] : ''?>"></td>
              <td data-title="<?=$this->lang->line('reports_date')?>"><?=date('d-m-Y',strtotime($val->wastage_date))?></td>
              <td data-title="<?=$this->lang->line('reports_items_code')?>"><?=isset($items_code[$val->wastage_itemsID]) ? $items_code[$val->wastage_itemsID] : ''?></td>
              <td data-title="<?=$this->lang->line('reports_items_name')?>"><?=isset($items_name[$val->wastage_itemsID]) ? $items_name[$val->wastage_itemsID] : ''?></td>
              <td data-title="<?=$this->lang->line('reports_items_quantity')?>"><?=$val->wastage_quantity?></td>
              <td data-title="<?=$this->lang->line('reports_note')?>"><?=namesorting($val->wastage_note,25)?></td>
            </tr>
            <?php } }?>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</section>


</div>

<script type="text/javascript">
$('.datepicker').datepicker({
  autoclose: true,
  format : 'dd-mm-yyyy',
});
</script>