<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_purchase_payment')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/reports')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('reports_purchase_payment')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$this->lang->line('report_generated')?></h3>
      </div>
      <form method="post">
        <div class="box-body">
          <div class="row">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="col-md-4">
              <div>
               <label for="reports_suppliers"><?=$this->lang->line('reports_suppliers')?></label>
               <?php 
               $supplierArray[''] = $this->lang->line('reports_please_select');
               if(count($suppliers)){
                foreach ($suppliers as $supplier){
                  $supplierArray[$supplier->suppliers_code] = $supplier->suppliers_name;
                }
              }
              echo form_dropdown('suppliers', $supplierArray,set_value('suppliers'),'id="suppliers" class="form-control select2 text-capitalize"');
              ?>              
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="tag_line"><?=$this->lang->line('reports_from_date')?></label>
              <input type="text" class="form-control datepicker" id="from_date" name="from_date" value="<?=date('d-m-Y')?>" readonly>
            </div>
          </div>
          <div class="col-md-4">
           <div class="form-group">
            <label for="type"><?=$this->lang->line('reports_to_date')?></label>
            <input type="text" class="form-control datepicker" id="to_date" name="to_date" value="<?=date('d-m-Y')?>" readonly>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-success btn-flat"><?=$this->lang->line('report_generate')?></button>
    </div>
  </form>
</div>


<div class="box box-solid">
  <div class="box-header header-custom">
    <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
    <div class="box-tools pull-right">
      <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
    </div>
  </div>
  <div class="box-body" id='print_areas'>
    <div>
      <div class="row">
        <div class="col-sm-7 col-xs-12">
          <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
          <div>
            <h4 class="text-capitalize"><?=settings()->company_name?></h4>
            <span><?=settings()->address?></span><br>
            <span>Phone: <?=settings()->phone?></span><br>
            <span>Email: <?=settings()->email?></span>
          </div>
        </div>
        <div class="col-sm-5 col-xs-12 text-center">
          <h3><?=$this->lang->line('reports_purchase_payment')?></h3>
          <?=(count($date)? '<p>'.date('d-M-Y',strtotime($date["from_date"])).'<b>&nbsp;To&nbsp;</b>'.date('d-M-Y',strtotime($date["to_date"])).'</p>' : '')?>
        </div>
      </div>
    </div><br>
    <div id="hide-table">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th><?=$this->lang->line('reports_sales_invoice')?></th>
            <th><?=$this->lang->line('reports_date')?></th>
            <th><?=$this->lang->line('reports_supplier_name')?></th>
            <th><?=$this->lang->line('reports_payment_type')?></th>
            <th><?=$this->lang->line('reports_note')?></th>
            <th><?=$this->lang->line('reports_paid_amount')?>(<?=settings()->currency_symbol;?>)</th>
          </tr>
        </thead>
        <tbody >
          <?php if(count($result)){ $i=0; foreach($result as $val) { $i++; ?>
          <tr>
            <td data-title="#"><?=$i?></td>
            <td data-title="<?=$this->lang->line('reports_sales_invoice')?>"><?=$val->payment_code?></td>
            <td data-title="<?=$this->lang->line('reports_date')?>"><?=date('d-m-Y',strtotime($val->payment_date))?></td>
            <td data-title="<?=$this->lang->line('reports_supplier_name')?>" class="text-capitalize"><?= isset($supplierInfo[$val->payment_by])?$supplierInfo[$val->payment_by] : $this->lang->line('reports_walk_in_customer');?></td>
            <td data-title="<?=$this->lang->line('reports_payment_type')?>" class="text-capitalize"><?=$val->payment_type?></td>
            <td data-title="<?=$this->lang->line('reports_note')?>"><?=$val->payment_note;?></td>
            <td data-title="<?=$this->lang->line('reports_paid_amount')?>" class="text-capitalize"><?=(!empty($val->payment_amount)?$val->payment_amount : '0.00')?></td>
          </tr>
          <?php } }?>
          <?php if(count($result)) {?>
          <tr>
            <td colspan="6" data-title="<?=$this->lang->line('reports_total')?>" class='text-right'><b><?=$this->lang->line('reports_total')?></b></td>
            <td data-title="<?=$this->lang->line('reports_total_amount')?>"><?php $total_sum = 0; foreach ($result as $value){  $total_sum += $value->payment_amount;} echo $total_sum;?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
</section>


</div>

<script type="text/javascript">
$('.datepicker').datepicker({
  autoclose: true,
  format : 'dd-mm-yyyy',
});
</script>