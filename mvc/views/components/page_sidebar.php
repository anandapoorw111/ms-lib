
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=profile_img($this->session->userdata('photo'))?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$this->session->userdata('name')?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <ul class="sidebar-menu" data-widget="tree">
            <!-- <li class="header"></li> -->
            <?php if(count($sidebarmenus)) { foreach($sidebarmenus as $sidebarmenu) { 
                $showmenu = true;
                if(!isset($sidebarmenu['child']) && $sidebarmenu['menulink'] == '#') {
                    $showmenu = false;
                }

                if($showmenu){
                    ?>
                    <li class="<?=(isset($sidebarmenu['child']) && count($sidebarmenu['child'])) ? menu_treeview_show($sidebarmenu['child'],$activemenu,'treeview active menu-open','treeview') : ''?> <?=($activemenu==$sidebarmenu['menulink']) ? 'active' : ''?>">
                        <a href="<?=base_url($sidebarmenu['menulink'])?>">
                            <i class="<?=$sidebarmenu['menuicon']?>"></i> <span><?=$this->lang->line('menu_'.strtolower($sidebarmenu['menuname']))?></span>
                            <?php if(isset($sidebarmenu['child'])) { ?>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                          </span>
                          <?php } ?>
                      </a>
                      <?php if(isset($sidebarmenu['child']) && count($sidebarmenu['child'])) { ?>
                      <ul class="treeview-menu" style="<?=menu_treeview_show($sidebarmenu['child'],$activemenu,'display: block','display: none')?>">
                        <?php foreach($sidebarmenu['child'] as $subsidebarmenu) { ?>
                        <li class="<?=($activemenu==$subsidebarmenu['menulink']) ? 'active' : ''?>"><a href="<?=base_url($subsidebarmenu['menulink'])?>"><i class="fa fa-circle-o"></i><?=$this->lang->line('menu_'.strtolower($subsidebarmenu['menuname']))?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <?php } } } ?>
            </ul>
        </section>
    </aside>