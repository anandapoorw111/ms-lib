        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 3.3
            </div>
            <strong>Powered by <a target="_blank" href="http://morningsunit.com">Morning Sun IT</a>.</strong>
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>

    <script src="<?=base_url('assets/bower_components/jquery-ui/jquery-ui.min.js')?>"></script>
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- Bootstrap 3.3.7 -->
    <script src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
    <script src="<?=base_url('assets/bower_components/fastclick/lib/fastclick.js')?>"></script>
    <script src="<?=base_url('assets/plugins/toastr/toastr.min.js')?>"></script>
    <script src="<?=base_url('assets/dist/js/adminlte.min.js')?>"></script>
    <script type='text/javascript' src="<?= base_url('assets/sweetalert/sweetalert2.min.js');?>"></script>
    <?php if(isset($footerassets['js']) && count($footerassets['js'])) { ?>
    <?php foreach ($footerassets['js'] as $js) { ?>
    <script src="<?=base_url($js)?>"></script>
    <?php } ?>
    <?php } ?>

    <?php $message = $this->session->userdata('message');
    if($message) { ?>
    <script>
        swal({
            title: "<?=$message?>",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1700,
        });
    </script>
    <?php } ?>
</body>
</html>
