<br>
<div class="row setup-content" >
  	<div class="col-sm-8 col-center">
    <!-- <div class="row"> -->
	    <div class="panel panel-info">
	        <div class="panel-heading"><h4>Business Information</h4></div>
	        <div class="panel-body ">
	        	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
					<fieldset class="border-fieldset">
                    <div class="box-body row">
                        <div class="col-md-12">
                        	<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group <?=form_error('company_name') ? 'has-error' : ''?>">
                                    <label for="company_name"><?=$this->lang->line('settings_company_name')?></label> <span class="text-red">*</span>
                                    <input type="text" class="custom-input-field" id="company_name" name="company_name" value="<?=set_value('company_name')?>" placeholder="<?=$this->lang->line('settings_company_name')?>">
                                    <?=form_error('company_name', '<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="tag_line"><?=$this->lang->line('settings_tag_line')?></label>
                                    <input type="text" class="custom-input-field" id="tag_line" name="tag_line" value="<?=set_value('tag_line')?>" placeholder="<?=$this->lang->line('settings_tag_line')?>">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
	                             <div class="form-group">
	                                <label for="type"><?=$this->lang->line('settings_type')?></label>
	                                <input type="text" class="custom-input-field" id="type" name="type" value="<?=set_value('type')?>" placeholder="<?=$this->lang->line('settings_type')?>">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                    	<div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('owner') ? 'has-error' : ''?>">
	                                <label for="owner"><?=$this->lang->line('settings_owner')?></label> <span class="text-red">*</span>
	                                <!-- < ?=(settings()->owner ? set_value('owner', settings()->owner))?> -->
	                                <input type="text" class="custom-input-field" id="owner" name="owner" value="<?=set_value('owner')?>" placeholder="<?=$this->lang->line('settings_owner')?>">
	                                <?=form_error('owner','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('mobile_no') ? 'has-error' : ''?>">
	                                <label for="mobile_no"><?=$this->lang->line('settings_mobile')?></label> <span class="text-red">*</span>
	                                <input type="text" class="custom-input-field" id="mobile_no" name="mobile_no" value="<?=set_value('mobile_no')?>" placeholder="<?=$this->lang->line('settings_mobile')?>">
	                                <?=form_error('mobile_no','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('logo') ? 'has-error' : ''?>">
	                                <label for="logo"><?=$this->lang->line('settings_logo')?></label> 
	                                <input type="file" class="custom-input-field" id="logo" name="logo"/>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">

	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('email') ? 'has-error' : ''?>">
	                                <label for="email"><?=$this->lang->line('settings_email')?></label> <span class="text-red">*</span>
	                                <input type="text" class="custom-input-field" id="email" name="email" value="<?=set_value('email')?>" placeholder="<?=$this->lang->line('settings_email')?>">
	                                <?=form_error('email','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>


	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group">
	                                <label for="tax_no"><?=$this->lang->line('settings_tax')?></label>
	                                <input type="text" class="custom-input-field" id="tax_no" name="tax_no" value="<?=set_value('tax_no')?>" placeholder="<?=$this->lang->line('settings_tax')?>">
	                            </div>
	                        </div>

	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group">
	                                <label for="fax_no"><?=$this->lang->line('settings_fax')?></label>
	                                <input type="text" class="custom-input-field" id="fax_no" name="fax_no" value="<?=set_value('fax_no')?>" placeholder="<?=$this->lang->line('settings_fax')?>">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                    	<div class="col-md-12">
	                            <div class="form-group <?=form_error('address') ? 'has-error' : ''?>">
	                                <label for="address"><?=$this->lang->line('settings_address')?></label> <span class="text-red">*</span>
	                                <input name="address" value="<?=set_value('address')?>" id="" class="custom-input-field" placeholder="<?=$this->lang->line('settings_address')?>">
	                                <?=form_error('address','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('time_zone') ? 'has-error' : ''?>">
	                                <label for="time_zone"><?=$this->lang->line("settings_time_zone")?>&nbsp; <span class="text-red">*</span></label>
	                                <?php
	                                $path = APPPATH."config/timezones_class.php";
	                                if(@include($path)) {
	                                    $timezones_cls = new Timezones();
	                                    $timezones = $timezones_cls->get_timezones();
	                                    unset($timezones['']);
	                                    $selectTimeZone['0'] = $this->lang->line('settings_time_zone');
	                                    $timeZones = array_merge($selectTimeZone, $timezones);
	                                            // , settings()->time_zone
	                                    echo form_dropdown("time_zone", $timeZones, set_value("time_zone"), "id='time_zone' class='custom-input-field select2'");
	                                }
	                                ?>
	                                <span class="control-label">
	                                    <?=form_error('time_zone','<div class="text-red">', '</div>'); ?>
	                                </span>
	                            </div>
	                        </div>

	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('currency_code') ? 'has-error' : ''?>">
	                                <label for="currency_code"><?=$this->lang->line('settings_currency_code')?></label> <span class="text-red">*</span>
	                                <input name="currency_code" value="<?=set_value('currency_code','USD')?>" id="currency_code" class="custom-input-field" placeholder="<?=$this->lang->line('settings_currency_code')?>">
	                                <?=form_error('currency_code','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                        <div class="col-md-4 col-sm-12">
	                            <div class="form-group <?=form_error('currency_symbol') ? 'has-error' : ''?>">
	                                <label for="currency_symbol"><?=$this->lang->line('settings_currency_symbol')?></label> <span class="text-red">*</span>
	                                <input name="currency_symbol" value="<?=set_value('currency_symbol','$')?>" id="currency_symbol" class="custom-input-field" placeholder="<?=$this->lang->line('settings_currency_symbol')?>">
	                                <?=form_error('currency_symbol','<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group <?=form_error('user_username') ? 'has-error' : ''?>">
                                    <label for="user_username"><?=$this->lang->line('user_username')?></label> <span class="text-red">*</span>
                                    <input type="text" class="custom-input-field" id="user_username" name="user_username" value="<?=set_value('user_username')?>" placeholder="<?=$this->lang->line('user_username')?>">
                                    <?=form_error('user_username', '<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group <?=form_error('user_password') ? 'has-error' : ''?>">
                                    <label for="user_password"><?=$this->lang->line('user_password')?></label><span class="text-red">*</span>
                                    <input type="password" class="custom-input-field" id="user_password" name="user_password" value="<?=set_value('user_password')?>" placeholder="<?=$this->lang->line('user_password')?>">
                                     <?=form_error('user_password', '<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
	                             <div class="form-group <?=form_error('confirm_password') ? 'has-error' : ''?>">
	                                <label for="confirm_password"><?=$this->lang->line('user_confirm_password')?></label><span class="text-red">*</span>
	                                <input type="password" class="custom-input-field" id="confirm_password" name="confirm_password" value="<?=set_value('confirm_password')?>" placeholder="<?=$this->lang->line('user_confirm_password')?>">
	                                 <?=form_error('confirm_password', '<div class="text-red">', '</div>')?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            	</fieldset>




	         
	        </div>
	        <div class="panel-footer">
	        	<div class="row">
	        		<div class="col-sm-12 col-xs-12">
			        	<div class="pull-left">
			        		<a href="<?=base_url('install/database')?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> PREVIOUS</a>
			        	</div>
						<!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
						<div class="pull-right">
							<button type="submit" class="btn btn-success">NEXT <i class="fa fa-long-arrow-right"></i></button>
						</div>
					</div>
				</div>
				</form>
			</div>
	  	</div>
	</div>
</div>