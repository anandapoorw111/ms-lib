<!-- purchasekey.php -->
<br>
<div class="row setup-content" >
  	<div class="col-sm-8 col-center">
    <!-- <div class="row"> -->
	    <div class="panel panel-info">
	        <div class="panel-heading"><h4>Purchase Key</h4></div>
	        <div class="panel-body ">
	        	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
				<?php
					if(form_error('purchasekey'))
						echo "<div class='form-group has-error'>";
					else
						echo "<div class='form-group' >";
				?>

				<!-- <label for="purchasekey" class="col-sm-4 control-label">
					<p>Purchase key</p>
				</label> -->
				<div class="col-sm-12 custom-input-field">
					<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
					<input type="text" class="custom-form-control" id="purchasekey" name="purchasekey" value="<?=set_value('purchasekey')?>">
					<span class=" control-label"><?php echo form_error('purchasekey'); ?></span>
				</div>

				</div>
	         
	        </div>
	        <div class="panel-footer">
	        	<div class="row">
	        		<div class="col-sm-12 col-xs-12">
			        	<div class="pull-left">
			        		<a href="<?=base_url('install/index')?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> PREVIOUS</a>
			        	</div>
						<!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
						<div class="pull-right">
							<button type="submit" class="btn btn-success">NEXT <i class="fa fa-long-arrow-right"></i></button>
						</div>
					</div>
				</div>
				</form>
			</div>
	  	</div>
	</div>
</div>