<br>
<div class="row setup-content" >
  <div class="col-sm-8 col-center">
    <!-- <div class="row"> -->

      <div class="panel panel-info">
        <div class="panel-heading"><h4>Pre-Install Checklist</h4></div>
        <div class="panel-body">
          <?php
            foreach ($success as $succ) {
              echo "<div class=\"alert alert-success\"><span class=\"fa fa-check-circle\"></span> ". $succ ."</div>";
            }
            foreach ($errors as $er) {
              echo "<div class=\"alert alert-danger\"><span class=\"fa fa-exclamation-circle\"></span> ". $er ."</div>";
            }
          ?>
        </div>
        <div class="panel-footer"><div class="text-right"><a href="<?=base_url('install/purchasekey')?>" class="btn btn-success">NEXT <i class="fa fa-long-arrow-right"></i></a></div></div>
      </div>

    <!-- </div> -->
  </div>
</div>
<br>
