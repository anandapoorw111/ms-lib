<!-- purchasekey.php -->
<br>
<div class="row setup-content" >
  	<div class="col-sm-8 col-center">
    <!-- <div class="row"> -->
	    <div class="panel panel-info">
	        <div class="panel-heading"><h4>Thank You</h4></div>
	        <div class="panel-body ">
	        	<h1 class="text-success text-center">Congratulations</h1>
	        	<h4 class="text-success text-center">Installation Completed.</h4>
				<h5 class="text-default text-center">Please click Finish Button and login Your Site . </h5>

				<div class="col-sm-12 text-center done">
					<p><b>username :</b> &nbsp; <i><?php echo $this->session->userdata('username'); ?></i></p>
					<p><b>password :</b> &nbsp; <i><?php echo $this->session->userdata('password'); ?></i></p>
	         	</div>
	        </div></br>
	        <div class="panel-footer">
	        	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
	        		<div class="text-right">
	        			<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
	        			<button type="submit"  name="submit" class="btn btn-success">Finish <i class="fa fa-long-arrow-right"></i></button>
	        		</div>
	  			</form>
	  		</div>
	  	</div>
	</div>
</div>