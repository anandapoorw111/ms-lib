$(function() {
  $('#example1').DataTable({
    'pageLength':10,
  });
})

$(document).on('keypress', '.only_number', function(e){
    var yourInput = $(this).val();
    var keyCode = e.which ? e.which : e.keyCode
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});

$(document).ready(function(){
  $('.select2').select2({dropdownParent: $('#insert')});
  $('.select2-up').select2({dropdownParent: $('#update')});
});

$(document).on('click', '.insert', function(e){
  "use strict";
  var book_name           = $("#book_name").val();
  var writer_name         = $("#writer_name").val();
  var categories          = $("#categories").val();
  var edition             = $("#edition").val();
  var memberID            = $("#memberID").val();
  var bookrequest_note    = $("#bookrequest_note").val();

  $('#error_bookrequest_name').html('');
  $('.error-name').removeClass('has-error');
  $('#bookrequest_writer_name').html('');
  $('.error-writer').removeClass('has-error');
  $('#error_bookrequest_categories').html('');
  $('.error-categories').removeClass('has-error');
  $('.error-edition').removeClass('has-error');
  $('#error_bookrequest_edition').html('');
  $('#error_bookrequest_member').html('');
  $('.error-member').removeClass('has-error');

  $.ajax({
    dataType: 'json',
    type: 'POST',
    url: THEMEBASEURL+'bookrequest/add',
    data: {
      'book_name'           : book_name,
      'writer_name'         : writer_name,
      'categories'          : categories, 
      'edition'             : edition, 
      'memberID'            : memberID, 
      'bookrequest_note'    : bookrequest_note, 
       [CSRFNAME]           : CSRFHASH,
    },
    dataType: "html",
    success: function(data){
      var response = jQuery.parseJSON(data);
      console.log(data);
      if(response.confirmation == 'Success'){
        $('#insert').modal('hide');
        swal({
          title: "Successfull.",
          position: 'top-end',
          type: 'success',
          showConfirmButton: false,
          timer: 1600,
        });
        setTimeout(function(){ window.location.href = THEMEBASEURL+'bookrequest'; }, 1800);
      }else{
        $('#error_bookrequest_name').html(response.validations['book_name']);
        if (response.validations['book_name']) {
            $('.error-name').addClass('has-error');
        };
        $('#bookrequest_writer_name').html(response.validations['writer_name']);
        if (response.validations['writer_name']) {
            $('.error-writer').addClass('has-error');
        };
        $('#error_bookrequest_categories').html(response.validations['categories']);
        if (response.validations['categories']) {
            $('.error-categories').addClass('has-error');
        };

        $('#error_bookrequest_edition').html(response.validations['edition']);
        if (response.validations['edition']) {
            $('.error-edition').addClass('has-error');
        };

        $('#error_bookrequest_member').html(response.validations['memberID']);
        if (response.validations['memberID']) {
            $('.error-member').addClass('has-error');
        };

      }
    }
  });
});

$(document).on('click', '.update', function(e){
  "use strict";
  var id = $(this).attr('id');
  if(id != 'NULL' || id != '') {
    dataType: "json",
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'bookrequest/retrive',
      data: {
        'id'      : id,
        [CSRFNAME]   : CSRFHASH,
      },
      dataType: "html",
      success: function(data) {
        var response = jQuery.parseJSON(data);
        console.log(response);
        if(response.confirmation == 'Success') {
          $('#bookrequestID').val(response.id);
          $('#book_name_up').val(response.book_name);
          $('#writer_name_up').val(response.writer_name);
          $('#categories_up').val(response.categories);
          $('#edition_up').val(response.edition);
          $('#memberID_up').val(response.memberID).trigger('change');
          $('#bookrequest_note_up').val(response.bookrequest_note);


          $('.updated').click(function(){
            var id                 = $('#bookrequestID').val();
            var book_name           = $("#book_name_up").val();
            var writer_name         = $("#writer_name_up").val();
            var categories          = $("#categories_up").val();
            var edition             = $("#edition_up").val();
            var memberID            = $("#memberID_up").val();
            var bookrequest_note    = $("#bookrequest_note_up").val();

            $('#error_bookrequest_name_up').html('');
            $('.error-name-up').removeClass('has-error');
            $('#bookrequest_writer_name_up').html('');
            $('.error-writer-up').removeClass('has-error');
            $('#error_bookrequest_categories_up').html('');
            $('.error-categories-up').removeClass('has-error');
            $('#error_bookrequest_edition_up').html('');
            $('.error-edition-up').removeClass('has-error');
            $('#error_bookrequest_member_up').html('');
            $('.error-member-up').removeClass('has-error');
            $.ajax({
              dataType: 'json',
              type: 'POST',
              url: THEMEBASEURL+'bookrequest/edit',
              data: {
                'id'  : id,
                'book_name'           : book_name,
                'writer_name'         : writer_name,
                'categories'          : categories, 
                'edition'             : edition, 
                'memberID'            : memberID, 
                'bookrequest_note'    : bookrequest_note, 
                [CSRFNAME]            : CSRFHASH,
              },
              dataType: "html",
              success: function(data){
                var response = jQuery.parseJSON(data);
                console.log(data);
                if(response.confirmation == 'Success'){
                  $('#update').modal('hide');
                  swal({
                    title: "Successfully Updated.",
                    position: 'top-end',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 1600,
                  });
                  setTimeout(function(){ window.location.href = THEMEBASEURL+'bookrequest'; }, 1800);
                }else{
                  $('#error_bookrequest_name_up').html(response.validations['book_name']);
                  if (response.validations['book_name']) {
                      $('.error-name-up').addClass('has-error');
                  };
                  $('#bookrequest_writer_name_up').html(response.validations['writer_name']);
                  if (response.validations['writer_name']) {
                      $('.error-writer-up').addClass('has-error');
                  };
                  $('#error_bookrequest_categories_up').html(response.validations['categories']);
                  if (response.validations['categories']) {
                      $('.error-categories-up').addClass('has-error');
                  };

                  $('#error_bookrequest_edition_up').html(response.validations['edition']);
                  if (response.validations['edition']) {
                      $('.error-edition-up').addClass('has-error');
                  };

                  $('#error_bookrequest_member_up').html(response.validations['memberID']);
                  if (response.validations['memberID']) {
                      $('.error-member-up').addClass('has-error');
                  };
                }
              }
            });
          });
        }
      }
    });
  }
});



// delete script
$(document).on('click', '.delete', function(e){
  "use strict";
  var id = $(this).attr("id");
  swal({
    title: 'Are you sure?',
    text: "It will be deleted permanently!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
    showLoaderOnConfirm: true,

    preConfirm: function() {
      return new Promise(function(resolve) {
       $.ajax({
        url: THEMEBASEURL+'bookrequest/delete',
        type: 'POST',
        data: {
          'id'       : id,
          [CSRFNAME]    : CSRFHASH,
        },
        dataType: 'json'
      })
       .done(function(response){
        swal('Deleted!', response.message, response.status);
        setTimeout(function(){ window.location.href = THEMEBASEURL+'bookrequest'; }, 2000);
      })
       .fail(function(){
        swal('Oops...', 'Something went wrong with You !', 'error');
      });
     });
    },
    allowOutsideClick: false        
  }); 
});


// status script
$(document).on('click', '.onoffswitch-small-checkbox', function(e){
  "use strict";
  var status = '';
  var id = 0;
  if($(this).prop('checked')){
    status = '1';
    id = $(this).parent().attr("id");
  } else {
    status = '0';
    id = $(this).parent().attr("id");
  }
  if((status != '' || status != null) && (id !='')) {
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'bookrequest/status',
      data: {
        'id'      : id,
        'status'    : status,
        [CSRFNAME]   : CSRFHASH,
      },

      dataType: "html",
      success: function(data){
        var response = jQuery.parseJSON(data);
        console.log(data);
        if(response.confirmation == 'Success'){
          swal({
            title: "Successfully Updated.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1600,
          });
        } 
      }
    });
  }
});


