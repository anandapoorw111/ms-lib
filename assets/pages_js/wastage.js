$(function() {
  $('#example1').DataTable({
    'pageLength':10,
  });
})

$(document).ready(function(){
    $('.select2').select2({ dropdownParent: $("#insert")});
});

$(document).on('click', '.insert', function(e){
  "use strict";
  var bookID        = $("#book_name").val();
  var wastage_note  = $("#wastage_note").val();


  $('#error_book_name').html('');
  $('.error_name').removeClass('has-error');
  $.ajax({
    dataType: 'json',
    type: 'POST',
    url: THEMEBASEURL+'wastage/add',
    data: {
      'bookID'          : bookID,
      'wastage_note'    : wastage_note,
       [CSRFNAME]       : CSRFHASH,
    },
    dataType: "html",
    success: function(data){
      var response = jQuery.parseJSON(data);
      console.log(data);
      if(response.confirmation == 'Success'){
        $('#insert').modal('hide');
        swal({
          title: "Successfull.",
          position: 'top-end',
          type: 'success',
          showConfirmButton: false,
          timer: 1600,
        });
        setTimeout(function(){ window.location.href = THEMEBASEURL+'wastage'; }, 1800);
      }else{
        $('#error_book_name').html(response.validations['bookID']);
        $('.error_name').addClass('has-error');
      }
    }
  });
});


// restore script
$(document).on('click', '.restore', function(e){
  "use strict";
  var id = $(this).attr("id");
  swal({
    title: 'Are you sure?',
    text: "It will be Restore Permanently!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, Restore It!',
    showLoaderOnConfirm: true,

    preConfirm: function() {
      return new Promise(function(resolve) {
       $.ajax({
        url: THEMEBASEURL+'wastage/restore',
        type: 'POST',
        data: {
          'id'       : id,
          [CSRFNAME]    : CSRFHASH,
        },
        dataType: 'json'
      })
       .done(function(response){
        swal('Deleted!', response.message, response.status);
        setTimeout(function(){ window.location.href = THEMEBASEURL+'wastage'; }, 2000);
      })
       .fail(function(){
        swal('Oops...', 'Something went wrong with You !', 'error');
      });
     });
    },
    allowOutsideClick: false        
  }); 
});




