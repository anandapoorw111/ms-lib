$(document).ready(function(){
    $('.select2').select2();
    $('[data-toggle="tooltip"]').tooltip();
});


// for print 
$(document).on('click', '.prints', function(e){
    var divElements = document.getElementById('print_areas').innerHTML;
    var oldPage = document.body.innerHTML;
    document.body.innerHTML = 
    "<html>"+
    "<head>"+
    "<title>ID Card Genarated</title><style type='text/css'>.box-border{border:1px solid#ccc;width: 3.6in; height: 2.2in}.img-height{height: 44px; position: absolute;top: 0px;right: 1px;}.idcard-img-resize{padding: 0px !important;}.img-thumbnail{height:100px !important;}.idcard-font-resize h4{line-height: 0px;margin-bottom: 15px;font-size: 15px;}.idcard-font-resize p{font-size: 11px;line-height: 11px;}.idcard-font-resize img{position: absolute;bottom: 5px; right: 0px;}.idcard-img-resize p{padding-top: 10px;}.box{width: 3.6in;height: 2.2in;}</style>"+
    "</head>"+
    "<body>" + 
    divElements + "</body>";
    window.print();
    document.body.innerHTML = oldPage;
});